#ifndef DATABASE_H
#define DATABASE_H

#include <QMainWindow>
#include <QDateTime>
#include <QtGui>
#include "stdio.h"
#include <sqlite3.h>
#include <define.h>




#define DBCLIENTE "extras/cliente.db"

/**
 *\class    databaseQuiniela
 *\brief    Clase de Quiniela
 *\note     Clase para la  administracion de las Quinielas entre la Base de Datos y Programa
*/

class databaseQuiniela{

    public:

        databaseQuiniela();
        ~databaseQuiniela();

        /// String para almacenar la Nonbre del Sorteo
        QString nombreSorteo;
        /// String para almacenar la Fecha del Sorteo
        QString fechaSorteo;
        /// String para almacenar la Tipo del Sorteo
        QString tipoSorteo;
        /// String para almacenar la Numero del Sorteo
        QString numeroSorteo;
        /// String para almacenar los 20 Premios del Sorteo
        QString premiosSorteo[20];
        /// String para almacenar las 4 Letras del Sorteo
        QString letrasSorteo;
        int manual;


        int insertarSorteo();
        int seleccionarSorteo();
        int seleccionarSorteoUnico();
        int insertarSorteoManual();
        int seleccionarSorteoManual();

private:
        sqlite3 *db;
};

class dataBaseSuenios{

    public:

        dataBaseSuenios();
        ~dataBaseSuenios();

        QString direccionSuenios[100];
        QString textoSuenios[100];

        int seleccionarSuenios();

    private:
        sqlite3 *db;
};

struct config
{
    QString nombre;
    int estado;
    int cambio;
};


/* Clase te carga en un vector todas las direcciones*/
class publicidadVariable{

    public:
        publicidadVariable(){

        };

        ~publicidadVariable(){

        };

        QString nombrePublicides[10];

        int seleccionarPublicidadVariable();

   private:
};


// FUNCIONES GENERALES
//int abrirDatabase (QString direccion,sqlite3 **db);  // abre la base
    int abrirBaseCliente(sqlite3 **db);


// FUNCIONES PARA USAR CON LA TABLA DE PUBLICIDAD VARIABLE ver ejemplos en el .cpp
    int insertarPublicidadVariable(QString direccion);                          // Inserta publicidad variable previo chequear que no exista
    int borrarPublicidadVariable (QString direccion);                           // Elimina una publicidad variable
    int actualizarPublicidadVariable(QString direccion1,QString direccion2);    // Le pasas el nombre viejo y el nuevo y te lo actualiza
    int borrarPublicidadVariableTodas(void);                                    // Deja la tabla de publicidad variable en blanco
    int contarPublicidadVariable(void);                                         // Cuenta cuantas publicidades ha en la tabla

// FUNCIONES PARA USAR CON LA TABLA DE PUBLICIDAD FIJA ver ejemplos en el .cpp
    int insertarPublicidadFija( QString direccion , int idPublicidad );                                     // Inserta publicidad fija previo chequear que no exista
    int borrarPublicidadFija (QString direccion);                                                           // Elimina publicidad fija
    int actualizarPublicidadFija ( int idPublicidadExistente, int idPublicidadNueva , QString direccion);   // Le pasas el nombre viejo y el nuevo y te lo actualiza
    int borrarPublicidadFijaTodas(void);                                                                    // Deja la tabla de publicidad variable en blanco
    int contarPublicidadFija(void);                                                                         // Cuenta cuantas publicidades ha en la tabla
    int seleccionarPublicidadFija(QString *vector);                                                         // Te carga en un vector todas las direcciones
    int existePublicidadFija( int * id );
    int actualizarIDPublicidadFija( int * id );

// FUNCIONES PARA LOS JUEGOS
    int direccionImagenQuini(int numero,char *direccion);                       // Le paso un numero y me devuelve la direccion de la imagen de ese numero

// Funciones de manejo de la opciones de la configuracion

    int setConfiguracion ( QString nombre , int valor );
    int getConfiguracion ( QString nombre );
    int cambioConfiguracion ( QString nombre );
    int clearConfiguracion ( void );


#endif // DATABASE_H
