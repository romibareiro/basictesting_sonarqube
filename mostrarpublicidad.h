#ifndef MOSTRARPUBLICIDAD_H
#define MOSTRARPUBLICIDAD_H

#include <QDialog>
#include <QApplication>
#include <QtGui>
#include <QTimer>
#include <QPainter>
#include <QtCore>
#include <QGraphicsOpacityEffect>
#include <QGraphicsEffect>
#include <QPalette>
#include <QColor>
#include <QPixmap>
#include <QGraphicsBlurEffect>
//#include <QColorGroup>
#include "database.h"
#include <define.h>

//#include <phonon/MediaObject> //audio
//#include <phonon/MediaSource> //audio
#include <QMovie>
#include <QDebug>
#include <QTime>

#define RESOLUCION_1920x1080 0
#define RESOLUCION_1280x720  1
#define RESOLUCION_1024x768  2
#define RESOLUCION_768x576   3
#define RESOLUCION_720x480   4

#define EFECTO_ZOOM_IN    0
#define EFECTO_DEZPLAZAR  1
#define EFECTO_OPACIDAD   2
#define EFECTO_CORTINA    3
#define INICIO_ESPACIO_PUBLICITARIO 4
#define FIN_ESPACIO_PUBLICITARIO    5

#define PUBLICIDAD_FIJA     0
#define PUBLICIDAD_VARIABLE 1

#define EFECTO_TITULO_SUBIR 0
#define EFECTO_TITULO_BAJAR 1

namespace Ui {
class MostrarPublicidad;
}

class MostrarPublicidad : public QDialog
{
    Q_OBJECT

public:

    explicit MostrarPublicidad(QWidget *parent = 0);
    QTimer *TimerPropaganda;
    publicidadVariable Variable;


    typedef struct _PANTALLA
    {
        int Ancho;
        int Alto;
        int Relacion_Ancho;
        int Relacion_Alto;
    }PANTALLA;

    PANTALLA Pantalla;

    typedef struct _PUBLICIDAD
    {
        int X;
        int Y;
        int Ancho;
        int Alto;
        int Cero_Y;
        int Tipo;
        int Cantidad_Variable;
        int Duracion;
        qreal IndiceOpacidad;
        qreal SaltoOpacidad;
    }PUBLICIDAD;

    PUBLICIDAD Publicidad;

    typedef struct _TITULO
    {
        int X;
        int Y;
        int Interlineado;
        int Separacion;
        int Ancho;
        int Fuente;
        int Efecto;
        int EstadoInicial;
    }TITULO;

    TITULO Titulo;

    typedef struct _MARQUESINA
    {
        int X;
        int Y;
        int Alto;
    }MARQUESINA;

    MARQUESINA Marquesina;

    typedef struct _EFECTOS
    {
        int Timer;
        int Contador_Timer;
        int Estado;
    } EFECTOS;

    EFECTOS Efectos;

    typedef struct _CUADRADO
    {
        int Ancho;
        int Alto;
        int X1;
        int X2;
        int X3;
        int X4;
        int Y1;
        int Y2;
        int Y3;
        int Y4;
    }CUADRADO;

    CUADRADO Cuadrado;

    typedef struct _ESPACIOPUBLICITARIO
    {
            int X;
            int Y;
            int Ancho;
            int Alto;
    }ESPACIOPUBLICITARIO;

    ESPACIOPUBLICITARIO EspacioPublicitario;

    ~MostrarPublicidad();

private:
    Ui::MostrarPublicidad *ui;

    int habilitacionSonido;
    int habilitacionPublicidad;

private slots:
    void ConfiguracionInicial  ( void );
    void ConfiguracionPantalla ( void );
    void ManejoEfectos         ( void );
    void Publicitario          ( void );
    void EfectoZoomIn          ( void );
    void EfectoDesplazar       ( void );
    void EfectoOpacidad        ( void );
    void EfectoCortina         ( void );
    void TituloPublicidad      ( void );
    void PosicionInicial       ( void );
    void TextoTitulo           ( void );
    void FuenteTitulo          ( void );
    void ColorTitulo           ( int  );
    void PosicionTitulo        ( void );
    void MarquesinaPublicidad  ( void );
};

#endif // MOSTRARPUBLICIDAD_H
