#include "presentacion.h"
#include "ui_presentacion.h"

extern int serialNumber;
extern bool estadoConexion;

/**
 *\fn       presentacion::presentacion(QWidget *parent) : QDialog(parent) , ui(new Ui::presentacion)
 *\brief    Clase de la ventana de la presentacion.
 *\note     Clase de la ventana de la presentacion.
*/

presentacion::presentacion(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::presentacion)
{
    ui->setupUi(this);

    this->setGeometry( 0 , 0 , 1280 , 720 );

    ui->tituloSuperior->setText("Bienvenido al");
    ui->tituloInferior->setText("TimBear");
    ui->imagen->setPixmap(QPixmap("extras/logoTimBear.png") );

    ui->texto->setText("Conectando al Servidor...");
    ui->progressBar->setValue(0);


    QCursor cursor;

    cursor.setPos(1280,720);

    this->setCursor(cursor);


}

/**
 *\fn       presentacion::~presentacion()
 *\brief    Destructor ventana de la presentacion.
 *\note     Clase de la ventana de la presentacion.
*/

presentacion::~presentacion()
{
    delete ui;
}

/**
 *\fn       void presentacion::onBarraPaso(int porcentaje , QString texto)
 *\brief    Barra de paso
 *\note     Avance de la barra de paso de la ventana de presentacion.
 *\param    [in] int porcentaje -> Procentaje de la barra de paso.
 *\param    [in] QString texto  -> Texto de mostrar en la ventana de presentacion.
*/

void presentacion::onBarraPaso(int porcentaje , QString texto)
{
    ui->texto->setText(texto);
    ui->progressBar->setValue(porcentaje);

    if(porcentaje > 100)
        this->close();
}

/**
 *\fn       presentacionThread::presentacionThread() : QThread()
 *\brief    Constructor del Thead de la ventana de la presentacion.
 *\note     Constructor del Thead de la ventana de la presentacion.
*/

presentacionThread::presentacionThread() :
    QThread()
{
    qDebug() << "Corriendo";

    myCliente = new MyTCPClient;

    this->stop = 0;
}

/**
 *\fn       void presentacionThread::run()
 *\brief    Thead de la ventana de la presentacion.
 *\note     Thead de la ventana de la presentacion.
*/

void presentacionThread::run()
{

    this->sleep(1);

    QString textoEnviar;

    emit barraPaso( 28 ,"Transfiriendo Quinielas...");

    textoEnviar = "{\"id\":" + QString::number(serialNumber) + ",\"pedido\":1,\"nombre\":\"Nacional\",\"tipo\":\"Primera\",\"fecha\":\"20150219\"}";
     myCliente->writeMyData(textoEnviar);

/*

    callTuJugada( "quiniela_nacional" , "1422015");
    passTuJugada( "Nacional", "20150214" );

    callTuJugada( "quiniela_provincia_buenos_aires" , "1422015");
    passTuJugada( "Buenos_Aires", "20150214" );

    callTuJugada( "quiniela_santa_fe" , "1422015");
    passTuJugada( "Santa_Fe", "20150214" );

    callTuJugada( "quiniela_cordoba" , "1422015");
    passTuJugada( "Cordoba", "20150214" );

    callTuJugada( "quiniela_nacional" , "2812015");
    passTuJugada( "Nacional", "20150218" );

    callTuJugada( "quiniela_provincia_buenos_aires" , "2812015");
    passTuJugada( "Buenos_Aires", "20150218" );

    callTuJugada( "quiniela_santa_fe" , "1822015");
    passTuJugada( "Santa_Fe", "20150218" );

    callTuJugada( "quiniela_cordoba" , "1822015");
    passTuJugada( "Cordoba", "20150218" );
*/
    this->sleep(10);

    textoEnviar = "{\"id\":" + QString::number(serialNumber) + ",\"pedido\":2}";
    myCliente->writeMyData(textoEnviar);

    this->sleep(10);

    emit barraPaso( 53 ,"Configurando TimBear...");

//   emit TCPCliente.readData();

// Pedido de Quiniela (Servicio 1)
// "{"id":55550000,"pedido":1,"nombre":"Nacional","tipo":"Primera","fecha":"20150131"}"
// "{"id":55550000,"pedido":1,"nombre":"Nacional","tipo":"Primera","fecha":"20150130","numero":"123456","estado":0,
// "premio":["3161","2603","1483","3782","7159","6571","9651","9054","7051","5722","6153","1924","6730","8663","0184",
// "0579","8166","0097","9771","2501"],"letras":"AFRX"}"

/*
    textoEnviar = "{\"id\":" + QString::number(serialNumber) + ",\"pedido\":1,\"nombre\":\"Nacional\",\"tipo\":\"Primera\",\"fecha\":\"20150219\"}";
    myCliente->writeMyData(textoEnviar);
*/

// Pedido de Publicidad (Servicio 3)
// "{"id":55550000,"pedido":2}"
// "{"id":55550000,"pedido":2,"idp1":2,"peso1":8140,"idp2":1"peso2":183687,"idp3":3"peso3":10775,"idp4":4"peso4":160350"}"

/*
    textoEnviar = "{\"id\":" + QString::number(serialNumber) + ",\"pedido\":2}";
    myCliente->writeMyData(textoEnviar);
*/

// Pedido de Version (Servicio 4)
// "{"id":55550000,"pedido":4,"version":172}"
// "{"id":55550000,"pedido":4,"publicidad":2,"vivo":1,"versiondisponible":201}"

/*
    int versionActual;
    versionActual = getConfiguracion("version");
    textoEnviar = "{\"id\":" + QString::number(serialNumber) + ",\"pedido\":4,\"version\":" + QString::number(versionActual) + "}";
    myCliente->writeMyData(textoEnviar);
*/

    this->sleep(5);

    emit barraPaso( 79 ,"Cargando Interfaz...");

    int versionActual;
    versionActual = getConfiguracion("version");
    textoEnviar = "{\"id\":" + QString::number(serialNumber) + ",\"pedido\":4,\"version\":" + QString::number(versionActual) + "}";
    myCliente->writeMyData(textoEnviar);

    estadoConexion = 0;

    this->sleep(5);

    emit barraPaso( 100 ,"Comenzando...");

    this->sleep(3);

    emit signalMainWindow( EXIT_SUCCESS );

    emit barraPaso( 101 ,"");

}
