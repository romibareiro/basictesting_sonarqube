#ifndef USBTHREAD_H
#define USBTHREAD_H

#include <QThread>
#include <QtCore>
#include <unistd.h>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include "usbthread.h"

#define JPG "jpg"
#define JPEG "jpeg"
#define PNG "png"

class USBThread : public QThread
{
    Q_OBJECT
public:
    explicit USBThread(QObject *parent = 0);
    void run();
    void stop();
signals:
   void cargarDesdeElUSB(char *);
public slots:
    
};

#endif // USBTHREAD_H
