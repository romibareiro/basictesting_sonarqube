#include "mostrarpublicidad.h"
#include "ui_mostrarpublicidad.h"


//int RESOLUCION_PANTALLA = RESOLUCION_1920x1080;
//int RESOLUCION_PANTALLA = RESOLUCION_1280x720;
//int RESOLUCION_PANTALLA = RESOLUCION_1024x768;
//int RESOLUCION_PANTALLA = RESOLUCION_768x576;
//int RESOLUCION_PANTALLA = RESOLUCION_720x480;

extern int RESOLUCION_PANTALLA;

/*************************************************************************************************************************************/

/**
 \fn MostrarPublicidad::MostrarPublicidad(QWidget *parent):QDialog(parent),ui(new Ui::MostrarPublicidad)
 \brief Constructor.
 \note Constructor de la Interfaz de Mostrar las Publicidades.
*/

MostrarPublicidad::MostrarPublicidad(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MostrarPublicidad)
{
    ui->setupUi(this);

    ConfiguracionPantalla ( );
    ConfiguracionInicial  ( );

    // Revisar si la Publicidad Local se encuentra Habilitada

    qDebug() << "Habilitar Publicidad";
    habilitacionPublicidad =  getConfiguracion("publicidad");

    qDebug() << "Habilitar Sonido";
    habilitacionSonido = getConfiguracion("sonido");

    // Cuento la cantidad de publicidades variables que existen en la base de datos
    Variable.seleccionarPublicidadVariable();
//    actualizarPublicidadVariable("/home/texto","/extras/publicidad variable");
    Publicidad.Cantidad_Variable = contarPublicidadVariable();

    TimerPropaganda = new QTimer ( this );
    TimerPropaganda->setInterval( Efectos.Timer );
    QObject::connect( TimerPropaganda , SIGNAL ( timeout ( ) ), this , SLOT ( ManejoEfectos ( ) ) );
    TimerPropaganda->start( );
}

/*************************************************************************************************************************************/


/**
 \fn MostrarPublicidad::~MostrarPublicidad()
 \brief Destructor.
 \note Destructor de la Interfaz de Mostrar las Publicidades.
*/

MostrarPublicidad::~MostrarPublicidad()
{
    delete ui;
}

/*************************************************************************************************************************************/


/**
 \fn void MostrarPublicidad::ConfiguracionInicial ( void )
 \brief Configuracion Inicial.
 \note Funcion que carga a las variables utilizadas en el QDialog con los valores iniciales necesarios para su funcionamiento.
*/

void MostrarPublicidad::ConfiguracionInicial ( void )
{
    Publicidad.Duracion       = 60;
    Publicidad.IndiceOpacidad = 0;
    Publicidad.SaltoOpacidad  = 0.01;
    Publicidad.Tipo           = PUBLICIDAD_FIJA;
    Publicidad.Cero_Y         = 80 * Pantalla.Alto / 720;

    Efectos.Estado         = INICIO_ESPACIO_PUBLICITARIO;
    Efectos.Timer          = 100;
    Efectos.Contador_Timer = 0;

    Titulo.EstadoInicial = 0;
    Titulo.Efecto        = EFECTO_TITULO_SUBIR;
    Titulo.Y             = 0;
    Titulo.X             = 70 * Pantalla.Ancho / 1280;
    Titulo.Interlineado  = 60 * Pantalla.Ancho / 1280;
    Titulo.Separacion    = 40 * Pantalla.Ancho / 1280;
    Titulo.Fuente        = 30 * Pantalla.Ancho / 1280;
    Titulo.Ancho         = 40 * Pantalla.Ancho / 1280;

    Cuadrado.Alto  = 1;
    Cuadrado.Ancho = 1;
    Cuadrado.X1    = 0;
    Cuadrado.X2    = Pantalla.Ancho - Cuadrado.Ancho;
    Cuadrado.X3    = 0;
    Cuadrado.X4    = Pantalla.Ancho - Cuadrado.Ancho;
    Cuadrado.Y1    = Publicidad.Cero_Y;
    Cuadrado.Y2    = Publicidad.Cero_Y;
    Cuadrado.Y3    = Publicidad.Cero_Y + Cuadrado.Alto;
    Cuadrado.Y4    = Publicidad.Cero_Y + Cuadrado.Alto;

    ui->Cuadrado_1->setGeometry( Cuadrado.X1 , Cuadrado.Y1 , Cuadrado.Ancho , Cuadrado.Alto );
    ui->Cuadrado_2->setGeometry( Cuadrado.X2 , Cuadrado.Y2 , Cuadrado.Ancho , Cuadrado.Alto );
    ui->Cuadrado_3->setGeometry( Cuadrado.X3 , Cuadrado.Y3 , Cuadrado.Ancho , Cuadrado.Alto );
    ui->Cuadrado_4->setGeometry( Cuadrado.X4 , Cuadrado.Y4 , Cuadrado.Ancho , Cuadrado.Alto );

    Marquesina.Alto = 40 * Pantalla.Alto / 720;
    Marquesina.X    = Pantalla.Ancho;
    Marquesina.Y    = Pantalla.Alto - Marquesina.Alto;

    EspacioPublicitario.X      = 0;
    EspacioPublicitario.Y      = 0;
    EspacioPublicitario.Alto   = Pantalla.Alto;
    EspacioPublicitario.Ancho  = Pantalla.Ancho;
}

/*************************************************************************************************************************************/


/**
 \fn void MostrarPublicidad::ConfiguracionPantalla ( void )
 \brief Configuracion Pantalla.
 \note Se cargan variables con el tamaño de la pantalla acorde a la Resolucion de Pantalla configurada por el agenciero y el tamaño de
       la pantalla se ajusta a las mismas.
*/

void MostrarPublicidad::ConfiguracionPantalla ( void )
{
    switch ( RESOLUCION_PANTALLA )
    {
        case RESOLUCION_1920x1080 : Pantalla.Alto  = 1080;
                                    Pantalla.Ancho = 1920;
                                    Pantalla.Relacion_Alto  = 15;
                                    Pantalla.Relacion_Ancho = 32;
                                    break;

        case RESOLUCION_1280x720 :  Pantalla.Alto  = 720;
                                    Pantalla.Ancho = 1280;
                                    Pantalla.Relacion_Alto  = 15;
                                    Pantalla.Relacion_Ancho = 32;
                                    break;

        case RESOLUCION_1024x768 :  Pantalla.Alto  = 768;
                                    Pantalla.Ancho = 1024;
                                    Pantalla.Relacion_Alto  = 5;
                                    Pantalla.Relacion_Ancho = 8;
                                    break;

        case RESOLUCION_768x576 :   Pantalla.Alto  = 576;
                                    Pantalla.Ancho = 768;
                                    Pantalla.Relacion_Alto  = 5;
                                    Pantalla.Relacion_Ancho = 8;
                                    break;

        case RESOLUCION_720x480 :   Pantalla.Alto  = 480;
                                    Pantalla.Ancho = 720;
                                    Pantalla.Relacion_Alto  = 5;
                                    Pantalla.Relacion_Ancho = 9;
                                    break;

        default :                   break;
    }

    // Ajusto el tamaño del QDialog a la Resolucion configurada por el Agenciero
    this->setGeometry(0,0,Pantalla.Ancho,Pantalla.Alto);
}

/*************************************************************************************************************************************/


/**
 \fn void MostrarPublicidad::ManejoEfectos ( void )
 \brief Administrar los efectos de las publicidades.
 \note Se maneja la aplicacion de los efectos dinamicos al comienzo y fin del proceso como tambien a las distintas publicidades tanto
       Fijas como Variables.
*/

//Se maneja la aplicacion de los efectos dinamicos a las distintas publicidades tanto Fijas como Variables

void MostrarPublicidad::ManejoEfectos ( void )
{
    switch ( Efectos.Estado )
    {
        case INICIO_ESPACIO_PUBLICITARIO :  Publicitario();
                                            break;

        case EFECTO_ZOOM_IN :               EfectoZoomIn();
                                            break;

        case EFECTO_DEZPLAZAR :             EfectoDesplazar();
                                            break;

        case EFECTO_OPACIDAD :              EfectoOpacidad();
                                            break;

        case EFECTO_CORTINA  :              EfectoCortina();
                                            break;

        case FIN_ESPACIO_PUBLICITARIO :     Publicitario();
                                            break;

        default :                           break;
    }
}

/*************************************************************************************************************************************/

/**
 \fn void MostrarPublicidad::Publicitario ( void )
 \brief Efecto Publicitario.
 \note Se maneja la aplicacion un aviso tanto visual como sonoro al inicio y el comienzo del espacio publicitario.
*/

void MostrarPublicidad::Publicitario ( void )
{
    if ( Efectos.Contador_Timer == 0 )
    {
        switch ( Efectos.Estado )
        {
            case INICIO_ESPACIO_PUBLICITARIO :  ui->Propaganda_1->setPixmap(QPixmap("extras/Inicio.jpg"));
                                                break;

            case FIN_ESPACIO_PUBLICITARIO :     ui->Propaganda_1->setPixmap(QPixmap("extras/Fin.jpg"));
                                                break;

            default :                           break;
        }

        ui->Propaganda_1->adjustSize( );
        ui->Propaganda_1->setScaledContents( true );
        ui->Propaganda_1->setGeometry( EspacioPublicitario.X , EspacioPublicitario.Y , EspacioPublicitario.Ancho , EspacioPublicitario.Alto );

        ui->Cuadrado_1->setGeometry( 0 , 0 , 1 , 1 );
        ui->Cuadrado_2->setGeometry( 0 , 0 , 1 , 1 );
        ui->Cuadrado_3->setGeometry( 0 , 0 , 1 , 1 );
        ui->Cuadrado_4->setGeometry( 0 , 0 , 1 , 1 );

        ui->Marquesina->setGeometry( 0 , 0 , 1 , 1 );

        ui->Propaganda_2->setGeometry( 0 , 0 , 1 , 1 );

        ui->Titulo_1->setGeometry ( 0 , 0 , 1 , 1 );
        ui->Titulo_2->setGeometry ( 0 , 0 , 1 , 1 );
        ui->Titulo_3->setGeometry ( 0 , 0 , 1 , 1 );
        ui->Titulo_4->setGeometry ( 0 , 0 , 1 , 1 );
        ui->Titulo_5->setGeometry ( 0 , 0 , 1 , 1 );
        ui->Titulo_6->setGeometry ( 0 , 0 , 1 , 1 );
        ui->Titulo_7->setGeometry ( 0 , 0 , 1 , 1 );
        ui->Titulo_8->setGeometry ( 0 , 0 , 1 , 1 );
        ui->Titulo_9->setGeometry ( 0 , 0 , 1 , 1 );
        ui->Titulo_10->setGeometry( 0 , 0 , 1 , 1 );
        ui->Titulo_11->setGeometry( 0 , 0 , 1 , 1 );
        ui->Titulo_12->setGeometry( 0 , 0 , 1 , 1 );
        ui->Titulo_13->setGeometry( 0 , 0 , 1 , 1 );
        ui->Titulo_14->setGeometry( 0 , 0 , 1 , 1 );
        ui->Titulo_15->setGeometry( 0 , 0 , 1 , 1 );
        ui->Titulo_16->setGeometry( 0 , 0 , 1 , 1 );
        ui->Titulo_17->setGeometry( 0 , 0 , 1 , 1 );
        ui->Titulo_18->setGeometry( 0 , 0 , 1 , 1 );
        ui->Titulo_19->setGeometry( 0 , 0 , 1 , 1 );
    }

    Efectos.Contador_Timer++;

    if ( Efectos.Contador_Timer == 10 * 1000 / (8 * Efectos.Timer) )
    {
        if ( Efectos.Estado == INICIO_ESPACIO_PUBLICITARIO )
        {
            Efectos.Estado = EFECTO_ZOOM_IN;
            if( habilitacionSonido == SONIDO_ON )
                system("mplayer extras/audio/Inicio.wav -endpos 2");// Reproduce solamente 5 segundos
        }

        if ( Efectos.Estado == FIN_ESPACIO_PUBLICITARIO )
        {
            if( habilitacionSonido == SONIDO_ON )
                system("mplayer extras/audio/Fin.wav -endpos 2"); // Reproduce solamente 5 segundos

            TimerPropaganda->stop();
            close();
        }

        Efectos.Contador_Timer = 0;
    }
}

/*************************************************************************************************************************************/

/**
 \fn void MostrarPublicidad::EfectoZoomIn ( void )
 \brief Efecto Zoom In.
 \note Se maneja la aplicacion del efecto dinamicos de Zoom In a las distintas publicidades tanto Fijas como Variables en la que las
       mismas comienza con un tamaño minimo y se va incrementado acorde a la relacion de aspecto del tamaño efectivo de la resolucion
       configurada por el agenciero.
*/

void MostrarPublicidad::EfectoZoomIn ( void )
{
    QRect Propiedades;

    if ( Efectos.Contador_Timer == 0 )
    {
        Publicidad.X     = Pantalla.Ancho / 2;
        Publicidad.Y     = (Pantalla.Alto - Publicidad.Cero_Y - Marquesina.Alto) / 2 + Publicidad.Cero_Y;
        Publicidad.Ancho = 1;
        Publicidad.Alto  = 1;

        switch ( Publicidad.Tipo )
        {
            case PUBLICIDAD_FIJA :      ui->Propaganda_1->setPixmap(QPixmap("extras/publicidadFija/0.jpg"));
                                        break;

            case PUBLICIDAD_VARIABLE :  ui->Propaganda_1->setPixmap(QPixmap(Variable.nombrePublicides[0]));
                                        break;

            default :                   break;
        }

        ui->Propaganda_1->adjustSize( );
        ui->Propaganda_1->setScaledContents( true );
        ui->Propaganda_1->setGeometry( Publicidad.X , Publicidad.Y , Publicidad.Ancho , Publicidad.Alto );

        ui->Propaganda_2->setGeometry( 0 , 0 , 1 , 1 );
    }

    Efectos.Contador_Timer++;

    Propiedades = ui->Propaganda_1->geometry( );

    if ( Propiedades.width() <= Pantalla.Ancho )
    {
        Publicidad.Alto  += Pantalla.Relacion_Alto;
        Publicidad.Ancho += Pantalla.Relacion_Ancho;
        Publicidad.X      = Pantalla.Ancho / 2 - Publicidad.Ancho / 2;
        Publicidad.Y      = (Pantalla.Alto - Publicidad.Cero_Y - Marquesina.Alto) / 2 + Publicidad.Cero_Y - Publicidad.Alto / 2;

        ui->Propaganda_1->setGeometry( Publicidad.X , Publicidad.Y , Publicidad.Ancho , Publicidad.Alto );
    }

    if ( Efectos.Contador_Timer == Publicidad.Duracion * 1000 / (8 * Efectos.Timer) )
    {
        Efectos.Estado         = EFECTO_DEZPLAZAR;
        Efectos.Contador_Timer = 0;
    }

    TituloPublicidad();
    MarquesinaPublicidad();

}

/*************************************************************************************************************************************/

/**
 \fn void MostrarPublicidad::EfectoDesplazar ( void )
 \brief Efecto Desplazar.
 \note Se maneja la aplicacion del efecto dinamicos de Desplazar a las distintas publicidades tanto Fijas como Variables en la que la
       misma es desplazada desde el margen derecho hacia el margen izquierdo hasta que la publicidad ocupa toda la pantalla.
*/

void MostrarPublicidad::EfectoDesplazar ( void )
{
    if ( Efectos.Contador_Timer == 0 )
    {
        Publicidad.Ancho = Pantalla.Ancho;
        Publicidad.Alto  = Pantalla.Alto - Publicidad.Cero_Y - Marquesina.Alto;
        Publicidad.X     = Pantalla.Ancho;
        Publicidad.Y     = Publicidad.Cero_Y;

        switch ( Publicidad.Tipo )
        {
            case PUBLICIDAD_FIJA :      ui->Propaganda_1->setPixmap(QPixmap("extras/publicidadFija/1.jpg"));
                                        break;

            case PUBLICIDAD_VARIABLE :  ui->Propaganda_1->setPixmap(QPixmap(Variable.nombrePublicides[1]));
                                        break;

            default :                   break;
        }

        ui->Propaganda_1->adjustSize( );
        ui->Propaganda_1->setScaledContents( true );
        ui->Propaganda_1->setGeometry( Publicidad.X , Publicidad.Y , Publicidad.Ancho , Publicidad.Alto );

        ui->Propaganda_2->setGeometry( 0 , 0 , 1 , 1 );
    }

    Efectos.Contador_Timer++;

    if ( Publicidad.X > 0 )
    {
        Publicidad.X -= 25;

        ui->Propaganda_1->setGeometry( Publicidad.X , Publicidad.Y , Publicidad.Ancho , Publicidad.Alto );
    }

    if ( Publicidad.X <= 0 )
    {
        Publicidad.Ancho = Pantalla.Ancho;
        Publicidad.Alto  = Pantalla.Alto - Publicidad.Cero_Y - Marquesina.Alto;
        Publicidad.X     = 0;
        Publicidad.Y     = Publicidad.Cero_Y;

        ui->Propaganda_1->setGeometry( Publicidad.X , Publicidad.Y , Publicidad.Ancho , Publicidad.Alto );
    }

    if ( Efectos.Contador_Timer >= Publicidad.Duracion*1000/(8*Efectos.Timer) )
    {
        Efectos.Estado = EFECTO_OPACIDAD;
        Efectos.Contador_Timer = 0;
    }

    TituloPublicidad();
    MarquesinaPublicidad();
}

/*************************************************************************************************************************************/

/**
 \fn void MostrarPublicidad::EfectoOpacidad ( void )
 \brief Efecto Opacidad.
 \note Se maneja la aplicacion del efecto dinamicos de Opacidad a las distintas publicidades tanto Fijas como Variables en la que la
       publicidad inicia con cierto valor de opacidad y va disminuyendo hasta que deja de ser opaco por completo.
*/

void MostrarPublicidad::EfectoOpacidad ( void )
{
    if ( Efectos.Contador_Timer == 0 )
    {
        Publicidad.Ancho = Pantalla.Ancho;
        Publicidad.Alto = Pantalla.Alto - Publicidad.Cero_Y - Marquesina.Alto;
        Publicidad.X = 0;
        Publicidad.Y = Publicidad.Cero_Y;

        switch ( Publicidad.Tipo )
        {
            case PUBLICIDAD_FIJA :      ui->Propaganda_1->setPixmap(QPixmap("extras/publicidadFija/2.jpg"));
                                        break;

            case PUBLICIDAD_VARIABLE :  ui->Propaganda_1->setPixmap(QPixmap(Variable.nombrePublicides[2]));
                                        break;

            default :                   break;
        }

        ui->Propaganda_1->adjustSize( );
        ui->Propaganda_1->setScaledContents( true );
        ui->Propaganda_1->setGeometry( Publicidad.X , Publicidad.Y , Publicidad.Ancho , Publicidad.Alto );

        ui->Propaganda_2->setGeometry( 0 , 0 , 1 , 1 );
    }

    Efectos.Contador_Timer++;

    QGraphicsOpacityEffect *Opaco;

    Opaco = new QGraphicsOpacityEffect (this);
    Opaco->setOpacity(Publicidad.IndiceOpacidad);
    ui->Propaganda_1->setGraphicsEffect(Opaco);

    if ( Publicidad.IndiceOpacidad < 1 )
    {
        Publicidad.IndiceOpacidad += Publicidad.SaltoOpacidad;
    }

    if ( Efectos.Contador_Timer >= Publicidad.Duracion*1000/(8*Efectos.Timer) )
    {
        Efectos.Estado = EFECTO_CORTINA;
        Efectos.Contador_Timer = 0;

        Publicidad.IndiceOpacidad = 0;
    }

    TituloPublicidad();
    MarquesinaPublicidad();

}

/*************************************************************************************************************************************/

/**
 \fn void MostrarPublicidad::EfectoCortina ( void )
 \brief Efecto Cortina.
 \note Se maneja la aplicacion del efecto dinamico de desplazar cuatro figuras geometricas para generar un efecto similar al de abrir
       dos cortinas pero en este caso tanto horizontal como vertical.
*/

void MostrarPublicidad::EfectoCortina ( void )
{
    if ( Efectos.Contador_Timer == 0 )
    {
        Publicidad.Ancho = Pantalla.Ancho;
        Publicidad.Alto = Pantalla.Alto - Publicidad.Cero_Y - Marquesina.Alto;
        Publicidad.X = 0;
        Publicidad.Y = Publicidad.Cero_Y;

        Cuadrado.Alto = (Pantalla.Alto - Publicidad.Cero_Y - Marquesina.Alto) / 2;
        Cuadrado.Ancho = Pantalla.Ancho / 2;
        Cuadrado.X1 = 0;
        Cuadrado.X2 = Pantalla.Ancho - Cuadrado.Ancho;
        Cuadrado.X3 = 0;
        Cuadrado.X4 = Pantalla.Ancho - Cuadrado.Ancho;
        Cuadrado.Y1 = Publicidad.Cero_Y;
        Cuadrado.Y2 = Publicidad.Cero_Y;
        Cuadrado.Y3 = Publicidad.Cero_Y + Cuadrado.Alto;
        Cuadrado.Y4 = Publicidad.Cero_Y + Cuadrado.Alto;

        switch ( Publicidad.Tipo )
        {
            case PUBLICIDAD_FIJA :      ui->Propaganda_2->setPixmap(QPixmap("extras/publicidadFija/3.jpg"));
                                        break;

            case PUBLICIDAD_VARIABLE :  ui->Propaganda_2->setPixmap(QPixmap(Variable.nombrePublicides[3]));
                                        break;

            default :                   break;
        }

        ui->Propaganda_2->adjustSize( );
        ui->Propaganda_2->setScaledContents( true );
        ui->Propaganda_2->setGeometry( Publicidad.X , Publicidad.Y , Publicidad.Ancho , Publicidad.Alto );

        ui->Propaganda_1->setGeometry( 0 , 0 , 1 , 1 );

        ui->Cuadrado_1->setGeometry( Cuadrado.X1 , Cuadrado.Y1 , Cuadrado.Ancho , Cuadrado.Alto );
        ui->Cuadrado_2->setGeometry( Cuadrado.X2 , Cuadrado.Y2 , Cuadrado.Ancho , Cuadrado.Alto );
        ui->Cuadrado_3->setGeometry( Cuadrado.X3 , Cuadrado.Y3 , Cuadrado.Ancho , Cuadrado.Alto );
        ui->Cuadrado_4->setGeometry( Cuadrado.X4 , Cuadrado.Y4 , Cuadrado.Ancho , Cuadrado.Alto );
    }

    Efectos.Contador_Timer++;

    Cuadrado.Ancho -= Pantalla.Relacion_Ancho / 4;
    Cuadrado.Alto  -= Pantalla.Relacion_Alto / 4;
    Cuadrado.X2    += Pantalla.Relacion_Ancho / 4;
    Cuadrado.X4    += Pantalla.Relacion_Ancho / 4;
    Cuadrado.Y3    += Pantalla.Relacion_Alto / 4;
    Cuadrado.Y4    += Pantalla.Relacion_Alto / 4;

    ui->Cuadrado_1->setGeometry( Cuadrado.X1 , Cuadrado.Y1 , Cuadrado.Ancho , Cuadrado.Alto );
    ui->Cuadrado_2->setGeometry( Cuadrado.X2 , Cuadrado.Y2 , Cuadrado.Ancho , Cuadrado.Alto );
    ui->Cuadrado_3->setGeometry( Cuadrado.X3 , Cuadrado.Y3 , Cuadrado.Ancho , Cuadrado.Alto );
    ui->Cuadrado_4->setGeometry( Cuadrado.X4 , Cuadrado.Y4 , Cuadrado.Ancho , Cuadrado.Alto );

    if ( Efectos.Contador_Timer >= Publicidad.Duracion*1000/(8*Efectos.Timer) )
    {
        if ( Publicidad.Tipo == PUBLICIDAD_FIJA )
        {
            // Terminar si estan desactivadas las Publicidades Locales

            if (habilitacionPublicidad == PUBLICIDAD_OFF)
            {
                Efectos.Estado = FIN_ESPACIO_PUBLICITARIO;
                Efectos.Contador_Timer = 0;
                return;
            }

            Publicidad.Tipo = PUBLICIDAD_VARIABLE;

            Efectos.Estado = EFECTO_ZOOM_IN;
            Efectos.Contador_Timer = 0;

            Cuadrado.Alto = 1;
            Cuadrado.Ancho = 1;
            Cuadrado.X1 = 0;
            Cuadrado.X2 = Pantalla.Ancho - Cuadrado.Ancho;
            Cuadrado.X3 = 0;
            Cuadrado.X4 = Pantalla.Ancho - Cuadrado.Ancho;
            Cuadrado.Y1 = Publicidad.Cero_Y;
            Cuadrado.Y2 = Publicidad.Cero_Y;
            Cuadrado.Y3 = Publicidad.Cero_Y + Cuadrado.Alto;
            Cuadrado.Y4 = Publicidad.Cero_Y + Cuadrado.Alto;

            ui->Cuadrado_1->setGeometry( Cuadrado.X1 , Cuadrado.Y1 , Cuadrado.Ancho , Cuadrado.Alto );
            ui->Cuadrado_2->setGeometry( Cuadrado.X2 , Cuadrado.Y2 , Cuadrado.Ancho , Cuadrado.Alto );
            ui->Cuadrado_3->setGeometry( Cuadrado.X3 , Cuadrado.Y3 , Cuadrado.Ancho , Cuadrado.Alto );
            ui->Cuadrado_4->setGeometry( Cuadrado.X4 , Cuadrado.Y4 , Cuadrado.Ancho , Cuadrado.Alto );
        }

        else if ( Publicidad.Tipo == PUBLICIDAD_VARIABLE )
        {
            Efectos.Estado = FIN_ESPACIO_PUBLICITARIO;
            Efectos.Contador_Timer = 0;
        }
    }

    TituloPublicidad();
    MarquesinaPublicidad();

}

/*************************************************************************************************************************************/

/**
 \fn void MostrarPublicidad::TituloPublicidad ( void )
 \brief Titulo de la Publicidad.
 \note Se maneja la aplicacion del efecto dinamicos del Titulo de las Publicidades.
*/

void MostrarPublicidad::TituloPublicidad ( void )
{
    if ( Titulo.EstadoInicial == 0 )
    {
        PosicionInicial ( );
        TextoTitulo     ( );
        FuenteTitulo    ( );
    }

    Titulo.EstadoInicial = 1;

    switch ( Titulo.Efecto )
    {
        case EFECTO_TITULO_SUBIR :  if ( Titulo.Y <= Publicidad.Cero_Y/2 )
                                    {
                                        PosicionTitulo ( );
                                        ColorTitulo    ( 0 );

                                        Titulo.Y+=2;

                                        if ( Titulo.Y == Publicidad.Cero_Y/2 )
                                        {
                                            Titulo.Efecto = EFECTO_TITULO_BAJAR;
                                        }
                                    }
                                    break;

        case EFECTO_TITULO_BAJAR :  if ( Titulo.Y <= Publicidad.Cero_Y/2 )
                                    {
                                        PosicionTitulo ( );
                                        ColorTitulo    ( 1 );

                                        Titulo.Y -= 2;
                                        if ( Titulo.Y == 0 )
                                        {
                                            Titulo.Efecto = EFECTO_TITULO_SUBIR;
                                        }
                                    }
                                    break;

        default : break;
    }
}

/*************************************************************************************************************************************/

/**
 \fn void MostrarPublicidad::PosicionInicial ( void )
 \brief Posicion Inicial del Titulo.
 \note Se setea la Posicion Inicial del titulo de las publicidades al comenzar el efecto de rebote alternado de las mismas.
*/

void MostrarPublicidad::PosicionInicial ( void )
{
    ui->Titulo_1->setGeometry(Titulo.X+0*Titulo.Interlineado,0,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_2->setGeometry(Titulo.X+1*Titulo.Interlineado,Titulo.Ancho,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_3->setGeometry(Titulo.X+2*Titulo.Interlineado,0,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_4->setGeometry(Titulo.X+3*Titulo.Interlineado,Titulo.Ancho,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_5->setGeometry(Titulo.X+4*Titulo.Interlineado,0,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_6->setGeometry(Titulo.X+5*Titulo.Interlineado,Titulo.Ancho,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_7->setGeometry(Titulo.X+6*Titulo.Interlineado,0,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_8->setGeometry(Titulo.X+7*Titulo.Interlineado+Titulo.Separacion,Titulo.Ancho,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_9->setGeometry(Titulo.X+8*Titulo.Interlineado+Titulo.Separacion,0,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_10->setGeometry(Titulo.X+9*Titulo.Interlineado+Titulo.Separacion,Titulo.Ancho,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_11->setGeometry(Titulo.X+10*Titulo.Interlineado+Titulo.Separacion,0,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_12->setGeometry(Titulo.X+11*Titulo.Interlineado+Titulo.Separacion,Titulo.Ancho,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_13->setGeometry(Titulo.X+12*Titulo.Interlineado+Titulo.Separacion,0,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_14->setGeometry(Titulo.X+13*Titulo.Interlineado+Titulo.Separacion,Titulo.Ancho,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_15->setGeometry(Titulo.X+14*Titulo.Interlineado+Titulo.Separacion,0,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_16->setGeometry(Titulo.X+15*Titulo.Interlineado+Titulo.Separacion,Titulo.Ancho,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_17->setGeometry(Titulo.X+16*Titulo.Interlineado+Titulo.Separacion,0,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_18->setGeometry(Titulo.X+17*Titulo.Interlineado+Titulo.Separacion,Titulo.Ancho,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_19->setGeometry(Titulo.X+18*Titulo.Interlineado+Titulo.Separacion,0,Titulo.Ancho,Titulo.Ancho);
}

/*************************************************************************************************************************************/

/**
 \fn void MostrarPublicidad::TextoTitulo ( void )
 \brief Texto del Titulo de las Publicidades.
 \note Se escribe el titulo de las publicidades caracter por caracter.
*/

void MostrarPublicidad::TextoTitulo ( void )
{
    ui->Titulo_1-> setText("E");
    ui->Titulo_2-> setText("S");
    ui->Titulo_3-> setText("P");
    ui->Titulo_4-> setText("A");
    ui->Titulo_5-> setText("C");
    ui->Titulo_6-> setText("I");
    ui->Titulo_7-> setText("O");
    ui->Titulo_8-> setText("P");
    ui->Titulo_9-> setText("U");
    ui->Titulo_10->setText("B");
    ui->Titulo_11->setText("L");
    ui->Titulo_12->setText("I");
    ui->Titulo_13->setText("C");
    ui->Titulo_14->setText("I");
    ui->Titulo_15->setText("T");
    ui->Titulo_16->setText("A");
    ui->Titulo_17->setText("R");
    ui->Titulo_18->setText("I");
    ui->Titulo_19->setText("O");
}

/*************************************************************************************************************************************/

/**
 \fn void MostrarPublicidad::FuenteTitulo ( void )
 \brief Fuente del texto del Titulo de las Publicidades.
 \note Se setea la fuente del titulo de las publicidades.
*/

void MostrarPublicidad::FuenteTitulo ( void )
{
    QFont Fuente("Arial",Titulo.Fuente,QFont::Bold);

    ui->Titulo_1-> setFont(Fuente);
    ui->Titulo_2-> setFont(Fuente);
    ui->Titulo_3-> setFont(Fuente);
    ui->Titulo_4-> setFont(Fuente);
    ui->Titulo_5-> setFont(Fuente);
    ui->Titulo_6-> setFont(Fuente);
    ui->Titulo_7-> setFont(Fuente);
    ui->Titulo_8-> setFont(Fuente);
    ui->Titulo_9-> setFont(Fuente);
    ui->Titulo_10->setFont(Fuente);
    ui->Titulo_11->setFont(Fuente);
    ui->Titulo_12->setFont(Fuente);
    ui->Titulo_13->setFont(Fuente);
    ui->Titulo_14->setFont(Fuente);
    ui->Titulo_15->setFont(Fuente);
    ui->Titulo_16->setFont(Fuente);
    ui->Titulo_17->setFont(Fuente);
    ui->Titulo_18->setFont(Fuente);
    ui->Titulo_19->setFont(Fuente);
}

/*************************************************************************************************************************************/

/**
 \fn void MostrarPublicidad::ColorTitulo ( void )
 \brief Color del texto del Titulo de las Publicidades.
 \note Se alterna el Color del titulo de las publicidades en los efectos de rebote del mismo.
*/

void MostrarPublicidad::ColorTitulo ( int Opcion )
{
    if ( Opcion == 0 )
    {
        QPalette Colores;

        Colores = ui->Titulo_1->palette();
        Colores.setColor(ui->Titulo_1->foregroundRole(), Qt::red);

        ui->Titulo_1-> setPalette(Colores);
        ui->Titulo_3-> setPalette(Colores);
        ui->Titulo_5-> setPalette(Colores);
        ui->Titulo_7-> setPalette(Colores);
        ui->Titulo_9-> setPalette(Colores);
        ui->Titulo_11->setPalette(Colores);
        ui->Titulo_13->setPalette(Colores);
        ui->Titulo_15->setPalette(Colores);
        ui->Titulo_17->setPalette(Colores);
        ui->Titulo_19->setPalette(Colores);

        Colores.setColor(ui->Titulo_1->foregroundRole(), Qt::yellow);

        ui->Titulo_2-> setPalette(Colores);
        ui->Titulo_4-> setPalette(Colores);
        ui->Titulo_6-> setPalette(Colores);
        ui->Titulo_8-> setPalette(Colores);
        ui->Titulo_10->setPalette(Colores);
        ui->Titulo_12->setPalette(Colores);
        ui->Titulo_14->setPalette(Colores);
        ui->Titulo_16->setPalette(Colores);
        ui->Titulo_18->setPalette(Colores);
    }

    if ( Opcion == 1 )
    {
        QPalette Colores;

        Colores = ui->Titulo_1->palette();
        Colores.setColor(ui->Titulo_1->foregroundRole(), Qt::yellow);

        ui->Titulo_1-> setPalette(Colores);
        ui->Titulo_3-> setPalette(Colores);
        ui->Titulo_5-> setPalette(Colores);
        ui->Titulo_7-> setPalette(Colores);
        ui->Titulo_9-> setPalette(Colores);
        ui->Titulo_11->setPalette(Colores);
        ui->Titulo_13->setPalette(Colores);
        ui->Titulo_15->setPalette(Colores);
        ui->Titulo_17->setPalette(Colores);
        ui->Titulo_19->setPalette(Colores);

        Colores.setColor(ui->Titulo_1->foregroundRole(), Qt::red);

        ui->Titulo_2-> setPalette(Colores);
        ui->Titulo_4-> setPalette(Colores);
        ui->Titulo_6-> setPalette(Colores);
        ui->Titulo_8-> setPalette(Colores);
        ui->Titulo_10->setPalette(Colores);
        ui->Titulo_12->setPalette(Colores);
        ui->Titulo_14->setPalette(Colores);
        ui->Titulo_16->setPalette(Colores);
        ui->Titulo_18->setPalette(Colores);
    }
}

/*************************************************************************************************************************************/

/**
 \fn void MostrarPublicidad::PosicionTitulo ( void )
 \brief Posicion del texto del Titulo de las Publicidades.
 \note Se modifica la Posicion en el efecto de rebote alternado del titulo de las publicidades.
*/

void MostrarPublicidad::PosicionTitulo ( void )
{
    ui->Titulo_1-> setGeometry(Titulo.X+0*Titulo.Interlineado,Titulo.Y,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_2-> setGeometry(Titulo.X+1*Titulo.Interlineado,Titulo.Ancho-Titulo.Y,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_3-> setGeometry(Titulo.X+2*Titulo.Interlineado,Titulo.Y,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_4-> setGeometry(Titulo.X+3*Titulo.Interlineado,Titulo.Ancho-Titulo.Y,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_5-> setGeometry(Titulo.X+4*Titulo.Interlineado,Titulo.Y,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_6-> setGeometry(Titulo.X+5*Titulo.Interlineado,Titulo.Ancho-Titulo.Y,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_7-> setGeometry(Titulo.X+6*Titulo.Interlineado,Titulo.Y,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_8-> setGeometry(Titulo.X+7*Titulo.Interlineado+Titulo.Separacion,Titulo.Ancho-Titulo.Y,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_9-> setGeometry(Titulo.X+8*Titulo.Interlineado+Titulo.Separacion,Titulo.Y,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_10->setGeometry(Titulo.X+9*Titulo.Interlineado+Titulo.Separacion,Titulo.Ancho-Titulo.Y,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_11->setGeometry(Titulo.X+10*Titulo.Interlineado+Titulo.Separacion,Titulo.Y,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_12->setGeometry(Titulo.X+11*Titulo.Interlineado+Titulo.Separacion,Titulo.Ancho-Titulo.Y,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_13->setGeometry(Titulo.X+12*Titulo.Interlineado+Titulo.Separacion,Titulo.Y,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_14->setGeometry(Titulo.X+13*Titulo.Interlineado+Titulo.Separacion,Titulo.Ancho-Titulo.Y,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_15->setGeometry(Titulo.X+14*Titulo.Interlineado+Titulo.Separacion,Titulo.Y,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_16->setGeometry(Titulo.X+15*Titulo.Interlineado+Titulo.Separacion,Titulo.Ancho-Titulo.Y,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_17->setGeometry(Titulo.X+16*Titulo.Interlineado+Titulo.Separacion,Titulo.Y,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_18->setGeometry(Titulo.X+17*Titulo.Interlineado+Titulo.Separacion,Titulo.Ancho-Titulo.Y,Titulo.Ancho,Titulo.Ancho);
    ui->Titulo_19->setGeometry(Titulo.X+18*Titulo.Interlineado+Titulo.Separacion,Titulo.Y,Titulo.Ancho,Titulo.Ancho);
}

/*************************************************************************************************************************************/

/**
 \fn void MostrarPublicidad::MarquesinaPublicidad ( void )
 \brief Marquesina de las Publicidades.
 \note Se implementa un marquesina en la parte inferior de la pantalla para publicitar la publicidades variables que el agenciero
       puede agregar o modificar a su gusto o conveniencia.
*/

void MostrarPublicidad::MarquesinaPublicidad ( void )
{
    QPalette Colores = ui->Marquesina->palette();
    Colores.setColor(ui->Marquesina->backgroundRole(), Qt::blue);
    ui->Marquesina->setPalette(Colores);

    ui->Marquesina->adjustSize();

    Marquesina.X -= 5;

    if ( Marquesina.X <= -(Pantalla.Ancho+50) )
    {
        Marquesina.X = Pantalla.Ancho;
    }

    ui->Marquesina->setGeometry(Marquesina.X,Marquesina.Y,Pantalla.Ancho+50,Marquesina.Alto);
}
