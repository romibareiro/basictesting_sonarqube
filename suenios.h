#ifndef SUENIOS_H
#define SUENIOS_H

#include <QDialog>
#include <QPalette>
#include <QGraphicsBlurEffect>
#include "database.h"
#include <QApplication>
#include <QtGui>
#include <QMovie>
#include <QDebug>
#include <QTime>
#include <define.h>

#define RESOLUCION_1920x1080 0
#define RESOLUCION_1280x720  1
#define RESOLUCION_1024x768  2
#define RESOLUCION_768x576   3
#define RESOLUCION_720x480   4

class SueniosThread : public QThread
{
    Q_OBJECT
public:
//    explicit jackpotThread(QObject *parent = 0);
    explicit SueniosThread();
    void run();
signals:

public slots:

};

namespace Ui {
class Suenios;
}

class Suenios : public QDialog
{
    Q_OBJECT

public:
    explicit Suenios(QWidget *parent = 0);
    QTimer *TimerNroSueno;

    typedef struct _PANTALLA
    {
        int Alto;
        int Ancho;
    }PANTALLA;

    PANTALLA Pantalla;

    typedef struct _IMAGENES
    {
        int X1;
        int X2;
        int Y1;
        int Y2;
        int Y3;
        int Y4;
        int Y5;
        int Alto;
        int Ancho;
    }IMAGENES;

    IMAGENES Imagenes;

    typedef struct _NOMBRES
    {
        int X1;
        int X2;
        int Y1;
        int Y2;
        int Y3;
        int Y4;
        int Y5;
        int Alto;
        int Ancho;
        int Fuente;
    }NOMBRES;

    NOMBRES Nombres;

    typedef struct _TITULO
    {
        int Alto;
        int Ancho;
        int X;
        int Y;
        int Fuente;
    }TITULO;

    TITULO Titulo;

    int ContadorSuenios;

    ~Suenios();

    QBasicTimer timer;
    void timerEvent(QTimerEvent*);

private:
    Ui::Suenios *ui;
    dataBaseSuenios *Nombres_DB;
    SueniosThread *t;

    int habilitacionSonido;


private slots:
    void ConfiguracionInicial( void );
    void Resolucion          ( void );
    void AjustarParametros   ( void );
    void PosicionarElementos ( void );
    void TextoTitulo         ( void );
    void MostrarNombres      ( void );
    void MostrarImagenes     ( void );
};

#endif // SUENIOS_H
