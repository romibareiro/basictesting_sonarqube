#include "alerta.h"
#include "ui_alerta.h"

alerta::alerta(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::alerta)
{
    ui->setupUi(this);

    porcentaje = 2;

    versionDisponible = getConfiguracion("versiondisponible");
    setConfiguracion("version", versionDisponible);

    ui->tituloSuperior->setText("Actualizacion Remota");
    ui->tituloInferior->setText("Conectandose al Servidor...");

    timer.start(600,this);

    this->close();

}

alerta::~alerta()
{
    delete ui;
}


void alerta::timerEvent(QTimerEvent *event)
{
        if(porcentaje > 20)
        {
            ui->tituloInferior->setText("Descargando Archivo..");
            porcentaje++;
        }
        if(porcentaje > 70)
        {
            ui->tituloInferior->setText("Descomprimiendo Archivo...");


        }
        if(porcentaje > 80)
        {
            ui->tituloInferior->setText("Verificando Archivo...");
            porcentaje++;
        }
        if(porcentaje > 90)
        {
            ui->tituloInferior->setText("Reiniciando...");
        }
        ui->barraProgreso->setValue(porcentaje);
        porcentaje += 3;

        if(porcentaje > 100)
                system("reboot");
}
