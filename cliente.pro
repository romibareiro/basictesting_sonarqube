#-------------------------------------------------
#
# Project created by QtCreator 2014-05-02T16:12:44
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cliente

target.files = cliente
target.path = /home/root

#extras.files += /home/fernando/Proyecto_Final/chiquitodelasuerte-code/trunk/cliente/extras/logo_loteria_buenos_aires.png
#extras.files += /home/fernando/Proyecto_Final/chiquitodelasuerte-code/trunk/cliente/extras/logo_loteria_nacional.png
#extras.files += /home/fernando/Proyecto_Final/chiquitodelasuerte-code/trunk/cliente/extras/bolita.png
#extras.files += /home/fernando/Proyecto_Final/chiquitodelasuerte-code/trunk/cliente/extras/ruleta.png
#extras.files += /home/fernando/Proyecto_Final/chiquitodelasuerte-code/trunk/cliente/extras/fondoCasino01.png
#extras.files += /home/fernando/Proyecto_Final/chiquitodelasuerte-code/trunk/cliente/extras/sonido_ruleta.wav
#extras.files += /home/fernando/Proyecto_Final/chiquitodelasuerte-code/trunk/cliente/extras/cliente.db
#extras.path = /home/root/extras

TEMPLATE = app
INSTALLS += target extras

SOURCES +=  main.cpp\
	    mainwindow.cpp\
	    visual.cpp\
	    configuracion.cpp\
            conexion.cpp\
            parser.cpp\
            database.cpp \
            fecha.cpp \
            suenios.cpp \
            juegosruleta.cpp \
            usbimages.cpp \
            popaladin.cpp \
            presentacion.cpp \
            mostrarpublicidad.cpp \
            jackpot.cpp \
            json.cpp \
            myTCPClient.cpp \
            configuracionkey.cpp \
            usbthread.cpp \
            alerta.cpp



HEADERS  += mainwindow.h\
            conexion.h\
            configuracion.h\
            parser.h\
            database.h \
            fecha.h \
            juegosruleta.h\
            suenios.h \
            usbthread.h \
            usbimages.h \
            popaladin.h \
            presentacion.h \
            mostrarpublicidad.h \
    jackpot.h \
    json.h \
    myTCPClient.h \
    define.h \
    alerta.h


FORMS    += mainwindow.ui\
	    configuracion.ui \
            fecha.ui \
            juegosruleta.ui\
            suenios.ui \
            usbimages.ui \
            popaladin.ui \
            presentacion.ui \
            mostrarpublicidad.ui \
    jackpot.ui \
    alerta.ui

LIBS += -l sqlite3
