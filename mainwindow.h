#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "stdio.h"
#include "sqlite3.h"
#include "qglobal.h"

#include <QMainWindow>
#include <QMessageBox>
#include <QTimer>
#include <QKeyEvent>

#include <configuracion.h>
#include <fecha.h>
#include <database.h>
#include <conexion.h>
#include <juegosruleta.h>
#include <suenios.h>
#include <popaladin.h>
#include <presentacion.h>
#include <mostrarpublicidad.h>
#include <jackpot.h>
#include <myTCPClient.h>
#include "usbimages.h"
#include <define.h>
#include <alerta.h>


#define ARCHIVO_AUXILIAR "quiniela.txt"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    int modificarProvincia();
    int modificarPresentacion();

    MyTCPClient * myCliente;

    class databaseQuiniela dbQuiniela;

    char path[50];

    int timerPrincipal;
    bool plataformaEjecucion;
    bool ocupadoTimer;

    int esquemaPOP;

    int formaPresentacion;

    int habilitacionJuegos;

    int seleccionQuiniela;

    int seleccionSorteo;

//    void mostrarNumeros();
    void checkPulsador();

    void borrarDataBase();
    void cerrarVentana();

    void mostrarQuinielaA( databaseQuiniela * );
    void mostrarQuinielaB( databaseQuiniela * );
    void mostrarQuinielaC( databaseQuiniela * );
    void mostrarQuinielaD( databaseQuiniela * );
    void mostrarCabezas( databaseQuiniela * , int );

    void keyPressEvent(QKeyEvent* event);

    int obtenerQuiniela(QString nombreSorteo, QString tipoSorteo, QString fechaSorteo);

    void mostrar_reloj(int tipoSorteo, int hora, int minutos);
    void maquinaEstadosCierreApuestas(int hora, int minutos, int segundos);


    QBasicTimer timer;

    void timerEvent(QTimerEvent*);

    void HoraSorteo ( int , int , int );

    typedef struct _HORASORTEO
    {
        int Horas;
        int Minutos;
        int Segundos;
        QString Mensaje;
        QString Reloj;
    }HORA_SORTEO;

public slots:
    void onGPIO();
    void onMainWindow( int );

private:
    Ui::MainWindow *ui;
    Configuracion *conf;
    Fecha *fec;
    juegosRuleta *jueRul;
    Suenios *suen;
    popaladin *aladin;
    USBImages *pendrive;
    MostrarPublicidad *publicidad;
    jackpot * claseJackpot;
    alerta * alert;
//    QPalette * paletteGeneral;

};

void configuracionGPIO();
int convertirTipoQuiniela(char);

#endif // MAINWINDOW_H



