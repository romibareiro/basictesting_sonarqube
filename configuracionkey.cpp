#include "configuracion.h"
#include "ui_configuracion.h"

extern int serialNumber;

/***************************************************************************************************/
/* Evento de Presionar una Tecla                                                                   */
/***************************************************************************************************/

/**
 \fn void Configuracion::keyPressEvent(QKeyEvent* event)
 \brief Evento de Presionar una Tecla.
 \note Evento de Presionar una Tecla.
 \param [in] QKeyEvent* event: Informacion de la tecla pulsada.
*/

void Configuracion::keyPressEvent(QKeyEvent* event)
{
    tiempo = 0;
    switch (estado)
    {
    case ESTADO_INICIAL:
        switch (event->key())
        {
        case Qt::Key_1:
            estado = ESTADO_PROPIEDADES_NOMBRE_QUINIELA;

            quinielaCargaManual.manual = 1;

            quinielaCargaManual.seleccionarSorteoManual();

            limpiarColumna0();
            ui->tituloSuperior->setText(QString("Cargar Datos de la Quiniela ") + event->text());
            ui->configuracionOpcion1->setText("Nombre de Quiniela : ");
            ui->configuracionOpcion2->setText(quinielaCargaManual.nombreSorteo);
            ui->configuracionOpcion3->setText("Fecha de Quiniela : ");
            ui->configuracionOpcion4->setText(quinielaCargaManual.fechaSorteo);
            ui->configuracionOpcion5->setText("Tipo de Quiniela : ");
            ui->configuracionOpcion6->setText(quinielaCargaManual.tipoSorteo);
            ui->configuracionOpcion7->setText("Numero de Quiniela : ");
            ui->configuracionOpcion8->setText(quinielaCargaManual.numeroSorteo);

            ui->configuracionOpcion11->setText("Seleccionar Nombre de Quiniela : ");
            ui->configuracionOpcion12->setText("1 - Quiniela Nacional");
            ui->configuracionOpcion13->setText("2 - Quiniela de la Provincia de Buenos Aires");
            ui->configuracionOpcion14->setText("3 - Quiniela de la Provincia de Santa Fe");

            ui->configuracionOpcion10->setText("0 - Para Continuar");

            if(quinielaCargaManual.nombreSorteo == "Nacional")
                seleccionarOpcion1(2);

            if(quinielaCargaManual.nombreSorteo == "Buenos Aires")
                seleccionarOpcion1(3);

            if(quinielaCargaManual.nombreSorteo == "Santa Fe")
                seleccionarOpcion1(4);
            break;
        case Qt::Key_2:
            estado = ESTADO_PROPIEDADES_NOMBRE_QUINIELA;

            quinielaCargaManual.manual = 2;

            quinielaCargaManual.seleccionarSorteoManual();

            limpiarColumna0();
            ui->tituloSuperior->setText(QString("Cargar Datos de la Quiniela ") + event->text());
            ui->configuracionOpcion1->setText("Nombre de Quiniela : ");
            ui->configuracionOpcion2->setText(quinielaCargaManual.nombreSorteo);
            ui->configuracionOpcion3->setText("Fecha de Quiniela : ");
            ui->configuracionOpcion4->setText(quinielaCargaManual.fechaSorteo);
            ui->configuracionOpcion5->setText("Tipo de Quiniela : ");
            ui->configuracionOpcion6->setText(quinielaCargaManual.tipoSorteo);
            ui->configuracionOpcion7->setText("Numero de Quiniela : ");
            ui->configuracionOpcion8->setText(quinielaCargaManual.numeroSorteo);

            ui->configuracionOpcion11->setText("Seleccionar Nombre de Quiniela : ");
            ui->configuracionOpcion12->setText("1 - Quiniela Nacional");
            ui->configuracionOpcion13->setText("2 - Quiniela de la Provincia de Buenos Aires");
            ui->configuracionOpcion14->setText("3 - Quiniela de la Provincia de Santa Fe");

            ui->configuracionOpcion10->setText("0 - Para Continuar");

            if(quinielaCargaManual.nombreSorteo == "Nacional")
                seleccionarOpcion1(2);

            if(quinielaCargaManual.nombreSorteo == "Buenos Aires")
                seleccionarOpcion1(3);

            if(quinielaCargaManual.nombreSorteo == "Santa Fe")
                seleccionarOpcion1(4);
            break;
        case Qt::Key_3:
            estado = ESTADO_PROPIEDADES_NOMBRE_QUINIELA;

            quinielaCargaManual.manual = 3;

            quinielaCargaManual.seleccionarSorteoManual();

            limpiarColumna0();
            ui->tituloSuperior->setText(QString("Cargar Datos de la Quiniela ") + event->text());
            ui->configuracionOpcion1->setText("Nombre de Quiniela : ");
            ui->configuracionOpcion2->setText(quinielaCargaManual.nombreSorteo);
            ui->configuracionOpcion3->setText("Fecha de Quiniela : ");
            ui->configuracionOpcion4->setText(quinielaCargaManual.fechaSorteo);
            ui->configuracionOpcion5->setText("Tipo de Quiniela : ");
            ui->configuracionOpcion6->setText(quinielaCargaManual.tipoSorteo);
            ui->configuracionOpcion7->setText("Numero de Quiniela : ");
            ui->configuracionOpcion8->setText(quinielaCargaManual.numeroSorteo);

            ui->configuracionOpcion11->setText("Seleccionar Nombre de Quiniela : ");
            ui->configuracionOpcion12->setText("1 - Quiniela Nacional");
            ui->configuracionOpcion13->setText("2 - Quiniela de la Provincia de Buenos Aires");
            ui->configuracionOpcion14->setText("3 - Quiniela de la Provincia de Santa Fe");

            ui->configuracionOpcion10->setText("0 - Para Continuar");

            if(quinielaCargaManual.nombreSorteo == "Nacional")
                seleccionarOpcion1(2);

            if(quinielaCargaManual.nombreSorteo == "Buenos Aires")
                seleccionarOpcion1(3);

            if(quinielaCargaManual.nombreSorteo == "Santa Fe")
                seleccionarOpcion1(4);
            break;

        case Qt::Key_4:
            estado = ESTADO_PROPIEDADES_NOMBRE_QUINIELA;

            quinielaCargaManual.manual = 4;

            quinielaCargaManual.seleccionarSorteoManual();

            limpiarColumna0();
            ui->tituloSuperior->setText(QString("Cargar Datos de la Quiniela ") + event->text());
            ui->configuracionOpcion1->setText("Nombre de Quiniela : ");
            ui->configuracionOpcion2->setText(quinielaCargaManual.nombreSorteo);
            ui->configuracionOpcion3->setText("Fecha de Quiniela : ");
            ui->configuracionOpcion4->setText(quinielaCargaManual.fechaSorteo);
            ui->configuracionOpcion5->setText("Tipo de Quiniela : ");
            ui->configuracionOpcion6->setText(quinielaCargaManual.tipoSorteo);
            ui->configuracionOpcion7->setText("Numero de Quiniela : ");
            ui->configuracionOpcion8->setText(quinielaCargaManual.numeroSorteo);

            ui->configuracionOpcion11->setText("Seleccionar Nombre de Quiniela : ");
            ui->configuracionOpcion12->setText("1 - Quiniela Nacional");
            ui->configuracionOpcion13->setText("2 - Quiniela de la Provincia de Buenos Aires");
            ui->configuracionOpcion14->setText("3 - Quiniela de la Provincia de Santa Fe");
            ui->configuracionOpcion14->setText("4 - Quiniela de la Provincia de Cordoba");

            ui->configuracionOpcion10->setText("0 - Para Continuar");

            if(quinielaCargaManual.nombreSorteo == "Nacional")
                seleccionarOpcion1(2);

            if(quinielaCargaManual.nombreSorteo == "Buenos Aires")
                seleccionarOpcion1(3);

            if(quinielaCargaManual.nombreSorteo == "Santa Fe")
                seleccionarOpcion1(4);
            break;

        case Qt::Key_5:
            estado = ESTADO_NUMERO_DEL_DIA_A;
            limpiarColumna0();
            ui->tituloSuperior->setText("Seleccionar Numero del Dia");
            break;

        case Qt::Key_6: // Opcion para Cargar opciones del Pendrive
            pendrive = new USBImages;
            pendrive->exec();
            delete pendrive;
            close();
            break;

        case Qt::Key_7: // Opcion para la actualizacion del Software

            limpiarColumna0();
            ui->tituloSuperior->setText("Actualizacion del Software");
            ui->tituloInferior->setText("Buscando Actualizaciones...");
            versionActual = getConfiguracion("version");
            subVersion = versionActual%100;
            ui->configuracionOpcion1->setText("Version Actual del Software: " + QString::number( versionActual/100 ) + "." + QString::number( subVersion ));

            versionDisponible = getConfiguracion("versiondisponible");
            subVersion = versionDisponible%100;
            ui->configuracionOpcion2->setText("Version Disponible del Software: " + QString::number( versionDisponible/100 ) + "." + QString::number( subVersion ));


            if(versionActual == versionDisponible)
            {
                 ui->configuracionOpcion0->setText("Presione cualquier tecla para Salir");
                 estado = SALIR;
            }else
            {
                  ui->configuracionOpcion9->setText("Presione 1 para Actualizar");
                  ui->configuracionOpcion0->setText("Presione 0 para Salir");
                  estado = ESTADO_ACTUALIZACION;
            }

            break;

        case Qt::Key_8: // Opcion Acerca de TimBear
            estado = SALIR;
            ui->tituloSuperior->setText("Acerca de TimBear");
            ui->configuracionOpcion1->setText("Numero de Serial: "  + QString::number(serialNumber) );
            ui->configuracionOpcion2->setText("Version de Hardware: "  VERSION_HARDWARE);
            ui->configuracionOpcion3->setText("Version del Sistema Operativo: " VERSION_SISTEMA_OPERATIVO);
            versionActual = getConfiguracion("version");
            subVersion = versionActual%100;
            ui->configuracionOpcion4->setText("Version deSoftware: " + QString::number( versionActual/100 ) + "." + QString::number( subVersion ));
            ui->configuracionOpcion5->clear();
            ui->configuracionOpcion6->clear();
            ui->configuracionOpcion7->clear();
            ui->configuracionOpcion8->clear();
            ui->configuracionOpcion9->clear();
            ui->configuracionOpcion0->setText("Presione Cualquier Tecla para Salir");
            limpiarColumna1();

            break;

        case Qt::Key_9:
            estado = ESTADO_CONFIGURACION;
            ui->tituloSuperior->setText("Configuracion - Seleccionar Opcion");
            ui->configuracionOpcion1->setText("1 - Estilo");
            ui->configuracionOpcion2->setText("2 - Forma de Presentacion");
            ui->configuracionOpcion3->setText("3 - Provincia");
            ui->configuracionOpcion4->setText("4 - Manual o Automatico");
            ui->configuracionOpcion5->setText("5 - Habilitar Publicidad");
            ui->configuracionOpcion6->setText("6 - Habilitar Juegos");
            ui->configuracionOpcion7->setText("7 - Reproducir Sonidos");
            ui->configuracionOpcion8->setText("8 - Configurar Fecha y Hora");
            ui->configuracionOpcion9->clear();
            ui->configuracionOpcion0->setText("0 - Salir");
            break;

        case Qt::Key_0:
            close();
            break;
        default:
            break;
        }
        break;
    case ESTADO_ACTUALIZACION:
        switch (event->key()) {
        case Qt::Key_1:
            setConfiguracion("version", versionActual);
            close();
            break;
        case Qt::Key_0:
            close();
            break;
        }

        break;
    case ESTADO_CONFIGURACION:
        seleccionarOpcion0(event->key()-'0');
        switch (event->key()) {
        case Qt::Key_1:
            estado = ESTADO_ESTILO;
            opcionManual = getConfiguracion("estilo");
            seleccionarOpcion1(opcionManual);
            ui->tituloSuperior->setText("Seleccionar Estilo de Colores");
            ui->configuracionOpcion11->setText("1 - Estilo 1");
            ui->configuracionOpcion12->setText("2 - Estilo 2");
            ui->configuracionOpcion13->clear();
            ui->configuracionOpcion14->clear();
            ui->configuracionOpcion15->clear();
            ui->configuracionOpcion16->clear();
            ui->configuracionOpcion17->clear();
            ui->configuracionOpcion18->clear();
            ui->configuracionOpcion19->setText("9 - Guardar Configuracion");;
            ui->configuracionOpcion10->setText("0 - Salir");
            break;
        case Qt::Key_2:
            estado = ESTADO_PRESENTACION;
            opcionManual = getConfiguracion("presentacion");
            seleccionarOpcion1(opcionManual);
            ui->tituloSuperior->setText("Seleccionar Forma de Presentacion");
            ui->configuracionOpcion11->setText("1 - Cabezas de Quiniela");
            ui->configuracionOpcion12->setText("2 - Dos Quinela Completas");
            ui->configuracionOpcion13->setText("3 - Cabezas de Quiniela y Una Completa");
            ui->configuracionOpcion14->clear();
            ui->configuracionOpcion15->clear();
            ui->configuracionOpcion16->clear();
            ui->configuracionOpcion17->clear();
            ui->configuracionOpcion18->clear();
            ui->configuracionOpcion19->setText("9 - Guardar y Salir");
            ui->configuracionOpcion10->setText("0 - Salir sin Guardar");
            break;
        case Qt::Key_3:
            estado = ESTADO_PROVINCIA;
            opcionManual = getConfiguracion("provincia");
            seleccionarOpcion1(opcionManual);
            ui->tituloSuperior->setText("Seleccionar Provincia");
            ui->configuracionOpcion11->setText("1 - Capital Federal");
            ui->configuracionOpcion12->setText("2 - Provincia de Buenos Aires");
            ui->configuracionOpcion13->setText("3 - Provincia de Santa Fe");
            ui->configuracionOpcion14->setText("4 - Provincia de Cordoba");
            ui->configuracionOpcion15->clear();
            ui->configuracionOpcion16->clear();
            ui->configuracionOpcion17->clear();
            ui->configuracionOpcion18->clear();
            ui->configuracionOpcion19->setText("9 - Guardar y Salir");
            ui->configuracionOpcion10->setText("0 - Salir sin Guardar");
            break;
        case Qt::Key_4:
            estado = ESTADO_OPERACION;
            opcionManual = getConfiguracion("operacion");
            seleccionarOpcion1(opcionManual);
            ui->tituloSuperior->setText("Forma de Operacion");
            ui->configuracionOpcion11->setText("1 - Manual");
            ui->configuracionOpcion12->setText("2 - Automatico");
            ui->configuracionOpcion13->clear();
            ui->configuracionOpcion14->clear();
            ui->configuracionOpcion15->clear();
            ui->configuracionOpcion16->clear();
            ui->configuracionOpcion17->clear();
            ui->configuracionOpcion18->clear();
            ui->configuracionOpcion19->setText("9 - Guardar y Salir");
            ui->configuracionOpcion10->setText("0 - Salir");
            break;
        case Qt::Key_5:
            estado = ESTADO_PUBLICIDAD;
            opcionManual = getConfiguracion("publicidad");
            seleccionarOpcion1(opcionManual);
            ui->tituloSuperior->setText("Habilitar Publicidad");
            ui->configuracionOpcion11->setText("1 - Habilitar Publicidades");
            ui->configuracionOpcion12->setText("2 - Deshabilitar Publicidades");
            ui->configuracionOpcion13->clear();
            ui->configuracionOpcion14->clear();
            ui->configuracionOpcion15->clear();
            ui->configuracionOpcion16->clear();
            ui->configuracionOpcion17->clear();
            ui->configuracionOpcion18->clear();
            ui->configuracionOpcion19->setText("9 - Guardar y Salir");
            ui->configuracionOpcion10->setText("0 - Salir sin Guardar");
            break;
        case Qt::Key_6:
            estado = ESTADO_JUEGOS;
            opcionManual = getConfiguracion("juegos");
            seleccionarOpcion1(opcionManual);
            ui->tituloSuperior->setText("Habilitar Juegos");
            ui->configuracionOpcion11->setText("1 - Habilitar Juegos");
            ui->configuracionOpcion12->setText("2 - Deshabilitar Juegos");
            ui->configuracionOpcion13->clear();
            ui->configuracionOpcion14->clear();
            ui->configuracionOpcion15->clear();
            ui->configuracionOpcion16->clear();
            ui->configuracionOpcion17->clear();
            ui->configuracionOpcion18->clear();
            ui->configuracionOpcion19->setText("9 - Guardar y Salir");
            ui->configuracionOpcion10->setText("0 - Salir sin Guardar");
            break;
        case Qt::Key_7:
            estado = ESTADO_SONIDO;
            opcionManual = getConfiguracion("sonido");
            seleccionarOpcion1(opcionManual);
            ui->tituloSuperior->setText("Habilitar Sonido");
            ui->configuracionOpcion11->setText("1 - Habilitar Sonido");
            ui->configuracionOpcion12->setText("2 - Deshabilitar Sonido");
            ui->configuracionOpcion13->clear();
            ui->configuracionOpcion14->clear();
            ui->configuracionOpcion15->clear();
            ui->configuracionOpcion16->clear();
            ui->configuracionOpcion17->clear();
            ui->configuracionOpcion18->clear();
            ui->configuracionOpcion19->setText("9 - Guardar y Salir");
            ui->configuracionOpcion10->setText("0 - Salir sin Guardar");
            break;
        case Qt::Key_8:
            estado = ESTADO_FECHA_ACTUAL;
            posicion = 0;
            seleccionarOpcion1(1);
            ui->tituloSuperior->setText("Configurar Fecha y Hora");
            ui->configuracionOpcion11->setText("Fecha: " + fechaActual);
            ui->configuracionOpcion12->setText("Hora: " + horaActual);
            ui->configuracionOpcion13->clear();
            ui->configuracionOpcion14->clear();
            ui->configuracionOpcion15->clear();
            ui->configuracionOpcion16->clear();
            ui->configuracionOpcion17->clear();
            ui->configuracionOpcion18->clear();
            ui->configuracionOpcion19->clear();
            ui->configuracionOpcion10->clear();
            break;
        case Qt::Key_0:
            close();
            break;
        default:
            break;
        }
        break;

    case ESTADO_ESTILO:
        switch (event->key()) {
        case Qt::Key_1:
            seleccionarOpcion1(1);
            opcionManual = 1;
            break;
        case Qt::Key_2:
            seleccionarOpcion1(2);
            opcionManual = 2;
            break;
        case Qt::Key_9:
// Guardar Configuracion
            setConfiguracion( "estilo" , opcionManual );
            close();
            break;
        case Qt::Key_0:
            close();
            break;
        default:
            break;
        }
        break;

    case ESTADO_PRESENTACION:
        switch (event->key()) {
        case Qt::Key_1:
            opcionManual = 1;
            seleccionarOpcion1(1);
            break;
        case Qt::Key_2:
            opcionManual = 2;
            seleccionarOpcion1(2);
            break;
        case Qt::Key_3:
            opcionManual = 3;
            seleccionarOpcion1(3);
            break;
        case Qt::Key_9:
// Guardar Configuracion
            setConfiguracion( "presentacion" , opcionManual );
            close();
            break;
        case Qt::Key_0:
            close();
            break;
        default:
            break;
        }
        break;

    case ESTADO_PROVINCIA:
        switch (event->key()) {
        case Qt::Key_1:
            seleccionarOpcion1(1);
            opcionManual = 1;
            break;
        case Qt::Key_2:
            seleccionarOpcion1(2);
            opcionManual = 2;
            break;
        case Qt::Key_3:
            seleccionarOpcion1(3);
            opcionManual = 3;
            break;
        case Qt::Key_4:
            seleccionarOpcion1(4);
            opcionManual = 4;
            break;
        case Qt::Key_9:
// Guardar Configuracion
            setConfiguracion( "provincia" , opcionManual );
            close();
            break;
        case Qt::Key_0:
            close();
            break;
        default:
            break;
        }
        break;

    case ESTADO_OPERACION:
        switch (event->key()) {
        case Qt::Key_1:
            seleccionarOpcion1(1);
            opcionManual = 1;
            break;
        case Qt::Key_2:
            seleccionarOpcion1(2);
            opcionManual = 2;
            break;
        case Qt::Key_9:
// Guardar Configuracion
            setConfiguracion( "operacion" , opcionManual );
            close();
            break;
        case Qt::Key_0:
            close();
            break;
        default:
            break;
        }
        break;

    case ESTADO_PUBLICIDAD:
        switch (event->key()) {
        case Qt::Key_1:
            seleccionarOpcion1(1);
            opcionManual = 1;
            break;
        case Qt::Key_2:
            seleccionarOpcion1(2);
            opcionManual = 2;
            break;
        case Qt::Key_9:
// Guardar Configuracion
            setConfiguracion( "publicidad" , opcionManual );
            close();
            break;
        case Qt::Key_0:
            close();
            break;
        default:
            break;
        }
        break;

    case ESTADO_JUEGOS:
        switch (event->key()) {
        case Qt::Key_1:
            seleccionarOpcion1(1);
            opcionManual = 1;
            break;
        case Qt::Key_2:
            seleccionarOpcion1(2);
            opcionManual = 2;
            break;
        case Qt::Key_9:
// Guardar Configuracion
            setConfiguracion( "juegos" , opcionManual );
            close();
            break;
        case Qt::Key_0:
            close();
            break;
        default:
            break;
        }
        break;

    case ESTADO_SONIDO:
        switch (event->key()) {
        case Qt::Key_1:
            seleccionarOpcion1(1);
            opcionManual = SONIDO_ON;
            break;
        case Qt::Key_2:
            seleccionarOpcion1(2);
            opcionManual = SONIDO_OFF;
            break;
        case Qt::Key_9:
// Guardar Configuracion
            setConfiguracion( "sonido" , opcionManual );
            close();
            break;
        case Qt::Key_0:
            close();
            break;
        default:
            break;
        }
        break;

    case ESTADO_FECHA_ACTUAL:
        if(event->key() >= '0' && event->key() <= '9')
        {
            fechaActual.replace(posicion,1,event->key());
            ui->configuracionOpcion11->setText("Fecha: " + fechaActual);

            posicion++;

            if( posicion == 2 || posicion == 5 )
                posicion++;

            if(posicion > 7)
            {
                posicion = 0;
                seleccionarOpcion1(2);
                estado = ESTADO_HORA_ACTUAL;
            }
        }
        break;
    case ESTADO_HORA_ACTUAL:
        if(event->key() >= '0' && event->key() <= '9')
        {
            horaActual.replace(posicion,1,event->key());
            ui->configuracionOpcion12->setText("Hora: " + horaActual);

            posicion++;

            if( posicion == 2 )
                posicion++;

            if(posicion > 4)
            {
                aux = "date -s \"" + fechaActual + " " + horaActual +"\"" ;

                qDebug() << aux.toLatin1();

                seleccionarOpcion1(3);

                if( system(aux.toLatin1() ) )
                    ui->configuracionOpcion13->setText("Fecha y/o Hora Invalida");
                else
                    ui->configuracionOpcion13->setText("Fecha y/o Hora Modificada Correctamente");

                ui->configuracionOpcion10->setText("Presione Cualquier Tecla para Salir");
                estado = SALIR;

            }
        }
        break;

    case ESTADO_PROPIEDADES_NOMBRE_QUINIELA:           // Propiedades de la Quiniela
        switch (event->key()) {
        case Qt::Key_1:
            seleccionarOpcion1(2);
            quinielaCargaManual.nombreSorteo = "Nacional";
            break;
        case Qt::Key_2:
            seleccionarOpcion1(3);
            quinielaCargaManual.nombreSorteo = "Buenos Aires";
            break;
        case Qt::Key_3:
            seleccionarOpcion1(4);
            quinielaCargaManual.nombreSorteo = "Santa Fe";
            break;
        case Qt::Key_4:
            seleccionarOpcion1(5);
            quinielaCargaManual.nombreSorteo = "Cordoba";
            break;
        case Qt::Key_0:
            limpiarColumna1();
            ui->configuracionOpcion2->setText(quinielaCargaManual.nombreSorteo);
            estado = ESTADO_PROPIEDADES_FECHA_QUINIELA;
            posicion = 0;
            flagGeneral = 1;
            break;
        default:
            break;
        }
        break;

    case ESTADO_PROPIEDADES_FECHA_QUINIELA:           // Propiedades de la Quiniela
        if(event->key() >= '0' && event->key() <= '9')
        {
            if(flagGeneral)
            {
                quinielaCargaManual.fechaSorteo.clear();
                flagGeneral = 0;
            }
            quinielaCargaManual.fechaSorteo += event->text();
            if(posicion == 1 || posicion == 3)
            {
                quinielaCargaManual.fechaSorteo += "-";
            }
            posicion ++;
            ui->configuracionOpcion4->setText(quinielaCargaManual.fechaSorteo);

        }
        if( ( event->key() == Qt::Key_Enter && posicion == 0) || posicion == 8)
        {
            estado = ESTADO_PROPIEDADES_TIPO_QUINIELA;
            ui->configuracionOpcion11->setText("Seleccionar Tipo de Quiniela : ");
            ui->configuracionOpcion12->setText("1 - Primera");
            ui->configuracionOpcion13->setText("2 - Matutina");
            ui->configuracionOpcion14->setText("3 - Vespertina");
            ui->configuracionOpcion15->setText("4 - Nocturna");

            ui->configuracionOpcion10->setText("0 - Para Continuar");

            if(quinielaCargaManual.tipoSorteo == "Primera")
                seleccionarOpcion1(2);

            if(quinielaCargaManual.tipoSorteo == "Matutina")
                seleccionarOpcion1(3);

            if(quinielaCargaManual.tipoSorteo == "Vespertina")
                seleccionarOpcion1(4);

            if(quinielaCargaManual.tipoSorteo == "Nocturna")
                seleccionarOpcion1(5);
        }
        break;

    case ESTADO_PROPIEDADES_TIPO_QUINIELA:           // Propiedades de la Quiniela
        switch (event->key()) {
        case Qt::Key_1:
            seleccionarOpcion1(2);
            quinielaCargaManual.tipoSorteo = "Primera";
            break;
        case Qt::Key_2:
            seleccionarOpcion1(3);
            quinielaCargaManual.tipoSorteo = "Matutina";
            break;
        case Qt::Key_3:
            seleccionarOpcion1(4);
            quinielaCargaManual.tipoSorteo = "Vespertina";
            break;
        case Qt::Key_4:
            seleccionarOpcion1(5);
            quinielaCargaManual.tipoSorteo = "Nocturna";
            break;
        case Qt::Key_0:
            limpiarColumna1();
            ui->configuracionOpcion6->setText(quinielaCargaManual.tipoSorteo);
            estado = ESTADO_PROPIEDADES_NUMERO_QUINIELA;
            posicion = 0;
            flagGeneral = 1;
            break;
        default:
            break;
        }
        break;

        case ESTADO_PROPIEDADES_NUMERO_QUINIELA:           // Propiedades de la Quiniela
        if(event->key() >= '0' && event->key() <= '9')
        {
            if(flagGeneral)
            {
                quinielaCargaManual.numeroSorteo.clear();
                flagGeneral = 0;
            }

            quinielaCargaManual.numeroSorteo += event->key();

            ui->configuracionOpcion8->setText(quinielaCargaManual.numeroSorteo);
            posicion ++;
        }
        if( event->key() == Qt::Key_Enter || posicion == 6)
        {
            estado = ESTADO_CARGAR_QUINIELA_CIFRA_A;
            seleccionarOpcion1(0);
            ui->configuracionOpcion1->setText("Numero 01 : ");
            ui->configuracionOpcion2->setText("Numero 02 : ");
            ui->configuracionOpcion3->setText("Numero 03 : ");
            ui->configuracionOpcion4->setText("Numero 04 : ");
            ui->configuracionOpcion5->setText("Numero 05 : ");
            ui->configuracionOpcion6->setText("Numero 06 : ");
            ui->configuracionOpcion7->setText("Numero 07 : ");
            ui->configuracionOpcion8->setText("Numero 08 : ");
            ui->configuracionOpcion9->setText("Numero 09 : ");
            ui->configuracionOpcion0->setText("Numero 10 : ");
            ui->configuracionOpcion11->setText("Numero 11 : ");
            ui->configuracionOpcion12->setText("Numero 12 : ");
            ui->configuracionOpcion13->setText("Numero 13 : ");
            ui->configuracionOpcion14->setText("Numero 14 : ");
            ui->configuracionOpcion15->setText("Numero 15 : ");
            ui->configuracionOpcion16->setText("Numero 16 : ");
            ui->configuracionOpcion17->setText("Numero 17 : ");
            ui->configuracionOpcion18->setText("Numero 18 : ");
            ui->configuracionOpcion19->setText("Numero 19 : ");
            ui->configuracionOpcion10->setText("Numero 20 : ");
            posicion = 0;
        }
        break;

    case ESTADO_CARGAR_QUINIELA_CIFRA_A:
        if(event->key() >= '0' && event->key() <= '9')
        {
            estado = ESTADO_CARGAR_QUINIELA_CIFRA_B;
            quinielaCargaManual.premiosSorteo[posicion] = event->key() ;
            visualizarNumeros();
        }
        if(event->key() == Qt::Key_Period && posicion)
        {
            estado = ESTADO_CARGAR_QUINIELA_CIFRA_A;
            posicion--;
            quinielaCargaManual.premiosSorteo[posicion] = "- - - -";
            visualizarNumeros();
        }
        break;
    case ESTADO_CARGAR_QUINIELA_CIFRA_B:
        if(event->key() >= '0' && event->key() <= '9')
        {
            estado = ESTADO_CARGAR_QUINIELA_CIFRA_C;
            quinielaCargaManual.premiosSorteo[posicion] += event->key();
            visualizarNumeros();
        }
        if(event->key() == Qt::Key_Period && posicion)
        {
            estado = ESTADO_CARGAR_QUINIELA_CIFRA_A;
            quinielaCargaManual.premiosSorteo[posicion] = "- - - -";
            visualizarNumeros();
        }
        break;
    case ESTADO_CARGAR_QUINIELA_CIFRA_C:
        if(event->key() >= '0' && event->key() <= '9')
        {
            estado = ESTADO_CARGAR_QUINIELA_CIFRA_D;
            quinielaCargaManual.premiosSorteo[posicion] += event->key();
            visualizarNumeros();
        }
        if(event->key() == Qt::Key_Period && posicion)
        {
            estado = ESTADO_CARGAR_QUINIELA_CIFRA_A;
            quinielaCargaManual.premiosSorteo[posicion] = "- - - -";
            visualizarNumeros();
        }
        break;
    case ESTADO_CARGAR_QUINIELA_CIFRA_D:
        if(event->key() >= '0' && event->key() <= '9')
        {
            estado = ESTADO_CARGAR_QUINIELA_CIFRA_A;
            quinielaCargaManual.premiosSorteo[posicion] += event->key();
            visualizarNumeros();
            posicion++;
            if(posicion == 20){
                limpiarColumna0();
                limpiarColumna1();
                seleccionarOpcion0(99);
                seleccionarOpcion1(99);
                ui->configuracionOpcion1->setText("Introducir Letra");
                ui->configuracionOpcion2->setText("1 - A B C D E F");
                ui->configuracionOpcion3->setText("2 - G H I J K L");
                ui->configuracionOpcion4->setText("3 - M N O P Q R");
                ui->configuracionOpcion5->setText("4 - S T U V W X");
                ui->configuracionOpcion6->setText("5 - Y Z");
                ui->configuracionOpcion9->setText("0 - Guardar sin Modificar");
                ui->tituloSuperior->setText("Letras Del Sorteo");
                posicion = 0;
                flagGeneral = 1;
                estado = ESTADO_GRUPO_LETRA;
                ui->tituloInferior->setText(quinielaCargaManual.letrasSorteo);
            }
        }
        if(event->key() == Qt::Key_Period && posicion)
        {
            estado = ESTADO_CARGAR_QUINIELA_CIFRA_A;
            quinielaCargaManual.premiosSorteo[posicion] = "- - - -";
            visualizarNumeros();
        }
        break;
    case ESTADO_GRUPO_LETRA:
        switch (event->key()) {
        case Qt::Key_1:
            if(flagGeneral)
            {
                quinielaCargaManual.letrasSorteo.clear();
                ui->tituloInferior->clear();
            }
            ui->configuracionOpcion2->setText("1 - A");
            ui->configuracionOpcion3->setText("2 - B");
            ui->configuracionOpcion4->setText("3 - C");
            ui->configuracionOpcion5->setText("4 - D");
            ui->configuracionOpcion6->setText("5 - E");
            ui->configuracionOpcion7->setText("6 - F");
            multiplicadorLetra = 1;
            estado = ESTADO_LETRA;
            break;
        case Qt::Key_2:
            if(flagGeneral)
            {
                quinielaCargaManual.letrasSorteo.clear();
                ui->tituloInferior->clear();
            }
            ui->configuracionOpcion2->setText("1 - G");
            ui->configuracionOpcion3->setText("2 - H");
            ui->configuracionOpcion4->setText("3 - I");
            ui->configuracionOpcion5->setText("4 - J");
            ui->configuracionOpcion6->setText("5 - K");
            ui->configuracionOpcion7->setText("6 - L");
            multiplicadorLetra = 2;
            estado = ESTADO_LETRA;
            break;
        case Qt::Key_3:
            if(flagGeneral)
            {
                quinielaCargaManual.letrasSorteo.clear();
                ui->tituloInferior->clear();
            }
            ui->configuracionOpcion2->setText("1 - M");
            ui->configuracionOpcion3->setText("2 - N");
            ui->configuracionOpcion4->setText("3 - O");
            ui->configuracionOpcion5->setText("4 - P");
            ui->configuracionOpcion6->setText("5 - Q");
            ui->configuracionOpcion7->setText("6 - R");
            multiplicadorLetra = 3;
            estado = ESTADO_LETRA;
            break;
        case Qt::Key_4:
            if(flagGeneral)
            {
                quinielaCargaManual.letrasSorteo.clear();
                ui->tituloInferior->clear();
            }
            ui->configuracionOpcion2->setText("1 - S");
            ui->configuracionOpcion3->setText("2 - T");
            ui->configuracionOpcion4->setText("3 - U");
            ui->configuracionOpcion5->setText("4 - V");
            ui->configuracionOpcion6->setText("5 - W");
            ui->configuracionOpcion7->setText("6 - X");
            multiplicadorLetra = 4;
            estado = ESTADO_LETRA;
            break;
        case Qt::Key_5:
            if(flagGeneral)
            {
                quinielaCargaManual.letrasSorteo.clear();
                ui->tituloInferior->clear();
            }
            ui->configuracionOpcion2->setText("1 - Y");
            ui->configuracionOpcion3->setText("2 - Z");
            ui->configuracionOpcion4->setText("");
            ui->configuracionOpcion5->setText("");
            ui->configuracionOpcion6->setText("");
            ui->configuracionOpcion7->setText("");
            multiplicadorLetra = 5;
            estado = ESTADO_LETRA;
            break;
        case Qt::Key_0:
            if(flagGeneral)
            {
                limpiarColumna0();
                limpiarColumna1();
                seleccionarOpcion0(99);
                seleccionarOpcion1(99);
                ui->configuracionOpcion1->setText("Quiniela Guardada");
                ui->configuracionOpcion2->setText("Presiones Cualquier Tecla para Salir");
                quinielaCargaManual.insertarSorteoManual();
                estado = SALIR;
            }
            break;
        default:
            if(flagGeneral)
            {
                quinielaCargaManual.letrasSorteo.clear();
                ui->tituloInferior->clear();
            }
            break;
        }
        flagGeneral = 0;
        ui->configuracionOpcion9->clear();
        break;
    case ESTADO_LETRA:
        if(multiplicadorLetra == 5){
            if(event->key() >= '1' && event->key() <= '2')
            {
                quinielaCargaManual.letrasSorteo = 'A' + (multiplicadorLetra - 1) * 6 + event->key() - '1';
                posicion ++;
                ui->configuracionOpcion2->setText("1 - A B C D E F");
                ui->configuracionOpcion3->setText("2 - G H I J K L");
                ui->configuracionOpcion4->setText("3 - M N O P Q R");
                ui->configuracionOpcion5->setText("4 - S T U V W X");
                ui->configuracionOpcion6->setText("5 - Y Z");
                ui->configuracionOpcion7->setText("");
                estado = ESTADO_GRUPO_LETRA;
            }
        }
        else
        {
            if(event->key() >= '1' && event->key() <= '6'){
                quinielaCargaManual.letrasSorteo += 'A' + (multiplicadorLetra - 1) * 6 + event->key() - '1';
                posicion ++;
                ui->configuracionOpcion2->setText("1 - A B C D E F");
                ui->configuracionOpcion3->setText("2 - G H I J K L");
                ui->configuracionOpcion4->setText("3 - M N O P Q R");
                ui->configuracionOpcion5->setText("4 - S T U V W X");
                ui->configuracionOpcion6->setText("5 - Y Z");
                ui->configuracionOpcion7->setText("");
                estado = ESTADO_GRUPO_LETRA;
            }
        }
        if (posicion == 4)
        {
            limpiarColumna0();
            limpiarColumna1();
            seleccionarOpcion0(99);
            seleccionarOpcion1(99);
            ui->configuracionOpcion1->setText("Quiniela Guardada");
            ui->configuracionOpcion2->setText("Presiones Cualquier Tecla para Salir");

// Cargar datos en la base de datos
/*
            sqlite3 *db;
            char direccion [100];
            memset (direccion,0,sizeof(direccion)); // blanquear vector
            sprintf (direccion,"extras/QUINIELERO.db");
            funcionesbd.abrirQuinieladb(direccion, &db);
*/


            quinielaCargaManual.insertarSorteoManual();

            estado = SALIR;
        }
        ui->tituloInferior->setText(quinielaCargaManual.letrasSorteo);

        break;
    case ESTADO_NUMERO_DEL_DIA_A:
        if(event->key() >= '0' && event->key() <= '9')
        {
            estado = ESTADO_NUMERO_DEL_DIA_B;
            numeroDia = event->key();
            ui->tituloInferior->setText(numeroDia);
        }
        break;
    case ESTADO_NUMERO_DEL_DIA_B:
        if(event->key() >= '0' && event->key() <= '9'){
            estado = SALIR;
            numeroDia += event->key();
            ui->tituloInferior->setText(numeroDia);
            ui->configuracionOpcion1->setText("Presiones Cualquier Tecla para Salir");

            setConfiguracion( "suerte" , numeroDia.toInt());

        }
        break;
    case SALIR:
        close();
        break;
    default:
        break;
    }
}
