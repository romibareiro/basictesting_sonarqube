#include "popaladin.h"
#include "ui_popaladin.h"

/**
 *\fn       popaladin::popaladin(QWidget *parent):QDialog(parent),ui(new Ui::popaladin)
 *\brief    Constructor.
 *\note     Constructor de la interfaz de Aladin en la que se muestra en pantalla el sorteo de un numero de cuatro cifras para que el
 *          apostador pueda apostar si asi lo desea.
 *\param    Ninguno.
 *\return   Nada.
*/

popaladin::popaladin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::popaladin)
{
    ui->setupUi(this);

    t = new aladinThread();

    opaco = new QGraphicsOpacityEffect (this);

    ConfiguracionInicial ( );

    t->start();

    timer.start(Reloj.Clock,this);

}

// ********************************************************************************************************************************* //

/**
 *\fn       popaladin::~popaladin()
 *\brief    Destructor.
 *\note     Destructor de la interfaz de Aladino.
 *\param    Ninguno.
 *\return   Nada.
*/


popaladin::~popaladin()
{
    delete ui;
}

// ********************************************************************************************************************************* //

/**
 *\fn       void popaladin::ConfiguracionInicial ( void )
 *\brief    Funcion para realizar la Configuracion Inicial..
 *\note     Se implementa la funcion para administrar todos los pasos de la configuracion inicial como la Resolucion de la pantalla,
 *          Ajustar los Parametros acorde a la resolucion de la pantalla configurada por el agenciero en la configuracion inicial al
 *          configurar el equipo y Posicionar los Elementos que componen el QDialog.
 *\param    Ninguno.
 *\return   Nada.
*/

void popaladin::ConfiguracionInicial ( void )
{
    Resolucion          ( );
    AjustarParametros   ( );
    PosicionarElementos ( );
}

// ********************************************************************************************************************************* //

/**
 *\fn       void popaladin::Resolucion ( void )
 *\brief    Funcion para ajustar la Resolucion de pantalla.
 *\note     Se ajusta la resolucion de la pantalla acorde a la resolucion seleccionada por el agenciero en la configuracion principal
 *          y se utiliza la misma para posicionar y redimensionar las demas posiciones y dimensiones del QDialog.
 *\param    Ninguno.
 *\return   Nada.
*/

void popaladin::Resolucion ( void )
{

    this->setGeometry( 0 , 0 , ANCHO_PANTALLA , ALTO_PANTALLA);

    ui->FondoPantalla->setPixmap(QPixmap("extras/FondoPopAladin.jpg"));
    ui->FondoPantalla->adjustSize( );
    ui->FondoPantalla->setScaledContents(false);
    ui->FondoPantalla->setGeometry( 0 , 0 , ANCHO_PANTALLA , ALTO_PANTALLA );

}

// ********************************************************************************************************************************* //

/**
 *\fn       void popaladin::AjustarParametros ( void )
 *\brief    Funcion para ajustar los parametros geometricos.
 *\note     Se ajustan los parametros geometricos como ancho, alto, posicion horizontal y vertical de cada uno de los Widget que
 *          componen el QDialog acorde a la resolucion de pantalla configurada por el agenciero.
 *\param    Ninguno.
 *\return   Nada.
*/

void popaladin::AjustarParametros ( void )
{
    Display.Alto  = 180;
    Display.Ancho = 110;
    Display.X1    = 345;
    Display.X2    = 505;
    Display.X3    = 665;
    Display.X4    = 825;
    Display.Y     = 270;

    Reloj.Clock    = 100;
    Reloj.Contador = 0;
}

// ********************************************************************************************************************************* //

/**
 *\fn       void popaladin::PosicionarElementos ( void )
 *\brief    Funcion para posicionar los Widget.
 *\note     Se posicionan los parametros geometricos como ancho, alto, posicion horizontal y vertical de cada uno de los Widget que
 *          componen el QDialog acorde a la resolucion de pantalla configurada por el agenciero.
 *\param    Ninguno.
 *\return   Nada.
*/

void popaladin::PosicionarElementos ( void )
{
    ui->lcd_miles->setGeometry( Display.X1 , Display.Y , Display.Ancho , Display.Alto );
    ui->lcd_centenas->setGeometry( Display.X2 , Display.Y , Display.Ancho , Display.Alto );
    ui->lcd_decenas->setGeometry( Display.X3 , Display.Y , Display.Ancho , Display.Alto );
    ui->lcd_unidades->setGeometry( Display.X4 , Display.Y , Display.Ancho , Display.Alto );

    ui->brillo->setPixmap(QPixmap("extras/destellos.png"));
    ui->brillo->setScaledContents(true);
    ui->brillo->setGeometry( 850 , 420 , 300, 250 );

    opaco->setOpacity(0);
    ui->brillo->setGraphicsEffect(opaco);
}

// ********************************************************************************************************************************* //

/**
 *\fn       void popaladin::timerEvent(QTimerEvent *event)
 *\brief    Funcion para temporizar los Widget del QDialog Aladin.
 *\note     Se implementa la funcion para que con un determinado periodo de tiempo para los Widget se impriman en pantalla un numero
 *          de cuatro cifras y que va variando al azar hasta que se detiene en uno.
 *\param    QTimerEvent *event.
 *\return   Nada.
*/

void popaladin::timerEvent(QTimerEvent *event)
{
    event = event;

    Reloj.Contador++;

    if ( Reloj.Contador <= 20 )
    {
        Display.Miles    = rand() % 10;
        Display.Centanas = rand() % 10;
        Display.Decenas  = rand() % 10;
        Display.Unidades = rand() % 10;

        ui->lcd_miles->display(Display.Miles);
        ui->lcd_centenas->display(Display.Centanas);
        ui->lcd_decenas->display(Display.Decenas);
        ui->lcd_unidades->display(Display.Unidades);
    }


    if( Reloj.Contador == 29 )
    {
        opaco->setOpacity(0);
        ui->brillo->setGraphicsEffect(opaco);
    }

    if( Reloj.Contador == 20 || Reloj.Contador == 28 )
    {
        opaco->setOpacity(0.1);
        ui->brillo->setGraphicsEffect(opaco);
    }
    if( Reloj.Contador == 21 || Reloj.Contador == 27 )
    {
        opaco->setOpacity(0.2);
        ui->brillo->setGraphicsEffect(opaco);
    }
    if( Reloj.Contador == 22 || Reloj.Contador == 26 )
    {
        opaco->setOpacity(0.4);
        ui->brillo->setGraphicsEffect(opaco);
    }
    if( Reloj.Contador == 23 || Reloj.Contador == 25 )
    {
        opaco->setOpacity(0.7);
        ui->brillo->setGraphicsEffect(opaco);
    }
    if( Reloj.Contador == 24 )
    {
        opaco->setOpacity(1);
        ui->brillo->setGraphicsEffect(opaco);
    }


    if ( Reloj.Contador == 100 )
    {
        Reloj.Contador = 0;
        timer.stop();
        close();
    }

//    system("mplayer extras/arabe.wav -endpos 1");
}

aladinThread::aladinThread() :
    QThread()
{
    qDebug() << "aladinThread::aladinThread() - Comienzo Constructor";
}


void aladinThread::run()
{
    qDebug() << "aladinThread:run() - Comienzo";
    system("mplayer extras/arabe.wav");
    qDebug() << "aladinThread:run() - Termina";
}
