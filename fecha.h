#ifndef FECHA_H
#define FECHA_H

#include <QDialog>
#include <database.h>

#include "stdio.h"
#include "stdio.h"
#include "sqlite3.h"


namespace Ui {
class Fecha;
}

class Fecha : public QDialog
{
    Q_OBJECT
    
public:
    explicit Fecha(QWidget *parent = 0);
    ~Fecha();

    // Funciones del Sistema

//    void timerEvent(QTimerEvent*);
    void keyPressEvent(QKeyEvent*);

    databaseQuiniela dbQuiniela;
    int estado;
    
    QString fechaAnio;
    QString fechaMes;
    QString fechaDia;

private slots:

private:
    Ui::Fecha *ui;
};

#endif // FECHA_H
