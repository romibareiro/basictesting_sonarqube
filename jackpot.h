#ifndef JACKPOT_H
#define JACKPOT_H

#include <QDialog>
#include <QtGui>
#include <QThread>


class jackpotThread : public QThread
{
    Q_OBJECT
public:
//    explicit jackpotThread(QObject *parent = 0);
    explicit jackpotThread();
    void run();
signals:

public slots:

};

namespace Ui {
class jackpot;
}

class jackpot : public QDialog
{
    Q_OBJECT

public:
    explicit jackpot(QWidget *parent = 0);
    ~jackpot();

    int y_numeroBajo1, y_numeroBajo2, y_numeroBajo3, y_numeroAlto1, y_numeroAlto2, y_numeroAlto3;
    int contadorTimer, y_pomo;
    int estadoJackpot;

    int randomNumero1, randomNumero2, randomNumero3;

    bool flagBajo1, flagBajo2, flagBajo3;

    QBasicTimer timer;
    void timerEvent(QTimerEvent*);

private:
    Ui::jackpot *ui;
    jackpotThread *t;
};

#endif // JACKPOT_H
