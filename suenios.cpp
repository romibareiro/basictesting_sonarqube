#include "suenios.h"
#include "ui_suenios.h"


//int RESOLUCION_PANTALLA = RESOLUCION_1920x1080;
int RESOLUCION_PANTALLA = RESOLUCION_1280x720;
//int RESOLUCION_PANTALLA = RESOLUCION_1024x768;
//int RESOLUCION_PANTALLA = RESOLUCION_768x576;
//int RESOLUCION_PANTALLA = RESOLUCION_720x480;

// ********************************************************************************************************************************* //

/**
 *\fn       void Suenios::Suenios ( void )
 *\brief    Constructor.
 *\note     Constructor de la interfaz de Suenios que representa a los numeros de la interpretacion de los sueños.
 *\param    Ninguno.
 *\return   Nada.
*/

Suenios::Suenios(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Suenios)
{
    ui->setupUi(this);

    qDebug() << "Sonido Habilitado??";
    habilitacionSonido = getConfiguracion("sonido");

    this->setGeometry( 0 , 0 , 1280 , 720 );

    ConfiguracionInicial ( );

    ContadorSuenios = 0;

    Nombres_DB = new dataBaseSuenios;
    Nombres_DB->seleccionarSuenios();

    MostrarImagenes( );
    MostrarNombres( );

    ContadorSuenios += 10;

    timer.start(3000,this);

//    TimerNroSueno = new QTimer(this);
//    TimerNroSueno->setInterval(3000);

    if( habilitacionSonido == SONIDO_ON )
    {
        t = new SueniosThread();
        t->start();
    }

//    QObject::connect(TimerNroSueno, SIGNAL(timeout()) , this , SLOT(VentanaSuenios()));
//    TimerNroSueno->start( );
}

// ********************************************************************************************************************************* //

/**
 *\fn       void Suenios::~Suenios ( void )
 *\brief    Constructor.
 *\note     Destructor de la interfaz de Suenios que representa a los numeros de la interpretacion de los sueños.
 *\param    Ninguno.
 *\return   Nada.
*/

Suenios::~Suenios()
{
    qDebug() << "Destructor de Sueños";
    delete ui;
}

// ********************************************************************************************************************************* //

/**
 *\fn       void Suenios::ConfiguracionInicial ( void )
 *\brief    Funcion para realizar la Configuracion Inicial..
 *\note     Se implementa la funcion para administrar todos los pasos de la configuracion inicial como la Resolucion de la pantalla,
 *          Ajustar los Parametros acorde a la resolucion de la pantalla configurada por el agenciero en la configuracion inicial al
 *          configurar el equipo y Posicionar los Elementos que componen el QDialog.
 *\param    Ninguno.
 *\return   Nada.
*/

void Suenios::ConfiguracionInicial ( )
{
    Resolucion ( );
    AjustarParametros ( );
    PosicionarElementos ( );
    TextoTitulo();
}

// ********************************************************************************************************************************* //

/**
 *\fn       void Suenios::Resolucion ( void )
 *\brief    Funcion para ajustar la Resolucion de pantalla.
 *\note     Se ajusta la resolucion de la pantalla acorde a la resolucion seleccionada por el agenciero en la configuracion principal
 *          y se utiliza la misma para posicionar y redimensionar las demas posiciones y dimensiones del QDialog.
 *\param    Ninguno.
 *\return   Nada.
*/

void Suenios::Resolucion ( void )
{
    switch ( RESOLUCION_PANTALLA )
    {
        case RESOLUCION_1920x1080 : Pantalla.Ancho = 1920;
                                    Pantalla.Alto  = 1080;
                                    break;

        case RESOLUCION_1280x720 :  Pantalla.Ancho = 1280;
                                    Pantalla.Alto  = 720;
                                    break;

        case RESOLUCION_1024x768 :  Pantalla.Ancho = 1024;
                                    Pantalla.Alto  = 768;
                                    break;

        case RESOLUCION_768x576 :   Pantalla.Ancho = 768;
                                    Pantalla.Alto  = 576;
                                    break;

        case RESOLUCION_720x480 :   Pantalla.Ancho = 720;
                                    Pantalla.Alto  = 480;
                                    break;

        default :                   break;
    }

    this->setGeometry( 0 , 0 , Pantalla.Ancho , Pantalla.Alto );

    ui->FondoSuenios->setPixmap(QPixmap("extras/suenos/Fondo.jpg"));
    ui->FondoSuenios->adjustSize( );
    ui->FondoSuenios->setGeometry( 0 , 0 , Pantalla.Ancho , Pantalla.Alto );
}

// ********************************************************************************************************************************* //

/**
 *\fn       void Suenios::AjustarParametros ( void )
 *\brief    Funcion para ajustar los parametros geometricos.
 *\note     Se ajustan los parametros geometricos como ancho, alto, posicion horizontal y vertical de cada uno de los Widget que
 *          componen el QDialog acorde a la resolucion de pantalla configurada por el agenciero.
 *\param    Ninguno.
 *\return   Nada.
*/

void Suenios::AjustarParametros ( void )
{
    Imagenes.Alto  = 100 * Pantalla.Alto  / 720;
    Imagenes.Ancho = 100 * Pantalla.Ancho / 1280;
    Imagenes.X1    = 130 * Pantalla.Ancho / 1280;
    Imagenes.X2    = 720 * Pantalla.Ancho / 1280;
    Imagenes.Y1    = 120 * Pantalla.Alto  / 720;
    Imagenes.Y2    = 240 * Pantalla.Alto  / 720;
    Imagenes.Y3    = 360 * Pantalla.Alto  / 720;
    Imagenes.Y4    = 480 * Pantalla.Alto  / 720;
    Imagenes.Y5    = 600 * Pantalla.Alto  / 720;

    Nombres.Alto   = 100 * Pantalla.Alto  / 720;
    Nombres.Ancho  = 320 * Pantalla.Ancho / 1280;
    Nombres.X1     = 229 * Pantalla.Ancho / 1280;
    Nombres.X2     = 819 * Pantalla.Ancho / 1280;
    Nombres.Y1     = 120 * Pantalla.Alto  / 720;
    Nombres.Y2     = 240 * Pantalla.Alto  / 720;
    Nombres.Y3     = 360 * Pantalla.Alto  / 720;
    Nombres.Y4     = 480 * Pantalla.Alto  / 720;
    Nombres.Y5     = 600 * Pantalla.Alto  / 720;
    Nombres.Fuente = 22  * Pantalla.Alto  / 720;

    Titulo.Alto   = 80 * Pantalla.Alto / 720;
    Titulo.Y      = 20 * Pantalla.Alto / 720;
    Titulo.Fuente = 36 * Pantalla.Alto / 720;

    QFont Fuente("Arial",Nombres.Fuente,QFont::Bold);
    ui->NombreSueno_0->setFont(Fuente);
    ui->NombreSueno_1->setFont(Fuente);
    ui->NombreSueno_2->setFont(Fuente);
    ui->NombreSueno_3->setFont(Fuente);
    ui->NombreSueno_4->setFont(Fuente);
    ui->NombreSueno_5->setFont(Fuente);
    ui->NombreSueno_6->setFont(Fuente);
    ui->NombreSueno_7->setFont(Fuente);
    ui->NombreSueno_8->setFont(Fuente);
    ui->NombreSueno_9->setFont(Fuente);
}

// ********************************************************************************************************************************* //

/**
 *\fn       void Suenios::PosicionarElementos ( void )
 *\brief    Funcion para posicionar los Widget.
 *\note     Se posicionan los parametros geometricos como ancho, alto, posicion horizontal y vertical de cada uno de los Widget que
 *          componen el QDialog acorde a la resolucion de pantalla configurada por el agenciero.
 *\param    Ninguno.
 *\return   Nada.
*/

void Suenios::PosicionarElementos ( void )
{
    ui->ImgSueno_0->setGeometry( Imagenes.X1 , Imagenes.Y1 , Imagenes.Ancho , Imagenes.Alto );
    ui->ImgSueno_1->setGeometry( Imagenes.X1 , Imagenes.Y2 , Imagenes.Ancho , Imagenes.Alto );
    ui->ImgSueno_2->setGeometry( Imagenes.X1 , Imagenes.Y3 , Imagenes.Ancho , Imagenes.Alto );
    ui->ImgSueno_3->setGeometry( Imagenes.X1 , Imagenes.Y4 , Imagenes.Ancho , Imagenes.Alto );
    ui->ImgSueno_4->setGeometry( Imagenes.X1 , Imagenes.Y5 , Imagenes.Ancho , Imagenes.Alto );
    ui->ImgSueno_5->setGeometry( Imagenes.X2 , Imagenes.Y1 , Imagenes.Ancho , Imagenes.Alto );
    ui->ImgSueno_6->setGeometry( Imagenes.X2 , Imagenes.Y2 , Imagenes.Ancho , Imagenes.Alto );
    ui->ImgSueno_7->setGeometry( Imagenes.X2 , Imagenes.Y3 , Imagenes.Ancho , Imagenes.Alto );
    ui->ImgSueno_8->setGeometry( Imagenes.X2 , Imagenes.Y4 , Imagenes.Ancho , Imagenes.Alto );
    ui->ImgSueno_9->setGeometry( Imagenes.X2 , Imagenes.Y5 , Imagenes.Ancho , Imagenes.Alto );

    ui->NombreSueno_0->setGeometry( Nombres.X1 , Nombres.Y1 , Nombres.Ancho , Nombres.Alto );
    ui->NombreSueno_1->setGeometry( Nombres.X1 , Nombres.Y2 , Nombres.Ancho , Nombres.Alto );
    ui->NombreSueno_2->setGeometry( Nombres.X1 , Nombres.Y3 , Nombres.Ancho , Nombres.Alto );
    ui->NombreSueno_3->setGeometry( Nombres.X1 , Nombres.Y4 , Nombres.Ancho , Nombres.Alto );
    ui->NombreSueno_4->setGeometry( Nombres.X1 , Nombres.Y5 , Nombres.Ancho , Nombres.Alto );
    ui->NombreSueno_5->setGeometry( Nombres.X2 , Nombres.Y1 , Nombres.Ancho , Nombres.Alto );
    ui->NombreSueno_6->setGeometry( Nombres.X2 , Nombres.Y2 , Nombres.Ancho , Nombres.Alto );
    ui->NombreSueno_7->setGeometry( Nombres.X2 , Nombres.Y3 , Nombres.Ancho , Nombres.Alto );
    ui->NombreSueno_8->setGeometry( Nombres.X2 , Nombres.Y4 , Nombres.Ancho , Nombres.Alto );
    ui->NombreSueno_9->setGeometry( Nombres.X2 , Nombres.Y5 , Nombres.Ancho , Nombres.Alto );
}

// ********************************************************************************************************************************* //

/**
 *\fn       void Suenios::TextoTitulo ( void )
 *\brief    Funcion para escribir el texto del titulo.
 *\note     Se escribe el texto del titulo para el encabezado, se posiciona el Widget en el centro de la pantalla.
 *\param    Ninguno.
 *\return   Nada.
*/

void Suenios::TextoTitulo ( void )
{
    QRect Dimensiones;

    ui->SuenosTitulo_1->adjustSize( );
    Dimensiones = ui->SuenosTitulo_1->geometry( );
    Titulo.X = (Pantalla.Ancho - Dimensiones.width( )) / 2;
    Titulo.Ancho = Dimensiones.width( );
    ui->SuenosTitulo_1->setGeometry(Titulo.X,Titulo.Y,Titulo.Ancho,Titulo.Alto);
}

// ********************************************************************************************************************************* //

/**
 *\fn       void Suenios::timerEvent(QTimerEvent*)
 *\brief    Funcion para administar la interpretacion de los sueños.
 *\note     Se administra la presentacion de la interpretacion de los numeros de los sueños en la cual se muestran las 100 diferentes
 *          imagenes junto con el nombre y numero que lo representa y que las cuales van alternandose arrancando desde el 00 hasta el
 *          99 en saltos de 10 con un periodo fijo de presentacion de cada salto.
 *\param    Ninguno.
 *\return   Nada.
*/

void Suenios::timerEvent(QTimerEvent*)
{
    if( ContadorSuenios > 90 )
    {
        timer.stop();
        close();
    }
    else
    {

    MostrarImagenes( );
    MostrarNombres( );

    qDebug() << "Contador: " << ContadorSuenios;

    ContadorSuenios += 10;
    }
    //system("mplayer extras/suenos/audio_suenios.wav -endpos 3");
}

// ********************************************************************************************************************************* //

/**
 *\fn       void Suenios::MostrarImagenes ( void )
 *\brief    Funcion para imprimir en pantalla las imagenes.
 *\note     Se imprimir en pantalla las imagenes correspondientes a la interpretacion de los sueños.
 *\param    Ninguno.
 *\return   Nada.
*/

void Suenios::MostrarImagenes ( void )
{
    ui->ImgSueno_0->setPixmap(QPixmap(Nombres_DB->direccionSuenios[ContadorSuenios+0]));
    ui->ImgSueno_0->adjustSize();
    ui->ImgSueno_0->setGeometry(Imagenes.X1,Imagenes.Y1,Imagenes.Ancho,Imagenes.Alto);

    ui->ImgSueno_1->setPixmap(QPixmap(Nombres_DB->direccionSuenios[ContadorSuenios+1]));
    ui->ImgSueno_1->adjustSize();
    ui->ImgSueno_1->setGeometry(Imagenes.X1,Imagenes.Y2,Imagenes.Ancho,Imagenes.Alto);

    ui->ImgSueno_2->setPixmap(QPixmap(Nombres_DB->direccionSuenios[ContadorSuenios+2]));
    ui->ImgSueno_2->adjustSize();
    ui->ImgSueno_2->setGeometry(Imagenes.X1,Imagenes.Y3,Imagenes.Ancho,Imagenes.Alto);

    ui->ImgSueno_3->setPixmap(QPixmap(Nombres_DB->direccionSuenios[ContadorSuenios+3]));
    ui->ImgSueno_3->adjustSize();
    ui->ImgSueno_3->setGeometry(Imagenes.X1,Imagenes.Y4,Imagenes.Ancho,Imagenes.Alto);

    ui->ImgSueno_4->setPixmap(QPixmap(Nombres_DB->direccionSuenios[ContadorSuenios+4]));
    ui->ImgSueno_4->adjustSize();
    ui->ImgSueno_4->setGeometry(Imagenes.X1,Imagenes.Y5,Imagenes.Ancho,Imagenes.Alto);

    ui->ImgSueno_5->setPixmap(QPixmap(Nombres_DB->direccionSuenios[ContadorSuenios+5]));
    ui->ImgSueno_5->adjustSize();
    ui->ImgSueno_5->setGeometry(Imagenes.X2,Imagenes.Y1,Imagenes.Ancho,Imagenes.Alto);

    ui->ImgSueno_6->setPixmap(QPixmap(Nombres_DB->direccionSuenios[ContadorSuenios+6]));
    ui->ImgSueno_6->adjustSize();
    ui->ImgSueno_6->setGeometry(Imagenes.X2,Imagenes.Y2,Imagenes.Ancho,Imagenes.Alto);

    ui->ImgSueno_7->setPixmap(QPixmap(Nombres_DB->direccionSuenios[ContadorSuenios+7]));
    ui->ImgSueno_7->adjustSize();
    ui->ImgSueno_7->setGeometry(Imagenes.X2,Imagenes.Y3,Imagenes.Ancho,Imagenes.Alto);

    ui->ImgSueno_8->setPixmap(QPixmap(Nombres_DB->direccionSuenios[ContadorSuenios+8]));
    ui->ImgSueno_8->adjustSize();
    ui->ImgSueno_8->setGeometry(Imagenes.X2,Imagenes.Y4,Imagenes.Ancho,Imagenes.Alto);

    ui->ImgSueno_9->setPixmap(QPixmap(Nombres_DB->direccionSuenios[ContadorSuenios+9]));
    ui->ImgSueno_9->adjustSize();
    ui->ImgSueno_9->setGeometry(Imagenes.X2,Imagenes.Y5,Imagenes.Ancho,Imagenes.Alto);
}

// ********************************************************************************************************************************* //

/**
 *\fn       void Suenios::MostrarNombres ( void )
 *\brief    Funcion para imprimir en pantalla los nombres.
 *\note     Se imprime en pantalla el nombre de la imagen y el numero correspondiente a la interpretacion de los sueños.
 *\param    Ninguno.
 *\return   Nada.
*/

void Suenios::MostrarNombres ( void )
{
    ui->NombreSueno_0->setText(Nombres_DB->textoSuenios[ContadorSuenios+0]);
    ui->NombreSueno_0->adjustSize();
    ui->NombreSueno_0->setGeometry(Nombres.X1,Nombres.Y1,Nombres.Ancho,Nombres.Alto);

    ui->NombreSueno_1->setText(Nombres_DB->textoSuenios[ContadorSuenios+1]);
    ui->NombreSueno_1->adjustSize();
    ui->NombreSueno_1->setGeometry(Nombres.X1,Nombres.Y2,Nombres.Ancho,Nombres.Alto);

    ui->NombreSueno_2->setText(Nombres_DB->textoSuenios[ContadorSuenios+2]);
    ui->NombreSueno_2->adjustSize();
    ui->NombreSueno_2->setGeometry(Nombres.X1,Nombres.Y3,Nombres.Ancho,Nombres.Alto);

    ui->NombreSueno_3->setText(Nombres_DB->textoSuenios[ContadorSuenios+3]);
    ui->NombreSueno_3->adjustSize();
    ui->NombreSueno_3->setGeometry(Nombres.X1,Nombres.Y4,Nombres.Ancho,Nombres.Alto);

    ui->NombreSueno_4->setText(Nombres_DB->textoSuenios[ContadorSuenios+4]);
    ui->NombreSueno_4->adjustSize();
    ui->NombreSueno_4->setGeometry(Nombres.X1,Nombres.Y5,Nombres.Ancho,Nombres.Alto);

    ui->NombreSueno_5->setText(Nombres_DB->textoSuenios[ContadorSuenios+5]);
    ui->NombreSueno_5->adjustSize();
    ui->NombreSueno_5->setGeometry(Nombres.X2,Nombres.Y1,Nombres.Ancho,Nombres.Alto);

    ui->NombreSueno_6->setText(Nombres_DB->textoSuenios[ContadorSuenios+6]);
    ui->NombreSueno_6->adjustSize();
    ui->NombreSueno_6->setGeometry(Nombres.X2,Nombres.Y2,Nombres.Ancho,Nombres.Alto);

    ui->NombreSueno_7->setText(Nombres_DB->textoSuenios[ContadorSuenios+7]);
    ui->NombreSueno_7->adjustSize();
    ui->NombreSueno_7->setGeometry(Nombres.X2,Nombres.Y3,Nombres.Ancho,Nombres.Alto);

    ui->NombreSueno_8->setText(Nombres_DB->textoSuenios[ContadorSuenios+8]);
    ui->NombreSueno_8->adjustSize();
    ui->NombreSueno_8->setGeometry(Nombres.X2,Nombres.Y4,Nombres.Ancho,Nombres.Alto);

    ui->NombreSueno_9->setText(Nombres_DB->textoSuenios[ContadorSuenios+9]);
    ui->NombreSueno_9->adjustSize();
    ui->NombreSueno_9->setGeometry(Nombres.X2,Nombres.Y5,Nombres.Ancho,Nombres.Alto);
}


SueniosThread::SueniosThread() :
    QThread()
{
    qDebug() << "SueniosThread::SueniosThread() - Comienzo Constructor";
}


void SueniosThread::run()
{
    qDebug() << "SueniosThread:run() - Comienzo";
    system("mplayer extras/audio_suenios.wav -endpos 27");
    qDebug() << "SueniosThread:run() - Termina";
}
