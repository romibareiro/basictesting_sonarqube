#ifndef JUEGORULETA_H
#define JUEGORULETA_H

#include <QDialog>

#include <QLabel>

#include <stdlib.h>
#include <cstdio>
#include <QGraphicsColorizeEffect>
#include <QTimer>
#include <QPainter>
#include <QThread>
#include <QtCore>
#include <QTimer>
//#include "audio.h"
//#include "audiothread.h"
#include <sys/types.h>
#include <signal.h>
#include <stdio.h>

#include <ostream>
#include <qglobal.h>
#include <QTime>
#include <unistd.h>

#define SONIDO_RULETA 1
#define SONIDO_COLOR 2
#define SONIDO_NUMERO 3
#define FIN_ 4


class ruletaThread : public QThread
{
    Q_OBJECT
public:
//    explicit jackpotThread(QObject *parent = 0);
    explicit ruletaThread();
    void run();
signals:

public slots:

};


namespace Ui
{
    class juegosRuleta;
    class timerRuleta ;
    //class audio ;
}



class juegosRuleta : public QDialog
{

    Q_OBJECT

public:
   // QBasicTimer timer;
    QTimer *timer;
    bool finAudio;
    bool finRuleta;
    bool finAudioSuerte;
    bool CERRAR;

    bool HAVE_NUMBER;
    int audioTime;

    QTimer* timerBolita;
    int state;

    int my_counter;

   // void timerEvent(QTimerEvent *event);
    explicit juegosRuleta(QWidget *parent = 0);
    FILE *fp;
    pid_t pidHijo;

   // audio* reproductionAudio;
    void startAudio();
    void stopAudio();
    void isBlackRedOrZero(int number,QPalette *colours, char *);
  //  void cargarArchivoAudio(char* archivo,QAudioOutput* audio_,QFile *sourceFile);
    void closeDialog();
    bool setAudioOn;

    //void start(QAudioOutput* audio_, QFile *source_file);

    //void stop(QAudioOutput* audio_, QFile *source_file);

  //  void pause(QAudioOutput* audio_);
//void cargarArchivoAudio(char* archivo);
  //  void setFormat(QAudioFormat format);
//QAudioOutput* audio_;
    //int getElapsedSeconds(QAudioOutput audio_);
    //int getElapsedmSeconds(QAudioOutput audio_);
 //QAudioFormat format;
    ~juegosRuleta();
//private:

 //   QFile sourceFile;   // class member.
   // QAudioOutput* audio_; // class member.



private slots:
//    void on_pushButton_clicked();
    void playMe();
void giroRuleta();
    void startRuleta();
   // void stopRuleta();
    void writeIntoLabelNumber(int number, char*);

 //   void on_pushButton_2_clicked();


private:

    Ui::juegosRuleta *ui;
    ruletaThread *t;

    QFile sourceFile;
    //QAudioFormat format;
    int FLAG_STOP;
    int FLAG_INTEGER;

};



#endif // JUEGORULETA_H

