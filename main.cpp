#include "mainwindow.h"
#include <QApplication>

#define COMPILACIONCOMPLETA

int serialNumber;

/**
 *\fn       int main(int argc, char *argv[])
 *\brief    Funcion Principal
*/

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    a.setApplicationName("TimBear");

// Obtener el Serial Number del Archivo "Serial Number"

    FILE *pFile;
    char buffer[10];

    pFile = fopen("serialNumber","r");

    if (pFile == NULL)
        perror ("Error opening file");
    else
    {
        fgets (buffer , sizeof(buffer)-1 , pFile);
        fclose (pFile);
    }

    serialNumber = atoi(buffer);

    qDebug() << "Serial Number" << serialNumber;

//    presentacion *pres;
//    pres = new presentacion;
//    pres->show();

//    presentacionThread presThread;
//    presThread.start();

    MainWindow w;

//    parametros = new parametrosConfiguracion;

//    parametros->sincronizar();

#ifdef COMPILACIONCOMPLETA
// COmpilacion Completa

    presentacion *pres;
    pres = new presentacion;

    presentacionThread * t;
    t = new presentacionThread;

    QObject::connect( t , SIGNAL( barraPaso( int , QString )) , pres , SLOT( onBarraPaso( int , QString )) );

    t->start();

    QObject::connect( t , SIGNAL( signalMainWindow( int )) , &w , SLOT( onMainWindow( int )) );

    pres->show();

#else
// Compilacion Simple

//    MyTCPClient Cliente;
    w.onMainWindow(0);
    w.showFullScreen();



//      MyTCPClient clienteTCP;

#endif

    return a.exec();
}


