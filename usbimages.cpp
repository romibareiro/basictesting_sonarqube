#include "usbimages.h"
#include "ui_usbimages.h"

//int RESOLUCION_PANTALLA = RESOLUCION_1920x1080;
//int RESOLUCION_PANTALLA = RESOLUCION_1280x720;
//int RESOLUCION_PANTALLA = RESOLUCION_1024x768;
//int RESOLUCION_PANTALLA = RESOLUCION_768x576;
//int RESOLUCION_PANTALLA = RESOLUCION_720x480;

extern int RESOLUCION_PANTALLA;

// ********************************************************************************************************************************* //

/**
 *\fn       void USBImages::USBImages() ( void )
 *\brief    Constructor.
 *\note     Constructor de la interfaz de USBImages.
 *\param    Ninguno.
 *\return   Nada.
*/

USBImages::USBImages(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::USBImages)
{
    ui->setupUi(this);

    ConfiguracionInicial();

    ui->Publicidad_1->setPixmap(QPixmap("/home/root/extras/logoTimBear.png"));
    ui->Publicidad_2->setPixmap(QPixmap("/home/root/extras/logoTimBear.png"));
    ui->Publicidad_3->setPixmap(QPixmap("/home/root/extras/logoTimBear.png"));
    ui->Publicidad_4->setPixmap(QPixmap("/home/root/extras/logoTimBear.png"));
    pendrive=new USBThread (this);
    connect(pendrive,SIGNAL(cargarDesdeElUSB(char*)),this,SLOT(cargarDesdeElUSB(char*)));
    timerEspera.start(100,this);

}

// ********************************************************************************************************************************* //

/**
 *\fn       void USBImages::~USBImages() ( void )
 *\brief    Destructor.
 *\note     Destructor de la interfaz de USBImages.
 *\param    Ninguno.
 *\return   Nada.
*/

USBImages::~USBImages()
{
    this->close();

    //delete ui;
}

// ********************************************************************************************************************************* //

/**
 *\fn       void USBImages::ConfiguracionInicial ( void )
 *\brief    Funcion para realizar la Configuracion Inicial.
 *\note     Se implementa la funcion para administrar todos los pasos de la configuracion inicial como la Resolucion de la pantalla,
 *          Ajustar los parametros acorde a la resolucion configurada, Texto del encabezado, Borde del encabezado, Texto por default
 *          del titulo de las publicidades e Imagen inicial donde apareceran las imagenes presentes en el pen drive.
 *\param    Ninguno.
 *\return   Nada.
*/

void USBImages::ConfiguracionInicial ( void )
{
    Resolucion();
    AjustarParametros();
    TextoEncabezado();
    BordeEncabezado();
    TextoTitulos();
    ImagenInicial();
}

// ********************************************************************************************************************************* //

/**
 *\fn       void USBImages::Resolucion ( void )
 *\brief    Funcion para ajustar la Resolucion de pantalla.
 *\note     Se ajusta la resolucion de la pantalla acorde a la resolucion seleccionada por el agenciero en la configuracion principal
 *          y se utiliza la misma para posicionar y redimensionar las demas posiciones y dimensiones del QDialog.
 *\param    Ninguno.
 *\return   Nada.
*/

void USBImages::Resolucion ( void )
{
    //Se setean las variables de la estructura PANTALLA en la que se guardan las dimensiones de la pantalla.
    switch ( RESOLUCION_PANTALLA )
    {
        case RESOLUCION_1920x1080 : Pantalla.Alto  = 1080;
                                    Pantalla.Ancho = 1920;
                                    break;

        case RESOLUCION_1280x720 :  Pantalla.Alto  = 720;
                                    Pantalla.Ancho = 1280;
                                    break;

        case RESOLUCION_1024x768 :  Pantalla.Alto  = 768;
                                    Pantalla.Ancho = 1024;
                                    break;

        case RESOLUCION_768x576 :   Pantalla.Alto  = 576;
                                    Pantalla.Ancho = 768;
                                    break;

        case RESOLUCION_720x480 :   Pantalla.Alto  = 480;
                                    Pantalla.Ancho = 720;
                                    break;

        default :                   break;
    }

    //Ajusto el tamaño del QDialog a la resolucion seleccionada por el agenciero.
    this->setGeometry( 0 , 0 , Pantalla.Ancho , Pantalla.Alto );
}

// ********************************************************************************************************************************* //

/**
 *\fn       void USBImages::AjustarParametros ( void )
 *\brief    Funcion para ajustar los parametros geometricos.
 *\note     Se ajustan los parametros geometricos como ancho, alto, posicion horizontal y vertical de cada uno de los Widget que
 *          componen el QDialog acorde a la resolucion de pantalla configurada por el agenciero.
 *\param    Ninguno.
 *\return   Nada.
*/

void USBImages::AjustarParametros ( void )
{
    Encabezado.Y1     = 20 * Pantalla.Alto  / 720;
    Encabezado.Y2     = 50 * Pantalla.Alto  / 720;
    Encabezado.Y3     = 80 * Pantalla.Alto  / 720;
    Encabezado.Fuente = 16 * Pantalla.Ancho / 1280;

    Borde_Encabezado.Separacion = 10 * Pantalla.Ancho / 1280;

    Titulos.X1     = 190 * Pantalla.Ancho / 1280;
    Titulos.X2     = 736 * Pantalla.Ancho / 1280;
    Titulos.Y1     = 150 * Pantalla.Alto  / 720;
    Titulos.Y2     = 430 * Pantalla.Alto  / 720;
    Titulos.Alto   = 30  * Pantalla.Alto  / 720;
    Titulos.Ancho  = 356 * Pantalla.Ancho / 1280;
    Titulos.Fuente = 15  * Pantalla.Ancho / 1280;

    Publicidades.Ancho = 356 * Pantalla.Ancho / 1280;
    Publicidades.Alto  = 205 * Pantalla.Alto  / 720;
    Publicidades.X1    = 190 * Pantalla.Ancho / 1280;
    Publicidades.X2    = 736 * Pantalla.Ancho / 1280;
    Publicidades.Y1    = Titulos.Y1 + Titulos.Alto + 10 * Pantalla.Alto / 720;
    Publicidades.Y2    = Titulos.Y2 + Titulos.Alto + 10 * Pantalla.Alto / 720;
}

// ********************************************************************************************************************************* //

/**
 *\fn       void USBImages::TextoEncabezado ( void )
 *\brief    Funcion para cargar el texto para el encabezado de la aplicacion.
 *\note     Se carga el texto del encabezado en la que se describe como realizar el proceso para cargar la base de datos con las
 *          imagenes que queremos introducir como propaganda en la seccion de Publicidad Variable.
 *\param    Ninguno.
 *\return   Nada.
*/

void USBImages::TextoEncabezado ( void )
{
    ui->Encabezado_1->setText("Inserte el pendrive con las imagenes publicitarias que desea mostrar.");
    ui->Encabezado_2->setText("Las mismas deberan estar situadas en la carpeta TIMBEAR.");
    ui->Encabezado_3->setText("Una vez cargadas, las mismas se visualizaran en los recuadros.");

    QFont Fuente("Arial",Encabezado.Fuente,QFont::Bold);

    ui->Encabezado_1->setFont(Fuente);
    ui->Encabezado_2->setFont(Fuente);
    ui->Encabezado_3->setFont(Fuente);

    ui->Encabezado_1->adjustSize( );
    ui->Encabezado_2->adjustSize( );
    ui->Encabezado_3->adjustSize( );

    QRect Dimensiones;

    Dimensiones = ui->Encabezado_1->geometry( );
    Encabezado.X1 = (Pantalla.Ancho - Dimensiones.width( )) / 2;
    ui->Encabezado_1->setGeometry(Encabezado.X1,Encabezado.Y1,Dimensiones.width(),Dimensiones.height());

    Dimensiones = ui->Encabezado_2->geometry( );
    Encabezado.X2 = (Pantalla.Ancho - Dimensiones.width( )) / 2;
    ui->Encabezado_2->setGeometry(Encabezado.X2,Encabezado.Y2,Dimensiones.width(),Dimensiones.height());

    Dimensiones = ui->Encabezado_3->geometry( );
    Encabezado.X3 = (Pantalla.Ancho - Dimensiones.width( )) / 2;
    ui->Encabezado_3->setGeometry(Encabezado.X3,Encabezado.Y3,Dimensiones.width(),Dimensiones.height());
}

// ********************************************************************************************************************************* //

/**
 *\fn       void USBImages::BordeEncabezado ( void )
 *\brief    Funcion para ajustar el tamaño de los bordes del encabezado del texto.
 *\note     Se ajusta el tamaño de los bordes del texto del encabezado acorde a la resolucion de la pantalla configurada por el
 *          agenciero y respecto del tamaño y la posicion del texto del encabezado.
 *\param    Ninguno.
 *\return   Nada.
*/

void USBImages::BordeEncabezado ( void )
{
    int X1;
    int Y1;
    int X2;
    int Y2;

    QRect Dimensiones;

    Dimensiones = ui->Encabezado_1->geometry( );
    Dimensiones.getCoords( &X1 , &Y1 , &X2 , &Y2 );

    Borde_Encabezado.X1 = X1 - 20 * Borde_Encabezado.Separacion;
    Borde_Encabezado.X2 = X2 + 20 * Borde_Encabezado.Separacion;
    Borde_Encabezado.Y1 = Y1 - Borde_Encabezado.Separacion;

    Dimensiones = ui->Encabezado_3->geometry( );
    Dimensiones.getCoords( &X1 , &Y1 , &X2 , &Y2 );

    Borde_Encabezado.Y2 = Y2 + Borde_Encabezado.Separacion;

    Dimensiones.setCoords( Borde_Encabezado.X1 , Borde_Encabezado.Y1 , Borde_Encabezado.X2 , Borde_Encabezado.Y2 );
    ui->BordeEncabezado->setGeometry( Dimensiones );
}

// ********************************************************************************************************************************* //

/**
 *\fn       void USBImages::TextoTitulos ( void )
 *\brief    Funcion para escribir el texto inicial de los titulos.
 *\note     Se escribe el texto inicial de las titulos de las publicidades variables previa a la conexion del pen drive al dispositivo.
 *\param    Ninguno.
 *\return   Nada.
*/

void USBImages::TextoTitulos ( void )
{
    QFont Fuente2("Sans Serif",Titulos.Fuente,QFont::Bold);

    ui->Titulo_1->setFont( Fuente2 );
    ui->Titulo_2->setFont( Fuente2 );
    ui->Titulo_3->setFont( Fuente2 );
    ui->Titulo_4->setFont( Fuente2 );

    ui->Titulo_1->setGeometry( Titulos.X1 , Titulos.Y1 , Titulos.Ancho ,Titulos.Alto );
    ui->Titulo_2->setGeometry( Titulos.X2 , Titulos.Y1 , Titulos.Ancho ,Titulos.Alto );
    ui->Titulo_3->setGeometry( Titulos.X1 , Titulos.Y2 , Titulos.Ancho ,Titulos.Alto );
    ui->Titulo_4->setGeometry( Titulos.X2 , Titulos.Y2 , Titulos.Ancho ,Titulos.Alto );
}

// ********************************************************************************************************************************* //

/**
 *\fn       void USBImages::ImagenInicial ( void )
 *\brief    Funcion para cargar las imagenes iniciales.
 *\note     Se cargan las imagenes iniciales tanto como la imagen de fondo de pantalla del QDialog como tambien de las imagenes que
 *          apareceran hasta que se carguen otras por medio de la conexion del pen drive al dispositivo.
 *\param    Ninguno.
 *\return   Nada.
*/

void USBImages::ImagenInicial ( void )
{
    ui->Publicidad_1->setPixmap(QPixmap("extras/logoTimBear.png"));
    ui->Publicidad_1->setScaledContents( true );
    ui->Publicidad_1->setGeometry( Publicidades.X1 , Publicidades.Y1 , Publicidades.Ancho , Publicidades.Alto );

    ui->Publicidad_2->setPixmap(QPixmap("extras/logoTimBear.png"));
    ui->Publicidad_2->setScaledContents( true );
    ui->Publicidad_2->setGeometry( Publicidades.X2 , Publicidades.Y1 , Publicidades.Ancho , Publicidades.Alto );

    ui->Publicidad_3->setPixmap(QPixmap("extras/logoTimBear.png"));
    ui->Publicidad_3->setScaledContents( true );
    ui->Publicidad_3->setGeometry( Publicidades.X1 , Publicidades.Y2 , Publicidades.Ancho , Publicidades.Alto );

    ui->Publicidad_4->setPixmap(QPixmap("extras/logoTimBear.png"));
    ui->Publicidad_4->setScaledContents( true );
    ui->Publicidad_4->setGeometry( Publicidades.X2 , Publicidades.Y2 , Publicidades.Ancho , Publicidades.Alto );

    ui->ImagenFondo->setPixmap(QPixmap("extras/FondoUSBImages.jpg"));
    ui->ImagenFondo->setScaledContents( true );
    ui->ImagenFondo->setGeometry( 0 , 0 , Pantalla.Ancho , Pantalla.Alto );
}


void USBImages::cargarDesdeElUSB(char* extension)
{

    FILE *fp, *CantidadArchivos;
    char comando[100];
//    char direccion[100];
    memset(comando,0,sizeof(comando));
    size_t b=0;
    char *puntero_path= NULL;//=path;
    puntero_path=(char*)malloc(100);
    char aux_string[100];
    //char buffer[100]	;


    sprintf(comando, "ls %s | grep .%s | wc",RUTA_ARCHIVOS_PENDRIVE, extension);

    CantidadArchivos = popen(comando,"r");

    if (CantidadArchivos==NULL)
    {    perror("\n Falla de popen:\n");
        return;
    }
    fgets(aux_string,10,CantidadArchivos);
    pclose(CantidadArchivos);

    sprintf(comando, "ls %s | grep .%s",RUTA_ARCHIVOS_PENDRIVE, extension);
    fp = popen(comando, "r");




    if(atoi(aux_string)!=0)
    {
       classPublicidad.NumeroArchivos=atoi(aux_string);
      int a=classPublicidad.NumeroArchivos;
      int posicion;
      char aux[60];
      memset(aux,0,sizeof(aux));
      printf("%s ", puntero_path);
      char direccion[100];
      memset (direccion,0,sizeof(direccion)); // blanquear vector
      sprintf (direccion,"extras/QUINIELERO.db");
      sqlite3 *db;

      abrirBaseCliente(&db);


        for(posicion=0;a>0;a--, posicion++)
        {
            int c=getline((&puntero_path),&b,fp);
            puntero_path[(c-1)]=(char)'\0';

            memset(classPublicidad.ClasePubli[posicion].nombrePublicidad,0,sizeof(classPublicidad.ClasePubli[posicion].nombrePublicidad));
            sprintf(classPublicidad.ClasePubli[posicion].nombrePublicidad,"%s",puntero_path);
            char dirPublicMovil[100];
            memset(dirPublicMovil,0,sizeof(dirPublicMovil));
            switch(posicion)
            {
            case 0:
              ui->Titulo_1->setText(classPublicidad.ClasePubli[posicion].nombrePublicidad);
              sprintf(aux,"%s/%s",RUTA_ARCHIVOS_PENDRIVE,classPublicidad.ClasePubli[posicion].nombrePublicidad );//ui->Propaganda1->text();                   //Tomo el nombre de la propaganda 1 seleccionada

              ui->Publicidad_1->adjustSize();
              ui->Publicidad_1->setScaledContents(true);
              ui->Publicidad_1->setPixmap(QPixmap(aux));
              ui->Publicidad_1->repaint();
              sprintf(dirPublicMovil,"cp %s %s%s",aux,RUTA_ARCHIVOS_MODIFICADOR,RUTA_ARCHIVOS_PUBLICIDAD_MOVIL);
              system(dirPublicMovil);
              sprintf(dirPublicMovil,"%s/%s",RUTA_ARCHIVOS_PUBLICIDAD_MOVIL,classPublicidad.ClasePubli[posicion].nombrePublicidad );
              insertarPublicidadVariable(dirPublicMovil);


                break;
            case 1:
             ui->Titulo_2->setText(classPublicidad.ClasePubli[posicion].nombrePublicidad);
             sprintf(aux,"%s/%s",RUTA_ARCHIVOS_PENDRIVE,classPublicidad.ClasePubli[posicion].nombrePublicidad );//ui->Propaganda1->text();                   //Tomo el nombre de la propaganda 1 seleccionada
             ui->Publicidad_2->adjustSize();
             ui->Publicidad_2->setScaledContents(true);
             ui->Publicidad_2->setPixmap(QPixmap(aux));
             ui->Publicidad_2->repaint();
             sprintf(dirPublicMovil,"cp %s %s%s",aux,RUTA_ARCHIVOS_MODIFICADOR,RUTA_ARCHIVOS_PUBLICIDAD_MOVIL);
             system(dirPublicMovil);
             sprintf(dirPublicMovil,"%s/%s",RUTA_ARCHIVOS_PUBLICIDAD_MOVIL,classPublicidad.ClasePubli[posicion].nombrePublicidad );
             insertarPublicidadVariable(dirPublicMovil);




             break;
            case 2:
             ui->Titulo_3->setText(classPublicidad.ClasePubli[posicion].nombrePublicidad);
             sprintf(aux,"%s/%s",RUTA_ARCHIVOS_PENDRIVE,classPublicidad.ClasePubli[posicion].nombrePublicidad );//ui->Propaganda1->text();                   //Tomo el nombre de la propaganda 1 seleccionada
             ui->Publicidad_3->adjustSize();
             ui->Publicidad_3->setScaledContents(true);
             ui->Publicidad_3->setPixmap(QPixmap(aux));
             ui->Publicidad_3->repaint();
             sprintf(dirPublicMovil,"cp %s %s%s",aux,RUTA_ARCHIVOS_MODIFICADOR,RUTA_ARCHIVOS_PUBLICIDAD_MOVIL);
             system(dirPublicMovil);
             sprintf(dirPublicMovil,"%s/%s",RUTA_ARCHIVOS_PUBLICIDAD_MOVIL,classPublicidad.ClasePubli[posicion].nombrePublicidad );
             insertarPublicidadVariable(dirPublicMovil);




             break;
            case 3:
             ui->Titulo_4->setText(classPublicidad.ClasePubli[posicion].nombrePublicidad);
             sprintf(aux,"%s/%s",RUTA_ARCHIVOS_PENDRIVE,classPublicidad.ClasePubli[posicion].nombrePublicidad );//ui->Propaganda1->text();                   //Tomo el nombre de la propaganda 1 seleccionada
             ui->Publicidad_4->adjustSize();
             ui->Publicidad_4->setScaledContents(true);
             ui->Publicidad_4->setPixmap(QPixmap(aux));
             ui->Publicidad_4->repaint();
             sprintf(dirPublicMovil,"cp %s %s%s",aux,RUTA_ARCHIVOS_MODIFICADOR,RUTA_ARCHIVOS_PUBLICIDAD_MOVIL);
             system(dirPublicMovil);
             sprintf(dirPublicMovil,"%s/%s",RUTA_ARCHIVOS_PUBLICIDAD_MOVIL,classPublicidad.ClasePubli[posicion].nombrePublicidad );
             insertarPublicidadVariable(dirPublicMovil);



             break;

            case 4:
             /*ui->IMAGEN_5->setText(classPublicidad.ClasePubli[posicion].nombrePublicidad);
             sprintf(aux,"%s/%s",RUTA_ARCHIVOS_PENDRIVE,classPublicidad.ClasePubli[posicion].nombrePublicidad );//ui->Propaganda1->text();                   //Tomo el nombre de la propaganda 1 seleccionada
             ui->publicidades_5->adjustSize();
             ui->publicidades_5->setScaledContents(true);
             ui->publicidades_5->setPixmap(QPixmap(aux));*/
             break;

            }
        }
        chargeReady=1;

        pclose(fp);


 }
}

void USBImages::timerEvent(QTimerEvent *event)
{
    event = event;
    timerEspera.stop();
    pendrive->run();
    sleep(10);
    closeDialog();
}
void USBImages::closeDialog()
{

    this->close();
}
