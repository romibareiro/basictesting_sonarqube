#include "fecha.h"
#include "ui_fecha.h"

Fecha::Fecha(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Fecha)
{
    ui->setupUi(this);

    // Cargar Titulo
    ui->titulo->setText("BUSCADOR DE QUINIELAS");
    ui->comandos->setText("Seleccionar el Nombre de la Quiniela");

    // Cargar Opciones
    ui->opcion01->setText("1 - Nacional");
    ui->opcion02->setText("2 - Buenos Aires");
    ui->opcion03->setText("3 - Santa Fe");
    ui->opcion04->setText("4 - Cordoba");

    fechaAnio.clear();
    fechaMes.clear();
    fechaDia.clear();

    estado = 0;


}

Fecha::~Fecha()
{
    delete ui;
}


void Fecha::keyPressEvent(QKeyEvent* event)
{
    switch (estado) {
    case 0:
        switch (event->key())
        {
        case Qt::Key_1:
            estado = 1;
            ui->nombreQuiniela->setText("Nombre: Nacional");
            dbQuiniela.nombreSorteo = "Nacional";

            ui->comandos->setText("Seleccionar Tipo de la Quiniela");

            ui->opcion01->setText("1 - Primera");
            ui->opcion02->setText("2 - Matutina");
            ui->opcion03->setText("3 - Vespertina");
            ui->opcion04->setText("4 - Nocturna");

            break;

        case Qt::Key_2:
            estado = 1;
            ui->nombreQuiniela->setText("Nombre: Buenos Aires");
            dbQuiniela.nombreSorteo = "Buenos_Aires";

            ui->comandos->setText("Seleccionar Tipo de la Quiniela");

            ui->opcion01->setText("1 - Primera");
            ui->opcion02->setText("2 - Matutina");
            ui->opcion03->setText("3 - Vespertina");
            ui->opcion04->setText("4 - Nocturna");

            break;

        case Qt::Key_3:
            estado = 1;
            ui->nombreQuiniela->setText("Nombre: Santa Fe");
            dbQuiniela.nombreSorteo = "Santa_Fe";

            ui->comandos->setText("Seleccionar Tipo de la Quiniela");

            ui->opcion01->setText("1 - Primera");
            ui->opcion02->setText("2 - Matutina");
            ui->opcion03->setText("3 - Vespertina");
            ui->opcion04->setText("4 - Nocturna");

            break;

        case Qt::Key_4:
            estado = 1;
            ui->nombreQuiniela->setText("Nombre: Cordoba");
            dbQuiniela.nombreSorteo = "Cordoba";

            ui->comandos->setText("Seleccionar Tipo de la Quiniela");

            ui->opcion01->setText("1 - Primera");
            ui->opcion02->setText("2 - Matutina");
            ui->opcion03->setText("3 - Vespertina");
            ui->opcion04->setText("4 - Nocturna");

            break;

        default:
            ui->comandos->setText("Seleccionar Opcion Valida");
            break;
        }
        break;

    case 1:
        switch (event->key())
        {
        case Qt::Key_1:
            estado = 2;
            ui->tipoQuiniela->setText("Tipo: Primera");
            dbQuiniela.tipoSorteo = "Primera";

            ui->comandos->setText("Seleccionar la Fecha de la Quiniela (DD-MM-AAAA)");

            ui->opcion01->clear();
            ui->opcion02->clear();
            ui->opcion03->clear();
            ui->opcion04->clear();

            break;

        case Qt::Key_2:
            estado = 2;
            ui->tipoQuiniela->setText("Tipo: Matutina");
            dbQuiniela.tipoSorteo = "Matutina";

           ui->comandos->setText("Seleccionar la Fecha de la Quiniela (DD-MM-AAAA)");

            ui->opcion01->clear();
            ui->opcion02->clear();
            ui->opcion03->clear();
            ui->opcion04->clear();

            break;

        case Qt::Key_3:
            estado = 2;
            ui->tipoQuiniela->setText("Tipo: Vespertina");
            dbQuiniela.tipoSorteo = "Vespertina";

           ui->comandos->setText("Seleccionar la Fecha de la Quiniela (DD-MM-AAAA)");

            ui->opcion01->clear();
            ui->opcion02->clear();
            ui->opcion03->clear();
            ui->opcion04->clear();

            break;

        case Qt::Key_4:
            estado = 2;
            ui->tipoQuiniela->setText("Tipo: Nocturna");
            dbQuiniela.tipoSorteo = "Nocturna";

            ui->comandos->setText("Seleccionar la Fecha de la Quiniela (DD-MM-AAAA)");

            ui->opcion01->clear();
            ui->opcion02->clear();
            ui->opcion03->clear();
            ui->opcion04->clear();

            break;

        default:
            ui->comandos->setText("Seleccionar Opcion Valida");
            break;
        }
        break;

    case 2:
        fechaDia += event->text();

        ui->fechaQuiniela->setText("Fecha: " + fechaDia + " - MM - AAAA");


        if(fechaDia.length() > 1)
        {
            estado = 3;
        }
        break;

    case 3:
        fechaMes += event->text();

        ui->fechaQuiniela->setText("Fecha: " + fechaDia + " - " + fechaMes + " - AAAA");


        if(fechaMes.length() > 1)
        {
            estado = 4;
        }
        break;

    case 4:
        fechaAnio += event->text();

        ui->fechaQuiniela->setText("Fecha: " + fechaDia + " - " + fechaMes + " - " + fechaAnio);

        if(fechaAnio.length() > 3)
        {
            estado = 5;

            dbQuiniela.fechaSorteo = fechaAnio + fechaMes + fechaDia;

            if ( dbQuiniela.seleccionarSorteoUnico())
            {
                ui->comandos->setText("Quiniela NO Encontrada - Presione cualquier tecla para Salir");
            }
            else
            {
                ui->comandos->setText("Quiniela Encontrada - Presione cualquier tecla para Salir");

                ui->premio01->setText("Numero 01: " + dbQuiniela.premiosSorteo[0]);
                ui->premio02->setText("Numero 02: " + dbQuiniela.premiosSorteo[1]);
                ui->premio03->setText("Numero 03: " + dbQuiniela.premiosSorteo[2]);
                ui->premio04->setText("Numero 04: " + dbQuiniela.premiosSorteo[3]);
                ui->premio05->setText("Numero 05: " + dbQuiniela.premiosSorteo[4]);
                ui->premio06->setText("Numero 06: " + dbQuiniela.premiosSorteo[5]);
                ui->premio07->setText("Numero 07: " + dbQuiniela.premiosSorteo[6]);
                ui->premio08->setText("Numero 08: " + dbQuiniela.premiosSorteo[7]);
                ui->premio09->setText("Numero 09: " + dbQuiniela.premiosSorteo[8]);
                ui->premio10->setText("Numero 10: " + dbQuiniela.premiosSorteo[9]);
                ui->premio11->setText("Numero 11: " + dbQuiniela.premiosSorteo[10]);
                ui->premio12->setText("Numero 12: " + dbQuiniela.premiosSorteo[11]);
                ui->premio13->setText("Numero 13: " + dbQuiniela.premiosSorteo[12]);
                ui->premio14->setText("Numero 14: " + dbQuiniela.premiosSorteo[13]);
                ui->premio15->setText("Numero 15: " + dbQuiniela.premiosSorteo[14]);
                ui->premio16->setText("Numero 16: " + dbQuiniela.premiosSorteo[15]);
                ui->premio17->setText("Numero 17: " + dbQuiniela.premiosSorteo[16]);
                ui->premio18->setText("Numero 18: " + dbQuiniela.premiosSorteo[17]);
                ui->premio19->setText("Numero 19: " + dbQuiniela.premiosSorteo[18]);
                ui->premio20->setText("Numero 20: " + dbQuiniela.premiosSorteo[19]);

            }
        }
        break;

    case 5:
        close();
        break;
    default:
        break;
    }

}
