#ifndef POPALADIN_H
#define POPALADIN_H

#include <QDialog>
#include <QtGui>
#include <define.h>


class aladinThread : public QThread
{
    Q_OBJECT
public:
    explicit aladinThread();
    void run();
signals:

public slots:

};

namespace Ui {
class popaladin;
}

class popaladin : public QDialog
{
    Q_OBJECT
    
public:
    explicit popaladin(QWidget *parent = 0);

    QBasicTimer timer;
    QGraphicsOpacityEffect *opaco;

    typedef struct _DISPLAY
    {
        int X1;
        int X2;
        int X3;
        int X4;
        int Y;
        int Alto;
        int Ancho;
        int Unidades;
        int Decenas;
        int Centanas;
        int Miles;
    }DISPLAY;
    DISPLAY Display;

    typedef struct _RELOJ
    {
        int Clock;
        int Contador;
    }RELOJ;
    RELOJ Reloj;

    void ConfiguracionInicial ( void );
    void Resolucion           ( void );
    void AjustarParametros    ( void );
    void PosicionarElementos  ( void );
    void timerEvent           ( QTimerEvent *event );

    ~popaladin();
    
private:
    Ui::popaladin *ui;
    aladinThread *t;
};

#endif // POPALADIN_H
