#include "configuracion.h"
#include "ui_configuracion.h"

/***************************************************************************************************/
/* Constructor de la inteface de Configuracion                                                     */
/***************************************************************************************************/

/**
 \fn Configuracion::Configuracion(QWidget *parent):QDialog(parent),ui(new Ui::Configuracion)
 \brief Constructor.
 \note Constructor de la inteface de Configuracion.
*/
Configuracion::Configuracion(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Configuracion)
{
    ui->setupUi(this);

    // Cargar Titulo
    ui->configuracionTituloSuperior->setText("TimBear");
    ui->configuracionTituloInferior->setText("MENU DE CONFIGURACION");
    ui->tituloSuperior ->setText("Seleccionar la Opcion Indicada");
    ui->tituloInferior ->clear();

    // Cargar Opciones Capa 1
    ui->configuracionOpcion1->setText("1 - Cargar Quiniela Slot 1");
    ui->configuracionOpcion2->setText("2 - Cargar Quiniela Slot 2");
    ui->configuracionOpcion3->setText("3 - Cargar Quiniela Slot 3");
    ui->configuracionOpcion4->setText("4 - Cargar Quiniela Slot 4");
    ui->configuracionOpcion5->setText("5 - Numero del Dia");
    ui->configuracionOpcion6->setText("6 - Cargar Publicidad Mediante USB");
    ui->configuracionOpcion7->setText("7 - Actualizar Firmware");
    ui->configuracionOpcion8->setText("8 - Acerca de TimBear");
    ui->configuracionOpcion9->setText("9 - Configuracion");
    ui->configuracionOpcion0->setText("0 - Salir");

   ui->configuracionInferior->clear();

    ui->imagenIzquierda->setPixmap(QPixmap("extras/logo_Oso_Izquierda") );
    ui->imagenDerecha->setPixmap(QPixmap("extras/logo_Oso_Derecha") );

    limpiarColumna1();

//    memset(&quinielaCargaManual,0,sizeof(quinielaCargaManual));

    estado = ESTADO_INICIAL;
    posicion = 0;
    tiempo = 0;
    flagInicio = 1;

    versionActual = 0;
    versionDisponible = 0;

    fechaActual = "AA-MM-DD";
    horaActual = "hh:mm";

    paletteBlue = new QPalette();
    paletteBlue->setColor(QPalette::WindowText,Qt::blue);
    paletteBlack = new QPalette();
    paletteBlack->setColor(QPalette::WindowText,Qt::black);

    timer.start(100,this);

}

/***************************************************************************************************/
/* Destructor de la inteface de Configuracion                                                      */
/***************************************************************************************************/

/**
 \fn Configuracion::~Configuracion()
 \brief Destructor.
 \note Destructor de la inteface de Configuracion.
*/
Configuracion::~Configuracion()
{
    delete ui;
    printf("Destructor del menu de Configuracion\n");
}

/***************************************************************************************************/
/* Funcion que se activa cuando el tiempo se acaba en el Timer "timer"                             */
/***************************************************************************************************/

/**
 \fn void Configuracion::timerEvent(QTimerEvent *event)
 \brief Cerrar el Menu Configuracion
 \note Cerrar el Menu Configuracion despues de transcurido un determinado tiempo y al no presionar ninguna tecla
*/
void Configuracion::timerEvent(QTimerEvent *event)
{
    if(tiempo == TIEMPO_INACTIVIDAD)
    {
        event = event;
        printf("Se acabo el Tiempo del Menu Configuracion\n");
        timer.stop();
        close();
    }

    if(flagInicio)
    {
        this->setWindowOpacity(0.1*tiempo);
        if(tiempo == 10)
            flagInicio = 0;
    }

    tiempo++;
}



/***************************************************************************************************/
/* Remarca la linea correspondiente a la "opcion"                                                  */
/***************************************************************************************************/

/**
 \fn void Configuracion::seleccionarOpcion0(int opcion)
 \brief Remarca la linea correspondiente a la "opcion".
 \note Remarca la linea correspondiente a la "opcion".
 \param [in] int opcion: Numero de la linea que debe ser Remarcada.
*/

void Configuracion::seleccionarOpcion1(int opcion)
{
    ui->configuracionOpcion11->setPalette(*paletteBlack);
    ui->configuracionOpcion12->setPalette(*paletteBlack);
    ui->configuracionOpcion13->setPalette(*paletteBlack);
    ui->configuracionOpcion14->setPalette(*paletteBlack);
    ui->configuracionOpcion15->setPalette(*paletteBlack);
    ui->configuracionOpcion16->setPalette(*paletteBlack);
    ui->configuracionOpcion17->setPalette(*paletteBlack);
    ui->configuracionOpcion18->setPalette(*paletteBlack);
    ui->configuracionOpcion19->setPalette(*paletteBlack);
    ui->configuracionOpcion10->setPalette(*paletteBlack);

    switch (opcion) {
    case 1:
        ui->configuracionOpcion11->setPalette(*paletteBlue);
        break;
    case 2:
        ui->configuracionOpcion12->setPalette(*paletteBlue);
        break;
    case 3:
        ui->configuracionOpcion13->setPalette(*paletteBlue);
        break;
    case 4:
        ui->configuracionOpcion14->setPalette(*paletteBlue);
        break;
    case 5:
        ui->configuracionOpcion15->setPalette(*paletteBlue);
        break;
    case 6:
        ui->configuracionOpcion16->setPalette(*paletteBlue);
        break;
    case 7:
        ui->configuracionOpcion17->setPalette(*paletteBlue);
        break;
    case 8:
        ui->configuracionOpcion18->setPalette(*paletteBlue);
        break;
    case 9:
        ui->configuracionOpcion19->setPalette(*paletteBlue);
        break;
    default:
        break;
    }

}

/***************************************************************************************************/
/* Remarca la linea correspondiente a la "opcion"                                                  */
/***************************************************************************************************/

/**
 \fn void Configuracion::seleccionarOpcion0(int opcion)
 \brief Remarca la linea correspondiente a la "opcion".
 \note Remarca la linea correspondiente a la "opcion".
 \param [in] int opcion: Numero de la linea que debe ser Remarcada.
*/

void Configuracion::seleccionarOpcion0(int opcion)
{
    ui->configuracionOpcion1->setPalette(*paletteBlack);
    ui->configuracionOpcion2->setPalette(*paletteBlack);
    ui->configuracionOpcion3->setPalette(*paletteBlack);
    ui->configuracionOpcion4->setPalette(*paletteBlack);
    ui->configuracionOpcion5->setPalette(*paletteBlack);
    ui->configuracionOpcion6->setPalette(*paletteBlack);
    ui->configuracionOpcion7->setPalette(*paletteBlack);
    ui->configuracionOpcion8->setPalette(*paletteBlack);
    ui->configuracionOpcion9->setPalette(*paletteBlack);
    ui->configuracionOpcion0->setPalette(*paletteBlack);

    switch (opcion) {
    case 1:
        ui->configuracionOpcion1->setPalette(*paletteBlue);
        break;
    case 2:
        ui->configuracionOpcion2->setPalette(*paletteBlue);
        break;
    case 3:
        ui->configuracionOpcion3->setPalette(*paletteBlue);
        break;
    case 4:
        ui->configuracionOpcion4->setPalette(*paletteBlue);
        break;
    case 5:
        ui->configuracionOpcion5->setPalette(*paletteBlue);
        break;
    case 6:
        ui->configuracionOpcion6->setPalette(*paletteBlue);
        break;
    case 7:
        ui->configuracionOpcion7->setPalette(*paletteBlue);
        break;
    case 8:
        ui->configuracionOpcion8->setPalette(*paletteBlue);
        break;
    case 9:
        ui->configuracionOpcion9->setPalette(*paletteBlue);
        break;
    default:
        break;
    }

}

/***************************************************************************************************/
/* Limpiar el texto de todos los "labels" de la columna 0                                          */
/***************************************************************************************************/

/**
 \fn void Configuracion::limpiarColumna0()
 \brief Limpiar el texto.
 \note Limpiar el texto de todos los "labels" de la columna 0.
*/

void Configuracion::limpiarColumna0()
{
    ui->configuracionOpcion0->clear();
    ui->configuracionOpcion1->clear();
    ui->configuracionOpcion2->clear();
    ui->configuracionOpcion3->clear();
    ui->configuracionOpcion4->clear();
    ui->configuracionOpcion5->clear();
    ui->configuracionOpcion6->clear();
    ui->configuracionOpcion7->clear();
    ui->configuracionOpcion8->clear();
    ui->configuracionOpcion9->clear();
}

/***************************************************************************************************/
/* Limpiar el texto de todos los "labels" de la columna 1                                          */
/***************************************************************************************************/

/**
 *\fn       void Configuracion::limpiarColumna1()
 *\brief    Limpiar el texto.
 *\note     Limpiar el texto de todos los "labels" de la columna 1.
*/

void Configuracion::limpiarColumna1()
{
    ui->configuracionOpcion10->clear();
    ui->configuracionOpcion11->clear();
    ui->configuracionOpcion12->clear();
    ui->configuracionOpcion13->clear();
    ui->configuracionOpcion14->clear();
    ui->configuracionOpcion15->clear();
    ui->configuracionOpcion16->clear();
    ui->configuracionOpcion17->clear();
    ui->configuracionOpcion18->clear();
    ui->configuracionOpcion19->clear();
}

/***************************************************************************************************/
/* Vizualizar los numeros en la opcion de la carga manual                                          */
/***************************************************************************************************/

/**
 *\fn       void Configuracion::limpiarColumna1()
 *\brief    Vizualizar los numeros.
 *\note     Vizualizar los numeros en la opcion de la carga manual.
            Dependiendo la posicion va mostrando los numeros cargados manualmente.
*/

void Configuracion::visualizarNumeros()
{
    switch (posicion + 1) {
    case 1:
        ui->configuracionOpcion1->setText("Numero 01 : " + quinielaCargaManual.premiosSorteo[posicion]);
        ui->configuracionOpcion1->setPalette(*paletteBlue);
        break;
    case 2:
        ui->configuracionOpcion2->setText("Numero 02 : " + quinielaCargaManual.premiosSorteo[posicion]);
        ui->configuracionOpcion2->setPalette(*paletteBlue);
        break;
    case 3:
        ui->configuracionOpcion3->setText("Numero 03 : " + quinielaCargaManual.premiosSorteo[posicion]);
        ui->configuracionOpcion3->setPalette(*paletteBlue);
        break;
    case 4:
        ui->configuracionOpcion4->setText("Numero 04 : " + quinielaCargaManual.premiosSorteo[posicion]);
        ui->configuracionOpcion4->setPalette(*paletteBlue);
        break;
    case 5:
        ui->configuracionOpcion5->setText("Numero 05 : " + quinielaCargaManual.premiosSorteo[posicion]);
        ui->configuracionOpcion5->setPalette(*paletteBlue);
        break;
    case 6:
        ui->configuracionOpcion6->setText("Numero 06 : " + quinielaCargaManual.premiosSorteo[posicion]);
        ui->configuracionOpcion6->setPalette(*paletteBlue);
        break;
    case 7:
        ui->configuracionOpcion7->setText("Numero 07 : " + quinielaCargaManual.premiosSorteo[posicion]);
        ui->configuracionOpcion7->setPalette(*paletteBlue);
        break;
    case 8:
        ui->configuracionOpcion8->setText("Numero 08 : " + quinielaCargaManual.premiosSorteo[posicion]);
        ui->configuracionOpcion8->setPalette(*paletteBlue);
        break;
    case 9:
        ui->configuracionOpcion9->setText("Numero 09 : " + quinielaCargaManual.premiosSorteo[posicion]);
        ui->configuracionOpcion9->setPalette(*paletteBlue);
        break;
    case 10:
        ui->configuracionOpcion0->setText("Numero 10 : " + quinielaCargaManual.premiosSorteo[posicion]);
        ui->configuracionOpcion0->setPalette(*paletteBlue);
        break;
    case 11:
        ui->configuracionOpcion11->setText("Numero 11 : " + quinielaCargaManual.premiosSorteo[posicion]);
        ui->configuracionOpcion11->setPalette(*paletteBlue);
        break;
    case 12:
        ui->configuracionOpcion12->setText("Numero 12 : " + quinielaCargaManual.premiosSorteo[posicion]);
        ui->configuracionOpcion12->setPalette(*paletteBlue);
        break;
    case 13:
        ui->configuracionOpcion13->setText("Numero 13 : " + quinielaCargaManual.premiosSorteo[posicion]);
        ui->configuracionOpcion13->setPalette(*paletteBlue);
        break;
    case 14:
        ui->configuracionOpcion14->setText("Numero 14 : " + quinielaCargaManual.premiosSorteo[posicion]);
        ui->configuracionOpcion14->setPalette(*paletteBlue);
        break;
    case 15:
        ui->configuracionOpcion15->setText("Numero 15 : " + quinielaCargaManual.premiosSorteo[posicion]);
        ui->configuracionOpcion15->setPalette(*paletteBlue);
        break;
    case 16:
        ui->configuracionOpcion16->setText("Numero 16 : " + quinielaCargaManual.premiosSorteo[posicion]);
        ui->configuracionOpcion16->setPalette(*paletteBlue);
        break;
    case 17:
        ui->configuracionOpcion17->setText("Numero 17 : " + quinielaCargaManual.premiosSorteo[posicion]);
        ui->configuracionOpcion17->setPalette(*paletteBlue);
        break;
    case 18:
        ui->configuracionOpcion18->setText("Numero 18 : " + quinielaCargaManual.premiosSorteo[posicion]);
        ui->configuracionOpcion18->setPalette(*paletteBlue);
        break;
    case 19:
        ui->configuracionOpcion19->setText("Numero 19 : " + quinielaCargaManual.premiosSorteo[posicion]);
        ui->configuracionOpcion19->setPalette(*paletteBlue);
        break;
    case 20:
        ui->configuracionOpcion10->setText("Numero 20 : " + quinielaCargaManual.premiosSorteo[posicion]);
        ui->configuracionOpcion10->setPalette(*paletteBlue);
        break;
    default:
        break;
    }
}

