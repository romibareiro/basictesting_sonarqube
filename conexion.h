#ifndef CONEXION_H
#define CONEXION_H

#include <QMainWindow>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <sys/syscall.h>
#include <QDateTime>
#include <QLineEdit>

#include <QMainWindow>
#include <qlcdnumber.h>
#include <QLCDNumber>
#include <QTextBrowser>

#define INICIO_LINEA "<tr>"
#define FIN_LINEA "</tr>"
#define INICIO_ESPACIO "<td>"
#define FIN_ESPACIO "</td>"

#define INICIO_DATOS "DatosSorteoQuiniela"
#define FECHA "Fecha"
#define SORTEO "Sorteo"
#define LETRAS "LetrasQuiniela"

#define PRIMERA_COLUMNA "Columna1Quiniela"
#define NUMEROS "Columna1Quiniela"
#define SEGUNDA_COLUMNA "Columna2Quiniela"

#define INICIO_RESALTE "<strong>"
#define FIN_RESALTE "</strong>"

#define INICIO_CENTER "<center>"
#define FIN_CENTER "</center>"
#include <QMainWindow>
#include <qlcdnumber.h>
#include <QLCDNumber>
#include <QTextBrowser>

#define INICIO_LINEA "<tr>"
#define FIN_LINEA "</tr>"
#define INICIO_ESPACIO "<td>"
#define FIN_ESPACIO "</td>"

#define INICIO_DATOS "DatosSorteoQuiniela"
#define FECHA "Fecha"
#define SORTEO "Sorteo"
#define LETRAS "LetrasQuiniela"

#define PRIMERA_COLUMNA "Columna1Quiniela"
#define NUMEROS "Columna1Quiniela"
#define SEGUNDA_COLUMNA "Columna2Quiniela"


#define INICIO_RESALTE "<strong>"
#define FIN_RESALTE "</strong>"

#define INICIO_CENTER "<center>"
#define FIN_CENTER "</center>"

#define COMANDO_WGET    "wget -O "
#define COMANDO_PAGINA_WEB_INI "\"http://www.loteria-nacional.gov.ar/Internet/Extractos/resultados_i.php?juego=quiniela&fechasorteo="
#define COMANDO_PAGINA_WEB_MED "&tiposorteo="
#define COMANDO_PAGINA_WEB_FIN "\""
#define COMANDO_TXT ".txt"

#define QUINIELA_NACIONAL "nacional"

#define PRIMERA "PRIMERA"
#define MATUTINA "MATUTINA"
#define NOCTURNA "NOCTURNA"
#define VESPERTINA "VESPERTINA"

// aca cambiate el nombre por tu carpeta
#define RUTA_ARCHIVOS "/home/romi/Quinielas/"

#define WGET_LENGTH     140
#define TIME_SLEEP      3
#define BUFF_LENGTH     220

#define NUMB_LINES      35



void callToWeb(char*, QString, QString);

void callTuJugada( QString , QString );

QString descapadoResalte(QString *sOriginal,int *found);
QString descapadoCentrado(QString *sOriginal,int *found);
QString descapadoEspacios(QString *sOriginal,int *found);

#endif // CONEXION_H
