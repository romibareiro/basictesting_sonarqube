#include "myTCPClient.h"

#define PEDIDO_QUINIELAS 1
#define PEDIDO_PUBLICIDAD_ID 2
#define PEDIDO_PUBLICIDAD_IMAGEN 3
#define PEDIDO_GENERAL 4

extern int serialNumber;

bool estadoConexion;

MyTCPClient::MyTCPClient()
{

    socket = new QTcpSocket(this);

//  socket->connectToHost("127.0.0.1",5001);
    socket->connectToHost( IP_SERVER, PORT_SERVER);

    if(socket->isOpen())
        qDebug() << "Socket Abieto";
    else
        qDebug() << "Socket Cerrado";

    sincronizarImagen = 0;
    data = new QByteArray();

    connect(socket , SIGNAL(readyRead()),this,SLOT(readMyData()));

}

MyTCPClient::~MyTCPClient()
{
    qDebug() << "Destructor del TCPClient";
}

void MyTCPClient::writeMyData(QString solicitud )
{

    qDebug() << solicitud;

    socket->write( solicitud.toLatin1(), solicitud.length() );
}

void MyTCPClient::readMyData()
{

    QString recibir;
    int i;
    databaseQuiniela Quiniela;
    QByteArray ByteArrayAux;

    estadoConexion = 0;

    if(sincronizarImagen)
    {
        *data = *data + socket->readAll();

        qDebug() << "Leyo: " << data->size();

        if( data->size() == pesoImagen)
        {
            qDebug() << "Termino Sincronizacion";
            FILE *fp;
            fp = fopen(nombreImagen.toLatin1(),"wb");
            fwrite(data->data(), 1 , data->length(), fp);
            sincronizarImagen = 0;
            fclose(fp);

            actualizarIDPublicidadFija( id );


        }
    }
    else
    {
    //qDebug() << socket->readAll() ;

        recibir = socket->readAll();

        qDebug() << recibir;

        JsonValue jsonOriginal(recibir.toLatin1());
        JsonValue jsonAux("");

        jsonAux = jsonOriginal.findChild("pedido");

        ByteArrayAux = jsonAux.data();

        qDebug() << jsonOriginal.toString(1,1);

        switch ( ByteArrayAux.toInt() ) {
        case PEDIDO_QUINIELAS:

//    recibir = "{\"id\":55550000,\"nombre\":\"Nacional\",\"tipo\":\"Matutina\",\"numero\":\"12345\",\"fecha\":\"16/10/2014\",";
//    recibir += "\"premio\":[\"0100\",\"0200\",\"0300\",\"0400\",\"0500\",\"0600\",\"0700\",\"0800\",\"0900\",\"1000\",\"1100\",\"1200\",\"1300\",\"1400\",\"1500\",\"1600\",\"1700\",\"1800\",\"1900\",\"2000\"],\"letra\":\"ASDF\"}";

            i = jsonParseQuiniela ( recibir.toLatin1(), &Quiniela );

            qDebug() << "ID del Equipo:" << i;

            qDebug() << "Nombre Quiniela:" << Quiniela.nombreSorteo;

            qDebug() << "Tipo Quiniela:" << Quiniela.tipoSorteo;

            qDebug() << "Numero Quiniela:" << Quiniela.numeroSorteo;

            qDebug() << "Fecha Quiniela:" <<  Quiniela.fechaSorteo;

            for( i = 0 ; i < 20 ; i++)
            {
                qDebug() << "Premio" << i << ":" <<  Quiniela.premiosSorteo[i];
            }

            qDebug() << "Letras Quiniela:" <<  Quiniela.letrasSorteo;

            Quiniela.insertarSorteo();

            break;

        case PEDIDO_PUBLICIDAD_ID:

            id[0] = 0;
            id[1] = 0;
            id[2] = 0;
            id[3] = 0;

            existePublicidadFija( id );

            jsonAux = jsonOriginal.findChild("idp1");
            ByteArrayAux = jsonAux.data();

            if( ByteArrayAux.toInt() != id [0])  // sincronizarPublicidad( id[0]);
            {
                 qDebug() << "Sincronizar Publicidad 1";
                sincronizarImagen = 1;
                nombreImagen = "extras/publicidadFija/0.jpg";

                jsonAux = jsonOriginal.findChild("peso1");
                ByteArrayAux = jsonAux.data();

                pesoImagen = ByteArrayAux.toInt();

                jsonAux = jsonOriginal.findChild("idp1");
                ByteArrayAux = jsonAux.data();

                id[0] = ByteArrayAux.toInt();

                QString texto;

                qDebug() << "Peso de la Imagen Sincronizar" << pesoImagen;


                texto   = "{\"id\":"  + QString::number(serialNumber) +  ",\"pedido\":3,\"idp\":"
                        + ByteArrayAux
                        + "}";

                writeMyData(texto);
                data->clear();
            }
            else
            {
                jsonAux = jsonOriginal.findChild("idp2");
                ByteArrayAux = jsonAux.data();

                if( ByteArrayAux.toInt() != id [1]) // sincronizarPublicidad( id[1]);
                {
                    qDebug() << "Sincronizar Publicidad 2";
                   sincronizarImagen = 1;
                   nombreImagen = "extras/publicidadFija/1.jpg";

                   jsonAux = jsonOriginal.findChild("peso2");
                   ByteArrayAux = jsonAux.data();

                   pesoImagen = ByteArrayAux.toInt();

                   jsonAux = jsonOriginal.findChild("idp2");
                   ByteArrayAux = jsonAux.data();

                   id[1] = ByteArrayAux.toInt();

                   QString texto;

                   qDebug() << "Peso de la Imagen a Sincronizar:" << pesoImagen;


                   texto   = "{\"id\":"  + QString::number(serialNumber) +  ",\"pedido\":3,\"idp\":"
                           + ByteArrayAux
                           + "}";

                   writeMyData(texto);
                   data->clear();
                }
                else
                {
                    jsonAux = jsonOriginal.findChild("idp3");
                    ByteArrayAux = jsonAux.data();

                    if( ByteArrayAux.toInt() != id [2]) // sincronizarPublicidad( id[2]);
                    {
                        qDebug() << "Sincronizar Publicidad 3";
                       sincronizarImagen = 1;
                       nombreImagen = "extras/publicidadFija/2.jpg";

                       jsonAux = jsonOriginal.findChild("peso3");
                       ByteArrayAux = jsonAux.data();

                       pesoImagen = ByteArrayAux.toInt();

                       jsonAux = jsonOriginal.findChild("idp3");
                       ByteArrayAux = jsonAux.data();

                       id[2] = ByteArrayAux.toInt();

                       QString texto;

                       qDebug() << "Peso de la Imagen a Sincronizar:" << pesoImagen;


                       texto   = "{\"id\":"  + QString::number(serialNumber) +  ",\"pedido\":3,\"idp\":"
                               + ByteArrayAux
                               + "}";

                       writeMyData(texto);
                       data->clear();
                    }
                    else
                    {
                        jsonAux = jsonOriginal.findChild("idp4");
                        ByteArrayAux = jsonAux.data();

                        if( ByteArrayAux.toInt() != id [3]) // sincronizarPublicidad( id[3]);
                        {
                            qDebug() << "Sincronizar Publicidad 4";
                           sincronizarImagen = 1;
                           nombreImagen = "extras/publicidadFija/3.jpg";

                           jsonAux = jsonOriginal.findChild("peso4");
                           ByteArrayAux = jsonAux.data();

                           pesoImagen = ByteArrayAux.toInt();

                           jsonAux = jsonOriginal.findChild("idp4");
                           ByteArrayAux = jsonAux.data();

                           id[3] = ByteArrayAux.toInt();


                           QString texto;

                           qDebug() << "Peso de la Imagen a Sincronizar:" << pesoImagen;


                           texto   = "{\"id\":"  + QString::number(serialNumber) +  ",\"pedido\":3,\"idp\":"
                                   + ByteArrayAux
                                   + "}";

                           writeMyData(texto);
                           data->clear();
                        }
                    }
                }
            }

            break;

    case PEDIDO_PUBLICIDAD_IMAGEN:

        break;

    case PEDIDO_GENERAL:

            jsonAux = jsonOriginal.findChild("versiondisponible");

            ByteArrayAux = jsonAux.data();

            setConfiguracion( "versiondisponible" , ByteArrayAux.toInt() );

            break;

    default:
        break;
    }

    }
}
