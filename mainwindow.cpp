#include "mainwindow.h"
#include "ui_mainwindow.h"

/***************************************************************************************************/
/* Funcion del Constructor de la Ventana Principal                                                 */
/***************************************************************************************************/
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);

    //system("uname -m");
    FILE *fp;

    fp = popen("uname -m", "r");

    plataformaEjecucion = 0;
    ocupadoTimer = 1;

    fgets(path, sizeof(path)-1, fp);

// Solo con la Placa
    if ( !(memcmp(path,"armv7l",strlen("armv7l")) ))
    {
        configuracionGPIO();
        plataformaEjecucion = 1;
        QTimer *timerGPIO = new QTimer(this);
        connect(timerGPIO, SIGNAL(timeout()), this, SLOT(onGPIO()));
        timerGPIO->start(1000);
    }

    pclose(fp);

// Solo con la Placa

//    paletteGeneral = new QPalette();

// Timer Principal del Programa.

    timerPrincipal = 0; // Estado Principal del Programa

    seleccionQuiniela = NACIONAL;

    seleccionSorteo =  SORTEO_PRIMERA;


//    Tiempo

//    QDateTime today;
//    QString fecha;

//    today = today.currentDateTime();
//    sprintf(dia,"%02d/%02/d%02d",today.date().day(),today.date().month(),today.date().year());

}

/***************************************************************************************************/
/* Funcion del Destructor de la Ventana Principal                                                  */
/***************************************************************************************************/
MainWindow::~MainWindow()
{
    delete ui;
}

/***************************************************************************************************/
/* Funcion de inicializacion de la Pantalla Principal                                              */
/***************************************************************************************************/
void MainWindow::onMainWindow( int Salida )
{
    if(Salida)
        Salida = Salida;

    clearConfiguracion();

    this->modificarProvincia();
    this->modificarPresentacion();
    this->showFullScreen();

    // Revisar si los Juegos estan Habilitados

    qDebug() << "Habilitacion juegos";
    habilitacionJuegos =  getConfiguracion("juegos");

    ui->numeroDia->setText("NUMERO HOY<br>" + QString::number(getConfiguracion("suerte")));

 //   pendrive= new USBImages;
 //     pendrive->exec();
  //delete pendrive;

    myCliente = new MyTCPClient;

    ocupadoTimer = 0;

    esquemaPOP = 0;

    timer.start(500,this);
}

/***************************************************************************************************/
/* Funcion Prueba Para Borrar la Base de Datos                                                     */
/***************************************************************************************************/
void MainWindow::borrarDataBase()
{
/*    sqlite3 *db = NULL;
    int rc;
    char sql[200];
    char *zErrMsg = 0;

    rc = sqlite3_open("extras/cliente.db", &db);

    if( rc ){
       fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
       exit(0);
    }else{
       fprintf(stderr, "Opened database successfully\n");
    }

    sprintf (sql, "DELETE from quiniela");
// Execute SQL statement
    rc = sqlite3_exec(db, sql, NULL , NULL , &zErrMsg);
    if( rc != SQLITE_OK ){
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }else{
        fprintf(stdout, "Operation done successfully\n");
    }
    sprintf (sql, "DELETE from premio");
// Execute SQL statement
    rc = sqlite3_exec(db, sql, NULL , NULL , &zErrMsg);
    if( rc != SQLITE_OK ){
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }else{
        fprintf(stdout, "Operation done successfully\n");
    }

    sqlite3_close(db);

    */

//    dbQuiniela.abrirDatabase("extras/QUINIELERO.db");

    dbQuiniela.fechaSorteo = "09/07/2017";
    dbQuiniela.numeroSorteo = "12556";
    dbQuiniela.nombreSorteo = "CAPITAL";
    dbQuiniela.tipoSorteo = "PRIMERA";
    dbQuiniela.letrasSorteo = "AAAA";

    dbQuiniela.premiosSorteo[0]  = "0001";
    dbQuiniela.premiosSorteo[1]  = "0002";
    dbQuiniela.premiosSorteo[2]  = "0003";
    dbQuiniela.premiosSorteo[3]  = "1234";
    dbQuiniela.premiosSorteo[4]  = "0005";
    dbQuiniela.premiosSorteo[5]  = "0006";
    dbQuiniela.premiosSorteo[6]  = "0007";
    dbQuiniela.premiosSorteo[7]  = "0008";
    dbQuiniela.premiosSorteo[8]  = "0009";
    dbQuiniela.premiosSorteo[9]  = "0010";
    dbQuiniela.premiosSorteo[10] = "0011";
    dbQuiniela.premiosSorteo[11] = "0012";
    dbQuiniela.premiosSorteo[12] = "0013";
    dbQuiniela.premiosSorteo[13] = "0014";
    dbQuiniela.premiosSorteo[14] = "0015";
    dbQuiniela.premiosSorteo[15] = "0016";
    dbQuiniela.premiosSorteo[16] = "0017";
    dbQuiniela.premiosSorteo[17] = "0018";
    dbQuiniela.premiosSorteo[18] = "0019";
    dbQuiniela.premiosSorteo[19] = "0020";

    dbQuiniela.insertarSorteo();

}

/***************************************************************************************************/
/* Funcion para Modificar el Entorno de la Ventana Principal segun la Ubicacion                    */
/***************************************************************************************************/
int MainWindow::modificarProvincia()
{
    int numero;

    numero = getConfiguracion("provincia");

    switch (numero) {
    case NACIONAL:
        ui->nombreProvincia->setText("Loteria Nacional");
        ui->Imagen->setPixmap(QPixmap("extras/logo_loteria_nacional.png") );
        break;

    case BUENOS_AIRES:
        ui->nombreProvincia->setText("Loteria de la <br> Provincia de Buenos Aires");
        ui->Imagen->setPixmap(QPixmap("extras/logo_loteria_buenos_aires.png") );
        break;

    case SANTA_FE:
        ui->nombreProvincia->setText("Loteria de la <br> Provincia de Santa Fe");
        ui->Imagen->setPixmap(QPixmap("extras/logo_loteria_santa_fe.jpg") );
        break;

    case CORDOBA:
        ui->nombreProvincia->setText("Loteria de la <br> Provincia de Cordoba");
        ui->Imagen->setPixmap(QPixmap("extras/logo_loteria_cordoba.png") );
        break;

    default:
        return -1;
        break;
    }    
    return 0;
}

/***************************************************************************************************/
/* Funcion para Modificar la Presentacion de la Ventana Principal                                  */
/***************************************************************************************************/
int MainWindow::modificarPresentacion()
{
    formaPresentacion = getConfiguracion("presentacion");

    // Inicializar Variables para no tener problemas
    seleccionQuiniela = NACIONAL;
    seleccionSorteo =  SORTEO_PRIMERA;

    switch (formaPresentacion) {
    case FORMA_CABEZAS:
        ui->SorteoCW->setVisible(false);
        ui->SorteoAW->setVisible(false);
        ui->LineaCabeza->setVisible(false);
        ui->LineaSorteos->setVisible(false);

        ui->CabezasW->setVisible(true);
        break;

    case FORMA_COMPLETA:
        ui->CabezasW->setVisible(false);
        ui->LineaCabeza->setVisible(false);

        ui->SorteoAW->setVisible(true);
        ui->LineaSorteos->setVisible(true);
        ui->SorteoCW->setVisible(true);
        break;

    case FORMA_MIXTA:
        ui->LineaSorteos->setVisible(false);
        ui->SorteoCW->setVisible(false);

        ui->CabezasW->setVisible(true);
        ui->LineaCabeza->setVisible(true);
        ui->SorteoAW->setVisible(true);
        break;
    default:
        return -1;
        break;
    }
    return 0;
}

/***************************************************************************************************/
/* Funcion para Obtener una Quiniela de la Web                                                     */
/***************************************************************************************************/
int MainWindow::obtenerQuiniela(QString nombreSorteo, QString tipoSorteo, QString fechaSorteo)
{
    nombreSorteo = nombreSorteo;
// Llama a la funcion para obtener los datos de la web
// callToWeb("quiniela.txt", dia, tipo);
    callToWeb((char *) ARCHIVO_AUXILIAR, fechaSorteo, tipoSorteo);
// Transfiere los datos obtenidos de la web a la Base de Datos
    passToDBO((char *) ARCHIVO_AUXILIAR, fechaSorteo.toLatin1().data(), tipoSorteo.toLatin1().data());

    return true;
}

/***************************************************************************************************/
/* Funcion Mostrar los Numeros en Pantalla                                                         */
/***************************************************************************************************/
/*void MainWindow::mostrarNumeros()
{

    QDateTime today;
    QString fecha;

    databaseQuiniela dbQuiniela;

    today=today.currentDateTime();
//    sprintf(dia,"%02d%02d%02d",today.date().day(),today.date().month(),today.date().year());
//    fecha.sprintf("%02d/%02d/%02d",today.date().day()-5,today.date().month(),today.date().year());

    fecha = "13/08/2014";

// Mostrar Quiniela A

    dbQuiniela.nombreSorteo = "nacional";

    dbQuiniela.tipoSorteo = "mat";

    dbQuiniela.fechaSorteo = fecha;

//   dbQuiniela.abrirDatabase((char *)"extras/QUINIELERO.db");

    if ( dbQuiniela.seleccionarSorteo())
        printf("Error con la carga de la Columna 1");
    else
        mostrarQuinielaA(&dbQuiniela);

// Mostrar Quiniela B


    dbQuiniela.nombreSorteo = "nacional";

    dbQuiniela.tipoSorteo = "mat";

    dbQuiniela.fechaSorteo = fecha;

//    dbQuiniela.abrirDatabase((char *)"extras/QUINIELERO.db");

    if ( dbQuiniela.seleccionarSorteo())
        printf("Error con la carga de la Columna 2");
    else
        mostrarQuinielaB(&dbQuiniela);

// Mostrar Quiniela C

    dbQuiniela.nombreSorteo = "nacional";

    dbQuiniela.tipoSorteo = "ves";

    dbQuiniela.fechaSorteo = fecha;

//    dbQuiniela.abrirDatabase((char *)"extras/QUINIELERO.db");

    if ( dbQuiniela.seleccionarSorteo())
        printf("Error con la carga de la Columna 3");
    else
        mostrarQuinielaC(&dbQuiniela);

// Mostrar Quiniela D

    dbQuiniela.nombreSorteo = "nacional";

    dbQuiniela.tipoSorteo = "ves";

    dbQuiniela.fechaSorteo = fecha;

//    dbQuiniela.abrirDatabase((char *)"extras/QUINIELERO.db");

    if ( dbQuiniela.seleccionarSorteo())
        printf("Error con la carga de la Columna 4");
    else
        mostrarQuinielaD(&dbQuiniela);

  else //  (??????) XQ FER? XQ?
    {
        // Mostrar Quiniela A
//            memset(&quinielam,0,sizeof(QUINIELA));

            quinielam.nombre = "Manual";

            quinielam.tipo = 1;

            quinielam.fecha = "112233";

            getQuiniela(&quinielam);

            mostrarQuinielaA(&quinielam);

            // Mostrar Quiniela B

//           memset(&quinielam,0,sizeof(QUINIELA));

            quinielam.nombre = "Manual";

            quinielam.tipo = 2;

            quinielam.fecha = "112233";

            getQuiniela(&quinielam);

            mostrarQuinielaB(&quinielam);

            // Mostrar Quiniela C

//            memset(&quinielam,0,sizeof(QUINIELA));

            quinielam.nombre = "Manual";

            quinielam.tipo = 3;

            quinielam.fecha = "112233";

            getQuiniela(&quinielam);

            mostrarQuinielaC(&quinielam);

    }

}
*/
/***************************************************************************************************/
/* Funcion para Configurar la Fecha Actual                                                         */
/***************************************************************************************************
void MainWindow::on_ConfigurarFechaB_clicked()
{
//    QDateTime today;
//    char dia[15];
//    char tipo[10];

    fec = new Fecha;
    fec->exec();

    today=today.currentDateTime();
//    sprintf(dia,"%02d%02d%02d",today.date().day(),today.date().month(),today.date().year());
        sprintf(dia,"%02d%02d%02d",07,today.date().month(),today.date().year());
    sprintf(tipo,"ves");
// Llama a la funcion para obtener los datos de la web
// callToWeb("quiniela.txt", dia, tipo);

    callToWeb((char *) ARCHIVO_AUXILIAR, dia, tipo);

    passToDBO((char *) ARCHIVO_AUXILIAR, dia, tipo);

// Mostrar Quiniela
    QUINIELA quinielam;

    quinielam.nombre = "nacional";

    quinielam.tipo = 2;

    quinielam.fecha = "07052014";

    getQuiniela(&quinielam);

    ui->NombreSorteoA->setText("Nacional");
    ui->FechaSorteoA->setText(quinielam.fecha);

    putMyValue(this->ui->NumeroSorteoB01,quinielam.numero1);
    putMyValue(this->ui->NumeroSorteoB02,quinielam.numero2);
    putMyValue(this->ui->NumeroSorteoB03,quinielam.numero3);
    putMyValue(this->ui->NumeroSorteoB04,quinielam.numero4);
    putMyValue(this->ui->NumeroSorteoB05,quinielam.numero5);
    putMyValue(this->ui->NumeroSorteoB06,quinielam.numero6);
    putMyValue(this->ui->NumeroSorteoB07,quinielam.numero7);
    putMyValue(this->ui->NumeroSorteoB08,quinielam.numero8);
    putMyValue(this->ui->NumeroSorteoB09,quinielam.numero9);
    putMyValue(this->ui->NumeroSorteoB10,quinielam.numero10);


}
*/
/***************************************************************************************************/
/* Boton para Cerrar Vetanas                                                                       */
/***************************************************************************************************/
void MainWindow::cerrarVentana()
{
    close();
}

/***************************************************************************************************/
/* Funcion para la Configuracion del GPIO                                                          */
/***************************************************************************************************/
void configuracionGPIO()
{
    printf("Configurando placa");
    system("echo 60 > /sys/class/gpio/export");
    system("echo in > /sys/class/gpio/gpio60/direction");

    system("echo 30 > /sys/class/gpio/export");
    system("echo in > /sys/class/gpio/gpio30/direction");

    system("echo 31 > /sys/class/gpio/export");
    system("echo in > /sys/class/gpio/gpio31/direction");

    system("echo 48 > /sys/class/gpio/export");
    system("echo in > /sys/class/gpio/gpio48/direction");
}

/***************************************************************************************************/
/* Timer Principal del Programa                                                                    */
/***************************************************************************************************/
void MainWindow::onGPIO()
{

    if(habilitacionJuegos == 2)     // Si los juegos estan deshabilitados sale de la funcion.
        return;

    if(ocupadoTimer)
        return;


    ocupadoTimer = 1;
/*
// Solo con la PC
    if ( memcmp(path,"armv7l",strlen("armv7l"))  )
    {
//        jueRul = new juegosRuleta;
//        jueRul->showFullScreen();
    }
// Solo con la PC

// Solo con la Placa
    if ( !memcmp(path,"armv7l",strlen("armv7l"))  )
    {
*/
//      system("echo 1 > /sys/class/gpio/gpio60/value");   // Activar GPIO

        char caracteres[3];
        char command60[100]="/sys/class/gpio/gpio60/value";
        char command30[100]="/sys/class/gpio/gpio30/value";
        char command31[100]="/sys/class/gpio/gpio31/value";
        char command48[100]="/sys/class/gpio/gpio48/value";
        FILE *in;

        in = fopen(command60,"r");
        fgets(caracteres,2,in);
        fclose(in);

        if(caracteres[0]=='0')                                          // GPIO60 Activado (PIN12)
        {
//            ui->NombreAgencia->setText("Blanco Derecho");
            qDebug("Blanco Izquierdo");
            suen = new Suenios;
            suen->exec();
            delete suen;

        }
        else
        {
            in = fopen(command30,"r");
            fgets(caracteres,2,in);
            fclose(in);
            if(caracteres[0]=='0')                                      // GPIO30 Activado (PIN11)
            {
//                ui->NombreAgencia->setText("Blanco Izquierdo");
                qDebug("Blanco Derecho");
                jueRul = new juegosRuleta;
                jueRul->exec();
                delete jueRul;

            }
            else
            {
                in = fopen(command31,"r");
                fgets(caracteres,2,in);
                fclose(in);
                if(caracteres[0]=='0')                                  // GPIO31 Activado (PIN13)
                {
//                    ui->NombreAgencia->setText("Rojo");
                    qDebug("Rojo");
                    claseJackpot = new jackpot;
                    claseJackpot->exec();
                    delete claseJackpot;
                }
                else
                {
                    in = fopen(command48,"r");
                    fgets(caracteres,2,in);
                    fclose(in);
                    if(caracteres[0]=='0')                              // GPIO48 Activado (PIN15)
                    {
//                        ui->NombreAgencia->setText("Negro");
                        aladin = new popaladin;
                        aladin->exec();
                        delete aladin;
                        qDebug("Negro");
                    }
                    else
                    {
//                    ui->NombreAgencia->setText("Vacio");
                    }
                }
            }
        }
//    }

    ocupadoTimer = 0;
}

/***************************************************************************************************/
/* Funcion para Controlar el Teclado USB                                                           */
/***************************************************************************************************/
void MainWindow::keyPressEvent(QKeyEvent* event)
{
    if(!ocupadoTimer)
    {
        ocupadoTimer = 1;

        if(event->key() == Qt::Key_0)
        {
            conf = new Configuracion;
            conf->exec();
            delete conf;

            if( cambioConfiguracion ("provincia"))
            {
                qDebug() << "Modificar Provincia\n";
                modificarProvincia();
            }

            if( cambioConfiguracion ("presentacion"))
            {
                qDebug() << "Modificar Monitor";
                modificarPresentacion();
            }

            if( cambioConfiguracion ("suerte"))
            {
                qDebug()  << "Modificar Numero de la Suerte";
                ui->numeroDia->setText("NUMERO DEL DIA <br>" + QString::number(getConfiguracion("suerte")));
            }

            if( cambioConfiguracion ("juegos"))
            {
                qDebug() << "Habilitacion juegos";
                habilitacionJuegos =  getConfiguracion("juegos");
            }

            if( cambioConfiguracion ("version"))
            {
                alert = new alerta;
                alert->exec();
                delete alert;
            }

            timerPrincipal = 0;  // Reinicio el Timer Principal

            clearConfiguracion(); // Limpiar el flag de cambio de la data base

        }
        if(event->key() == Qt::Key_1)
        {
            suen = new Suenios;
            suen->exec();
            delete suen;
        }
        if(event->key() == Qt::Key_2)
        {

            aladin = new popaladin;
            aladin->exec();
            delete aladin;
        }
        if(event->key() == Qt::Key_3)
        {
            jueRul = new juegosRuleta;
            jueRul->exec();
            delete jueRul;
        }
        if(event->key() == Qt::Key_5)
        {
            pendrive= new USBImages;
            pendrive->exec();
            delete pendrive;
        }
        if(event->key() == Qt::Key_6)
        {
            publicidad = new MostrarPublicidad;
            publicidad->exec();
            delete publicidad;
        }

        if(event->key() == Qt::Key_7)
        {
            claseJackpot = new jackpot;
            claseJackpot->exec();
            delete claseJackpot;
        }

        if(event->key() == Qt::Key_8)
        {
            fec = new Fecha;
            fec->exec();
            delete fec;
        }

//        if(event->key() == Qt::Key_9)
//        {
//            cerrarVentana();
//        }

        ocupadoTimer = 0;

//          QMessageBox* box = new QMessageBox();
//          box->setWindowTitle(QString("Hello"));
//          box->setText(QString("You Pressed: ")+ e->text());
//          box->show();
    }

}
