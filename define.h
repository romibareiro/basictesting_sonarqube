#ifndef DEFINE_H
#define DEFINE_H


#define IP_SERVER "192.168.137.39"
#define PORT_SERVER 5001
#define VERSION_HARDWARE "BeagleBone Black rev B"
#define VERSION_SISTEMA_OPERATIVO "Angstrom"
#define VERSION_SOFTWARE "105"


#define ANCHO_PANTALLA 1280
#define ALTO_PANTALLA 720


#define NACIONAL            1
#define BUENOS_AIRES        2
#define SANTA_FE            3
#define CORDOBA             4

#define SORTEO_PRIMERA      1
#define SORTEO_MATUTINA     2
#define SORTEO_VESPERTINA   3
#define SORTEO_NOCTURNA     4

// Sistema de Visualizacion de Quinielas

#define FORMA_CABEZAS 1
#define FORMA_COMPLETA 2
#define FORMA_MIXTA 3

// Sistema de Operacion (Si es Automatico o Manual)

#define OPERACION_MANUAL 1
#define OPERACION_AUTOMATICO 2


// Habilitar y Deshabilitar Publicidad

#define PUBLICIDAD_ON  1
#define PUBLICIDAD_OFF 2

// Habilitar y Deshabilitar Juegos

#define JUEGO_ON  1
#define JUEGO_OFF 2

// Habilitar y Deshabilitar Sonido

#define SONIDO_ON  1
#define SONIDO_OFF 2

#endif // DEFINE_H
