#ifndef PRESENTACION_H
#define PRESENTACION_H

#include <QDialog>
#include <QtGui>

#include <database.h>
#include <parser.h>
#include <myTCPClient.h>

class presentacionThread : public QThread
{
    Q_OBJECT

public:
//    explicit presentacionThread(QWidget *parent = 0);
    explicit presentacionThread();
    void run();
    bool stop;

    MyTCPClient * myCliente;

signals:
    void barraPaso( int , QString );
    void signalMainWindow( int );

public slots:

};

namespace Ui {
class presentacion;
}

class presentacion : public QDialog
{
    Q_OBJECT
    
public:
    explicit presentacion(QWidget *parent = 0);
    ~presentacion();

//    presentacionThread * t;

    int estado;
    
private:
    Ui::presentacion *ui;

public slots:

    void onBarraPaso( int , QString );

};



#endif // PRESENTACION_H
