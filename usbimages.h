#ifndef USBIMAGES_H
#define USBIMAGES_H

#include <QDialog>
#include <unistd.h>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include "usbthread.h"
#include <QtCore>

#include <QMessageBox>
#include <iostream>
#include "database.h"

#define RESOLUCION_1920x1080 0
#define RESOLUCION_1280x720  1
#define RESOLUCION_1024x768  2
#define RESOLUCION_768x576   3
#define RESOLUCION_720x480   4

#define RUTA_ARCHIVOS_PENDRIVE "/media/pendrive/TIMBEAR"
#define RUTA_ARCHIVOS_MODIFICADOR "~/"
#define RUTA_ARCHIVOS_PUBLICIDAD_MOVIL "extras/publicidadVariable"
#define JPG "jpg"
#define JPEG "jpeg"
#define PNG "png"

namespace Ui
{
class USBImages;
class usedThread;
}
class NombresPublicidad
{
 public:
    char nombrePublicidad[100];
};


class USBImages : public QDialog
{
    Q_OBJECT
    
public:
    explicit USBImages(QWidget *parent = 0);
    ~USBImages();
    void timerEvent(QTimerEvent *event);

    class parametrosPublicidad
    {
    public:
     class NombresPublicidad  ClasePubli[10];
     int NumeroArchivos;
    }classPublicidad;

    USBThread *pendrive;
    QMutex mutex;
    QBasicTimer timerEspera;

    typedef struct _PANTALLA
    {
        int Ancho;
        int Alto;
    }PANTALLA;

    PANTALLA Pantalla;

    typedef struct _ENCABEZADO
    {
        int X1;
        int Y1;
        int X2;
        int Y2;
        int X3;
        int Y3;
        int Fuente;
    }ENCABEZADO;

    ENCABEZADO Encabezado;

    typedef struct _BORDEENCABEZADO
    {
        int X1;
        int X2;
        int Y1;
        int Y2;
        int Separacion;
    }BORDEENCABEZADO;

    BORDEENCABEZADO Borde_Encabezado;

    typedef struct _TITULOS
    {
        int X1;
        int Y1;
        int X2;
        int Y2;
        int Alto;
        int Ancho;
        int Fuente;
    }TITULOS;

    TITULOS Titulos;

    typedef struct _PUBLICIDADES
    {
        int X1;
        int Y1;
        int X2;
        int Y2;
        int Alto;
        int Ancho;
    }PUBLICIDADES;

    PUBLICIDADES Publicidades;

private:
     Ui::USBImages *ui;
          int chargeReady;
     void createPipe();
public slots:
     void cargarDesdeElUSB(char* extension);

     void closeDialog();
    void ConfiguracionInicial( void );
    void Resolucion          ( void );
    void AjustarParametros   ( void );
    void ImagenInicial       ( void );
    void TextoEncabezado     ( void );
    void BordeEncabezado     ( void );
    void TextoTitulos        ( void );

private slots:
};



#endif // USBIMAGES_H
/*


class Publicidad : public QDialog
{
    Q_OBJECT

public:
    explicit Publicidad(QWidget *parent = 0);

    //class abrirbd Base;
    ~Publicidad();

    void ImprimirResultadosPropaganda ( int , int , int );

    class parametrosPublicidad
    {
    public:
     class NombresPublicidad  ClasePubli[10];
     int NumeroArchivos;
    }classPublicidad;





    void cargarDesdeElUSB(char* extension);


private slots:
    void on_pushButton_clicked();

private:
    Ui::Publicidad *ui;
};


*/
