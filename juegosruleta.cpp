#include "juegosruleta.h"
#include "ui_juegosruleta.h"

/**
 \fn juegosRuleta::juegosRuleta(QWidget *parent) : QDialog(parent), ui(new Ui::juegosRuleta)
 \brief Constructor de la clase para el Juego de la Ruleta
 \note Constructor de la clase para el Juego de la Ruleta
*/

juegosRuleta::juegosRuleta(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::juegosRuleta)
{
    ui->setupUi(this);

    HAVE_NUMBER = 0;
    audioTime = 0;

    this->setGeometry( 0 , 0 , 1280 , 720 );

    ui->label_back->setScaledContents(true);
    ui->label_back->setGeometry( 0 , 0 , 1280 , 720 );
    ui->label_back->setPixmap(QPixmap("extras/fondoCasino01.png"));

    timer = new QTimer(this);
    timerBolita = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(playMe()));
    connect(timerBolita, SIGNAL(timeout()), this, SLOT(giroRuleta()));

    state = SONIDO_RULETA;

    t = new ruletaThread;

    startRuleta();
}

/**
 \fn juegosRuleta::~juegosRuleta()
 \brief Destructor de la clase para el Juego de la Ruleta
 \note Destructor de la clase para el Juego de la Ruleta
*/

juegosRuleta::~juegosRuleta()
{
    delete ui;
    delete timer;
    delete timerBolita;
    delete t;
}

/**
 \fn void juegosRuleta::startRuleta()
 \brief
 \note
*/

void juegosRuleta::startRuleta()
{
   ui->label_numero->setHidden(true);


//   pidHijo=fork();

   t->start();
   timerBolita->start(25);
   timer->start(1000);

}

/**
 \fn void juegosRuleta::writeIntoLabelNumber(int number,char *archivo_audio)
 \brief Asocia el numero con el archivo de audio correspondiente
 \note
*/

void juegosRuleta::writeIntoLabelNumber(int number,char *archivo_audio)
{

  ui->label_numero->setHidden(false);
  char buffer[20];
  sprintf(buffer,"%d",number);

  strcat(archivo_audio,buffer);
  strcat(archivo_audio,".wav");
}
/**
 \fn void juegosRuleta::isBlackRedOrZero(int number,QPalette *colours, char*archivo_audio)
 \brief Logica del color asociado al nro aleatorio de la ruleta
 \note
*/

void juegosRuleta::isBlackRedOrZero(int number,QPalette *colours, char*archivo_audio)
{
    char buffer[5];
    int aux1=0;
    memset(buffer,0,sizeof(buffer));

    colours->setColor(QPalette::Window, Qt::red);
    colours->setColor(QPalette::WindowText, Qt::white);
    sprintf(archivo_audio,"%s","./extras/numeros/colorado.wav");
    sprintf(buffer,"%d",number);
    aux1=(buffer[0]-'0')+(buffer[1]-'0');


    if (number==0)
    {
         colours->setColor(QPalette::Window, Qt::green);
         colours->setColor(QPalette::WindowText, Qt::black);
         sprintf(archivo_audio,"%s","extras/numeros/0.wav");
    }
    else if(number==10)
       colours->setColor(QPalette::Window, Qt::black);

    else
    {
        if(aux1<=10)
        {
            aux1=aux1%2;
            if(aux1==0)             // Es negro
            {
                colours->setColor(QPalette::Window, Qt::black);
                sprintf(archivo_audio,"%s","./extras/numeros/negro.wav");
            }
        }
        else
        {
            sprintf(buffer,"%d",aux1);
            aux1=(buffer[0]-'0')+(buffer[1]-'0');
            aux1=aux1%2;
            if(aux1==0)         // Es negro
            {
                colours->setColor(QPalette::Window, Qt::black);
                sprintf(archivo_audio,"%s","./extras/numeros/negro.wav");
            }
        }
    }
}

/**
 \fn void juegosRuleta::playMe()
 \brief Maquina de Estados principal del Juego de la Ruleta
 \note
*/

void juegosRuleta::playMe()
{
    static QPalette sample_palette;
    static int number;

    char archivo_audio[100];

    switch(state)
    {
        case SONIDO_RULETA:
            if(audioTime > 5)
            {
                // AudioTime++;
                HAVE_NUMBER = 0;

                if (!HAVE_NUMBER && audioTime > 2)
                {
                    // Create seed for the random
                    // That is needed only once on application startup
                    timerBolita->stop();
                    QTime time = QTime::currentTime();
                    qsrand((uint)time.msec());

                    // Get random value between 0-100
                    number=qrand() % ((36 + 1) - 1) + 1;
                    HAVE_NUMBER=1;
                    ui->label_numero->setHidden(false);
                    ui->label_numero->setAutoFillBackground(true);

                    memset(archivo_audio,0,sizeof(archivo_audio));
                    isBlackRedOrZero(number,&sample_palette,archivo_audio);
                    char consola[100];

                    sprintf(consola, "mplayer %s -endpos 8",archivo_audio);
                    system(consola);
//                    kill(-9,pidHijo);

                    ui->label_numero->setPalette(sample_palette);
                    char buffer[10];
                    sprintf(buffer,"%d",number);
                    ui->label_numero->setText(buffer);

                }
                state = SONIDO_COLOR;
            }
            else
            {
               state = SONIDO_RULETA;
               audioTime++;
            }

            break;
        case SONIDO_COLOR:
            if(audioTime >= 6)
            {
                sprintf( archivo_audio,"%s","./extras/numeros/");
                writeIntoLabelNumber(number,archivo_audio );// carga ademas el archivo de audio
                char consola[100];
//                pidHijo=fork();
                sprintf(consola, "mplayer %s -endpos 6 ",archivo_audio);
                system(consola);
//                kill(-9,pidHijo);
                audioTime = 0;
                state = SONIDO_NUMERO;
            }
                else audioTime++;
            break;
        case SONIDO_NUMERO:
            if(audioTime >= 10)
            {
                //  state=SONIDO_RULETA;
                closeDialog();
            }
            else audioTime++;
            break;
        default:
            break;
    }
}

/**
 \fn void juegosRuleta::giroRuleta()
 \brief Controla el giro de las imagenes de la ruleta
 \note
*/

void juegosRuleta::giroRuleta()
{
    static QImage canvas = QImage("extras/ruleta.png");
    static QImage canvas4 = QImage(canvas.width(),canvas.height(),QImage::Format_ARGB32);
    static QImage canvas2 = QImage("extras/ruleta.png");
    static QImage canvas3 = QImage("extras/bolita.png");

//Draw image with offset to center of image.
    static QPainter painter( &canvas2 );
    static QPainter painter2( &canvas4 );

    static double angle=0;
    static double angle_ball=360;

    static qint8 internal_count=0;
    static    qreal ancho = (qreal) canvas.width()/2;
    static    qreal alto = (qreal) canvas.height()/2;

    canvas4.fill(Qt::transparent);


//Move windows's coordinate system to center.
    QTransform transform_centerOfWindow( 1, 0, 0, 1, ancho, alto );

            //Move windows's coordinate system to center.
    QTransform transform_centerOfWindow2( 1, 0, 0, 1, ancho, alto );

            //Rotate coordinate system.
    transform_centerOfWindow.rotate( angle );

            //Transform coordinate system.
    painter.setTransform( transform_centerOfWindow );

            //Load image.

            //Notice: As coordinate system is set at center of window, we have to center the image also... so the minus offset to come to center of image.
    painter.drawImage(-ancho, -alto, canvas);

    ui->label_ruleta->setPixmap(QPixmap::fromImage(canvas2));

    angle=angle+(double)(180/37);

    if (angle>360)
        angle=angle-360;

             //Rotate coordinate system.
    transform_centerOfWindow2.rotate( angle_ball );

            //Transform coordinate system.
    painter2.setTransform( transform_centerOfWindow2 );

            //Load image.

            //Notice: As coordinate system is set at center of window, we have to center the image also... so the minus offset to come to center of image.
    painter2.drawImage(-ancho, -alto, canvas3);

    ui->label_bolita->setPixmap(QPixmap::fromImage(canvas4));

    angle_ball=angle_ball-(double)(180/37);
    if(angle_ball<0)
        angle_ball=angle_ball+360;

        internal_count++;

        if (internal_count >= 2)
        {
            internal_count=0;
            FLAG_INTEGER=1;
        }

        else
            FLAG_INTEGER=0;
}

/**
 \fn void juegosRuleta::closeDialog()
 \brief Cierra el Juego de la Ruleta
 \note
*/

void juegosRuleta::closeDialog()
{
//    pid_t padre = getpid();
//    kill(padre,-9);
    this->close();
}

/**
 \fn ruletaThread::ruletaThread() : QThread()
 \brief Constructor del Thread para el sonido del juego de la ruleta
 \note
*/

ruletaThread::ruletaThread() :
    QThread()
{
    qDebug() << "ruletaThread::ruletaThread() - Comienzo Constructor";
}

/**
 \fn void ruletaThread::run()
 \brief Thread para el sonido del juego de la ruleta
 \note
*/

void ruletaThread::run()
{
    qDebug() << "ruletaThread:run() - Comienzo";
    system("mplayer extras/sonido_ruleta.wav -endpos 5");
    qDebug() << "ruletaThread:run() - Termina";
}
