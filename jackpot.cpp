#include "jackpot.h"
#include "ui_jackpot.h"

#define X_NUMERO1 420
#define X_NUMERO2 554
#define X_NUMERO3 688
#define Y_NUMERO -265
#define ANCHONUMERO 110
#define ALTONUMERO 600

#define X_POMO 923
#define Y_POMO 138
#define ANCHO_POMO 100
#define ALTO_POMO 100

#define X_BARRA 953
#define Y_BARRA 238
#define ANCHO_BARRA 40
#define ALTO_BARRA 250


/**
 *\fn       jackpot::jackpot(QWidget *parent) : QDialog(parent), ui(new Ui::jackpot)
 *\brief    Constructor de la clase jackpot.
 *\note     Constructor de la clase jackpot.
*/

jackpot::jackpot(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::jackpot)
{
    qDebug() << "jackpot::jackpot() - Comienzo Constructor";
    ui->setupUi(this);

    y_numeroBajo1 = Y_NUMERO;
    y_numeroBajo2 = Y_NUMERO;
    y_numeroBajo3 = Y_NUMERO;
    y_numeroAlto1 = y_numeroBajo1 + 600;
    y_numeroAlto2 = y_numeroBajo2 + 600;
    y_numeroAlto3 = y_numeroBajo3 + 600;

    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());

    randomNumero1 = ( rand() % 86 ) + 27 ;
    randomNumero2 = ( rand() % 80 ) + 30 ;
    randomNumero3 = ( rand() % 90 ) + 25 ;

    this->setGeometry( 0 , 0 , 1280 , 720 );

    ui->maquina->setScaledContents(true);
    ui->maquina->setGeometry( 0 , 0 , 1280 , 720 );
    ui->maquina->setPixmap(QPixmap("extras/maquinaJackpot.png"));

    ui->fondo->setScaledContents(true);
    ui->fondo->setGeometry( 0 , 0 , 1280 , 720 );
    ui->fondo->setPixmap(QPixmap("extras/fondoCasino01.png"));

    ui->numeroBajo1->setScaledContents(true);
    ui->numeroBajo1->setGeometry( X_NUMERO1 , y_numeroBajo1 , ANCHONUMERO , ALTONUMERO );
    ui->numeroBajo1->setPixmap(QPixmap("extras/numerosBajos.png"));

    ui->numeroAlto1->setScaledContents(true);
    ui->numeroAlto1->setGeometry( X_NUMERO1 , y_numeroAlto1 , ANCHONUMERO , ALTONUMERO );
    ui->numeroAlto1->setPixmap(QPixmap("extras/numerosAltos.png"));

    ui->numeroBajo2->setScaledContents(true);
    ui->numeroBajo2->setGeometry( X_NUMERO2 , y_numeroBajo2 , ANCHONUMERO , ALTONUMERO );
    ui->numeroBajo2->setPixmap(QPixmap("extras/numerosBajos.png"));

    ui->numeroAlto2->setScaledContents(true);
    ui->numeroAlto2->setGeometry( X_NUMERO2 , y_numeroAlto2 , ANCHONUMERO , ALTONUMERO );
    ui->numeroAlto2->setPixmap(QPixmap("extras/numerosAltos.png"));

    ui->numeroBajo3->setScaledContents(true);
    ui->numeroBajo3->setGeometry( X_NUMERO3 , y_numeroBajo3 , ANCHONUMERO , ALTONUMERO );
    ui->numeroBajo3->setPixmap(QPixmap("extras/numerosBajos.png"));

    ui->numeroAlto3->setScaledContents(true);
    ui->numeroAlto3->setGeometry( X_NUMERO3 , y_numeroAlto3 , ANCHONUMERO , ALTONUMERO );
    ui->numeroAlto3->setPixmap(QPixmap("extras/numerosAltos.png"));

    ui->pomo->setScaledContents(true);
    ui->pomo->setGeometry( X_POMO , Y_POMO , ANCHO_POMO , ALTO_POMO );
    ui->pomo->setPixmap(QPixmap("extras/pomo.png"));

    ui->barra->setScaledContents(true);
    ui->barra->setGeometry( X_BARRA , Y_BARRA , ANCHO_BARRA , ALTO_BARRA );
    ui->barra->setPixmap(QPixmap("extras/barra.png"));

    t = new jackpotThread();
    estadoJackpot = 0;

//    QObject::connect( t , SIGNAL( barraPaso( int , QString )) , pres , SLOT( onBarraPaso( int , QString )) );

    t->start();

    contadorTimer = 0;
    y_pomo = 0;
    flagBajo1 = 1;
    flagBajo2 = 1;
    flagBajo3 = 1;
    timer.start(100,this);

    qDebug() << "jackpot::jackpot() - Termina Constructor";

}

/**
 *\fn       jackpot::~jackpot()
 *\brief    Destructor de la clase jackpot.
 *\note     Destructor de la clase jackpot.
*/

jackpot::~jackpot()
{
    qDebug() << "jackpot::~jackpot() - Destructor";
    delete ui;
    delete t;
}

/**
 *\fn       void jackpot::timerEvent(QTimerEvent *event)
 *\brief    Timer principal de la clase Jackpot.
 *\note     Timer principal de la clase Jackpot.
 *\param    [in] QTimerEvent *event -> Puntero al Evento del Timer.
*/

void jackpot::timerEvent(QTimerEvent *event)
{
    event = event;
//    qDebug() << "jackpot::timerEvent() - contadorTimer=" << contadorTimer;
    contadorTimer ++;
    int aux;

    switch (estadoJackpot) {
    case 0:                                 // Espera
        if( contadorTimer > 10)
        {
            estadoJackpot = 1;
        }

        break;
    case 1:                                 // Movimiento de Palanca
        y_pomo += 26;

 //       ui->pomo->setGeometry( X_POMO, Y_POMO + 250 , ANCHO_POMO , ALTO_POMO );
 //       ui->barra->setGeometry( X_BARRA , Y_BARRA + 200 , ANCHO_BARRA , ALTO_BARRA / 10);

        ui->pomo->setGeometry( X_POMO, Y_POMO + y_pomo, ANCHO_POMO , ALTO_POMO );
        ui->barra->setGeometry( X_BARRA , Y_BARRA + y_pomo, ANCHO_BARRA , ALTO_BARRA - y_pomo);

        if( contadorTimer > 19)
        {
            estadoJackpot = 2;
        }
        break;
    case 2:                                 // Movimiento de Numeros

        if( flagBajo1 )
        {
            y_numeroBajo1 += randomNumero1;
            y_numeroAlto1 += randomNumero1;

            if( contadorTimer > 80 )
            {
                aux = (y_numeroBajo1 - Y_NUMERO) % 100;

                y_numeroBajo1 -= aux;
                y_numeroAlto1 -= aux;

                if ( y_numeroBajo1 == Y_NUMERO || y_numeroAlto1 == Y_NUMERO)
                {
                    y_numeroBajo1 += 100;
                    y_numeroAlto1 += 100;
                }

                flagBajo1 = 0;
            }

            ui->numeroBajo1->setGeometry( X_NUMERO1 , y_numeroBajo1 , ANCHONUMERO , ALTONUMERO );
            ui->numeroAlto1->setGeometry( X_NUMERO1 , y_numeroAlto1 , ANCHONUMERO , ALTONUMERO );

        }

        if( flagBajo2 )
        {
            y_numeroBajo2 += randomNumero2;
            y_numeroAlto2 += randomNumero2;

            if( contadorTimer > 85 )
            {
                aux = (y_numeroBajo2 - Y_NUMERO) % 100;

                y_numeroBajo2 -= aux;
                y_numeroAlto2 -= aux;

                if ( y_numeroBajo2 == Y_NUMERO || y_numeroAlto2 == Y_NUMERO)
                {
                    y_numeroBajo2 += 100;
                    y_numeroAlto2 += 100;
                }

                flagBajo2 = 0;
            }

            ui->numeroBajo2->setGeometry( X_NUMERO2 , y_numeroBajo2 , ANCHONUMERO , ALTONUMERO );
            ui->numeroAlto2->setGeometry( X_NUMERO2 , y_numeroAlto2 , ANCHONUMERO , ALTONUMERO );

        }
        if( flagBajo3 )
        {
            y_numeroBajo3 += randomNumero3;
            y_numeroAlto3 += randomNumero3;

            if( contadorTimer > 90 )
            {
                aux = (y_numeroBajo3 - Y_NUMERO) % 100;

                y_numeroBajo3 -= aux;
                y_numeroAlto3 -= aux;

                if ( y_numeroBajo3 == Y_NUMERO || y_numeroAlto3 == Y_NUMERO)
                {
                    y_numeroBajo3 += 100;
                    y_numeroAlto3 += 100;
                }

                flagBajo3 = 0;
            }

            ui->numeroBajo3->setGeometry( X_NUMERO3 , y_numeroBajo3 , ANCHONUMERO , ALTONUMERO );
            ui->numeroAlto3->setGeometry( X_NUMERO3 , y_numeroAlto3 , ANCHONUMERO , ALTONUMERO );

        }

        if(y_numeroBajo1 > 600)
            y_numeroBajo1 -= 1200;
        if(y_numeroBajo2 > 600)
            y_numeroBajo2 -= 1200;
        if(y_numeroBajo3 > 600)
            y_numeroBajo3 -= 1200;

        if(y_numeroAlto1 > 600)
            y_numeroAlto1 -= 1200;
        if(y_numeroAlto2 > 600)
            y_numeroAlto2 -= 1200;
        if(y_numeroAlto3 > 600)
            y_numeroAlto3 -= 1200;

        if( contadorTimer > 100)
        {
            estadoJackpot = 3;
        }
        break;
    case 3:                                 // Muestra los Numeros por un Tiempo
        if( contadorTimer > 200)
        {
           timer.stop();
           close();
        }
        break;
    default:
        break;
    }


}

/**
 *\fn       jackpotThread::jackpotThread() : QThread()
 *\brief    Contructor del Thread para el sonido del Jackpot.
 *\note     Contructor del Thread para el sonido del Jackpot.
*/

jackpotThread::jackpotThread() :
    QThread()
{
    qDebug() << "jackpotThread::jackpotThread() - Comienzo Constructor";
}

/**
 *\fn       void jackpotThread::run()
 *\brief    Thread para el sonido del Jackpot.
 *\note     Thread para el sonido del Jackpot. Reproduce el Sonido del Juego.
*/

void jackpotThread::run()
{
    qDebug() << "jackpotThread:run() - Comienzo";
    system("mplayer extras/jackpot.mp3");
    qDebug() << "jackpotThread:run() - Termina";
}

