#include <mainwindow.h>
#include "ui_mainwindow.h"

#define TIMER_GPIO  1
#define TIMER_REINICIAR 500
#define TIMER_CAMBIO 20
#define TIEMPO_DESCONEXION 50

extern bool estadoConexion;
extern int serialNumber;

/***************************************************************************************************/
/* Funcion que Administra el Tiempo en el Programa Principal (500ms)                               */
/***************************************************************************************************/
/**
 \fn void MainWindow::timerEvent(QTimerEvent *event)
 \brief Administra el Tiempo
 \note Funcion que Administra el Tiempo en el Programa Principal (500ms)
 \param [in] QTimerEvent *event -> Evento del Timer
*/
void MainWindow::timerEvent(QTimerEvent *event)
{
    event = event;
    if(ocupadoTimer)
        return;

    ocupadoTimer = 1;

// Actualizaion de la Hora

    QDateTime today;
    QString fecha;
    QString textoEnviar;
    today = today.currentDateTime();
    fecha.sprintf("%02d:%02d:%02d",today.time().hour() ,today.time().minute(),today.time().second());
    ui->fechaHoy->setText(fecha);

    HoraSorteo ( today.time().hour() , today.time().minute() , today.time().second() );


    if(timerPrincipal%2)
        ui->numeroDia->setFrameShadow(QFrame::Raised);
    else
        ui->numeroDia->setFrameShadow(QFrame::Sunken);

    if( !( timerPrincipal % ( TIMER_CAMBIO * 2 ) ) )
    {
        qDebug() << timerPrincipal ;
        QString fecha;
        QString nombreSorteo;
        databaseQuiniela dbQuiniela;
        int operacion;

        fecha = "13/01/2015";

        operacion = getConfiguracion("operacion");

        switch (seleccionQuiniela) {
        case NACIONAL:
            nombreSorteo = "Nacional";
            break;
        case BUENOS_AIRES:
            nombreSorteo = "Buenos_Aires";
            break;
        case SANTA_FE:
            nombreSorteo = "Santa_Fe";
            break;
        case CORDOBA:
            nombreSorteo = "Cordoba";
            break;
        default:
            timerPrincipal = TIMER_REINICIAR;

            break;
        }


        if( timerPrincipal >= TIMER_REINICIAR)
        {

            seleccionQuiniela = NACIONAL;
            seleccionSorteo = SORTEO_PRIMERA;
            timerPrincipal = 0;

            switch (esquemaPOP) {
            case 0:
                qDebug() << "Mostrar Publicidad";
                esquemaPOP++;
                publicidad = new MostrarPublicidad;
                publicidad->exec();
                delete publicidad;
                break;
            case 1:
                qDebug() << "Mostrar Sueños";
                esquemaPOP++;
                suen = new Suenios;
                suen->exec();
                delete suen;
                break;
            default:
                textoEnviar = "{\"id\":" + QString::number(serialNumber) + ",\"pedido\":2}";
                myCliente->writeMyData(textoEnviar);
                esquemaPOP = 0;
                break;
            }
        }
        else
        {
            switch (formaPresentacion) {
            case FORMA_CABEZAS:

                if( operacion == OPERACION_MANUAL )
                {
                    dbQuiniela.manual = 1;
                    dbQuiniela.seleccionarSorteoManual();
                    mostrarCabezas( &dbQuiniela , 1);

                    dbQuiniela.manual = 2;
                    dbQuiniela.seleccionarSorteoManual();
                    mostrarCabezas( &dbQuiniela , 2);

                    dbQuiniela.manual = 3;
                    dbQuiniela.seleccionarSorteoManual();
                    mostrarCabezas( &dbQuiniela , 3);

                    dbQuiniela.manual = 4;
                    dbQuiniela.seleccionarSorteoManual();
                    mostrarCabezas( &dbQuiniela , 4);

                }
                else
                {
                    dbQuiniela.nombreSorteo = nombreSorteo;
                    dbQuiniela.tipoSorteo = "Primera";
                    dbQuiniela.fechaSorteo = fecha;
                    if ( dbQuiniela.seleccionarSorteo())
                        printf("Error con la carga de la Columna 1");
                    mostrarCabezas( &dbQuiniela , 1);

                    dbQuiniela.nombreSorteo = nombreSorteo;
                    dbQuiniela.tipoSorteo = "Matutina";
                    dbQuiniela.fechaSorteo = fecha;
                    if ( dbQuiniela.seleccionarSorteo())
                        printf("Error con la carga de la Columna 1");
                    mostrarCabezas( &dbQuiniela , 2);

                    dbQuiniela.nombreSorteo = nombreSorteo;
                    dbQuiniela.tipoSorteo = "Vespertina";
                    dbQuiniela.fechaSorteo = fecha;
                    if ( dbQuiniela.seleccionarSorteo())
                        printf("Error con la carga de la Columna 1");
                    mostrarCabezas( &dbQuiniela , 3);

                    dbQuiniela.nombreSorteo = nombreSorteo;
                    dbQuiniela.tipoSorteo = "Nocturna";
                    dbQuiniela.fechaSorteo = fecha;
                    if ( dbQuiniela.seleccionarSorteo())
                        printf("Error con la carga de la Columna 1");
                    mostrarCabezas( &dbQuiniela , 4);

                    seleccionQuiniela ++;

                }

                break;

            case FORMA_COMPLETA:

                if( operacion == OPERACION_MANUAL )
                {
                    if( timerPrincipal == (TIMER_CAMBIO * 0) )
                        dbQuiniela.manual = 1;
                    else
                        dbQuiniela.manual = 3;

                    dbQuiniela.seleccionarSorteoManual();

                    // Mostrar Quiniela A y B

                    mostrarQuinielaA(&dbQuiniela);
                    mostrarQuinielaB(&dbQuiniela);

                    if( timerPrincipal == (TIMER_CAMBIO * 0) )
                        dbQuiniela.manual = 2;
                    else
                        dbQuiniela.manual = 4;

                    dbQuiniela.seleccionarSorteoManual();

                    // Mostrar Quiniela C y D

                    mostrarQuinielaC(&dbQuiniela);
                    mostrarQuinielaD(&dbQuiniela);

                }
                else
                {
                    switch (seleccionSorteo) {
                    case SORTEO_PRIMERA:
                        dbQuiniela.tipoSorteo = "Primera";
                        break;
                    case SORTEO_VESPERTINA:
                        dbQuiniela.tipoSorteo = "Vespertina";
                        break;
                    default:
                        qDebug() << "MainWindow::timerEvent - Error en Seleccion de Sorteo";
                        break;
                    }

                    dbQuiniela.nombreSorteo = nombreSorteo;
                    dbQuiniela.fechaSorteo = fecha;
                    if ( dbQuiniela.seleccionarSorteo())
                        printf("MainWindow::timerEvent - Error con la carga de la Columna 1");

                    // Mostrar Quiniela A y B

                    mostrarQuinielaA(&dbQuiniela);
                    mostrarQuinielaB(&dbQuiniela);

                    seleccionSorteo ++;

                    switch (seleccionSorteo) {
                    case SORTEO_MATUTINA:
                        dbQuiniela.tipoSorteo = "Matutina";
                        break;
                    case SORTEO_NOCTURNA:
                        dbQuiniela.tipoSorteo = "Nocturna";
                        break;
                    default:
                        qDebug() << "MainWindow::timerEvent - Error en Seleccion de Sorteo";
                        break;
                    }

                    dbQuiniela.nombreSorteo = nombreSorteo;
                    dbQuiniela.fechaSorteo = fecha;
                    if ( dbQuiniela.seleccionarSorteo())
                        printf("MainWindow::timerEvent - Error con la carga de la Columna 2");

                    // Mostrar Quiniela C y D

                    mostrarQuinielaC(&dbQuiniela);
                    mostrarQuinielaD(&dbQuiniela);

                    if(seleccionSorteo == SORTEO_NOCTURNA)
                    {
                        seleccionSorteo = SORTEO_PRIMERA;
                        seleccionQuiniela ++;
                    }
                    else
                    {
                        seleccionSorteo ++;
                    }

                }
                break;

            case FORMA_MIXTA:

                if( operacion == OPERACION_MANUAL )
                {

                    dbQuiniela.manual = 1;
                    dbQuiniela.seleccionarSorteoManual();
                    mostrarCabezas( &dbQuiniela , 1);

                    if( timerPrincipal == (TIMER_CAMBIO * 0) )
                    {
                        mostrarQuinielaA(&dbQuiniela);      // Mostrar Quiniela A y B
                        mostrarQuinielaB(&dbQuiniela);
                    }

                    dbQuiniela.manual = 2;
                    dbQuiniela.seleccionarSorteoManual();
                    mostrarCabezas( &dbQuiniela , 2);

                    if( timerPrincipal == (TIMER_CAMBIO * 2) )
                    {
                        mostrarQuinielaA(&dbQuiniela);      // Mostrar Quiniela A y B
                        mostrarQuinielaB(&dbQuiniela);
                    }

                    dbQuiniela.manual = 3;
                    dbQuiniela.seleccionarSorteoManual();
                    mostrarCabezas( &dbQuiniela , 3);

                    if( timerPrincipal == (TIMER_CAMBIO * 4) )
                    {
                        mostrarQuinielaA(&dbQuiniela);      // Mostrar Quiniela A y B
                        mostrarQuinielaB(&dbQuiniela);
                    }

                    dbQuiniela.manual = 4;
                    dbQuiniela.seleccionarSorteoManual();
                    mostrarCabezas( &dbQuiniela , 4);

                    if( timerPrincipal == (TIMER_CAMBIO * 6) )
                    {
                        mostrarQuinielaA(&dbQuiniela);      // Mostrar Quiniela A y B
                        mostrarQuinielaB(&dbQuiniela);
                    }

                }
                else
                {
                    dbQuiniela.nombreSorteo = nombreSorteo;
                    dbQuiniela.tipoSorteo = "Primera";
                    dbQuiniela.fechaSorteo = fecha;
                    if ( dbQuiniela.seleccionarSorteo())
                        printf("Error con la carga de la Columna 1");
                    mostrarCabezas( &dbQuiniela , 1);

                    if(seleccionSorteo == SORTEO_PRIMERA)
                    {
                        mostrarQuinielaA(&dbQuiniela);
                        mostrarQuinielaB(&dbQuiniela);
                    }

                    dbQuiniela.nombreSorteo = nombreSorteo;
                    dbQuiniela.tipoSorteo = "Matutina";
                    dbQuiniela.fechaSorteo = fecha;
                    if ( dbQuiniela.seleccionarSorteo())
                        printf("Error con la carga de la Columna 1");
                    mostrarCabezas( &dbQuiniela , 2);

                    if(seleccionSorteo == SORTEO_MATUTINA)
                    {
                        mostrarQuinielaA(&dbQuiniela);
                        mostrarQuinielaB(&dbQuiniela);
                    }

                    dbQuiniela.nombreSorteo = nombreSorteo;
                    dbQuiniela.tipoSorteo = "Vespertina";
                    dbQuiniela.fechaSorteo = fecha;
                    if ( dbQuiniela.seleccionarSorteo())
                        printf("Error con la carga de la Columna 1");
                    mostrarCabezas( &dbQuiniela , 3);

                    if(seleccionSorteo == SORTEO_VESPERTINA)
                    {
                        mostrarQuinielaA(&dbQuiniela);
                        mostrarQuinielaB(&dbQuiniela);
                    }

                    dbQuiniela.nombreSorteo = nombreSorteo;
                    dbQuiniela.tipoSorteo = "Nocturna";
                    dbQuiniela.fechaSorteo = fecha;
                    if ( dbQuiniela.seleccionarSorteo())
                        printf("Error con la carga de la Columna 1");
                    mostrarCabezas( &dbQuiniela , 4);

                    if(seleccionSorteo == SORTEO_NOCTURNA)
                    {
                        mostrarQuinielaA(&dbQuiniela);
                        mostrarQuinielaB(&dbQuiniela);

                        seleccionSorteo = SORTEO_PRIMERA;
                        seleccionQuiniela ++;

                        textoEnviar = "{\"id\":" + QString::number(serialNumber) + ",\"pedido\":1,\"nombre\":\"Nacional\",\"tipo\":\"Primera\",\"fecha\":\"20150219\"}";
                        myCliente->writeMyData(textoEnviar);
                    }
                    else
                    {
                        seleccionSorteo ++;
                    }

                }

                break;
            default:
                break;
            }
        }
    }

    if( ! ( (timerPrincipal + 10) % TIEMPO_DESCONEXION ))
    {

        if (estadoConexion)
        {
            ui->estadoRed->setPixmap(QPixmap("extras/redDesconectada.png") );
            delete myCliente;
            myCliente = new MyTCPClient;
        }
        else
        {
             ui->estadoRed->setPixmap(QPixmap("extras/redConectada.png") );
        }

        int versionActual;
        versionActual = getConfiguracion("version");
        textoEnviar = "{\"id\":" + QString::number(serialNumber) + ",\"pedido\":4,\"version\":" + QString::number(versionActual) + "}";
        myCliente->writeMyData(textoEnviar);

        estadoConexion = 1;

    }



    timerPrincipal ++;

    ocupadoTimer = 0;
}

/***************************************************************************************************/
/* Funcion para mostrar las quinielas en la Columna A                                              */
/***************************************************************************************************/
/**
 \fn void MainWindow::mostrarQuinielaA( databaseQuiniela * dbQuiniela )
 \brief Muestra la Quiniela en la Columna A
 \note Muestra la Quiniela en la Columna A
 \param [in] databaseQuiniela * dbQuiniela -> Puntero con los valores de la Database
*/
void MainWindow::mostrarQuinielaA( databaseQuiniela * dbQuiniela )
{
    ui->NombreSorteoA->setText(dbQuiniela->nombreSorteo);
    ui->TipoSorteoA->setText(dbQuiniela->tipoSorteo);

/*
    if(dbQuiniela->tipoSorteo == "pri")
        ui->TipoSorteoA->setText("Primera");
    if(dbQuiniela->tipoSorteo == "mat")
        ui->TipoSorteoA->setText("Matutino");
    if(dbQuiniela->tipoSorteo == "ves")
        ui->TipoSorteoA->setText("Vespertino");
    if(dbQuiniela->tipoSorteo == "noc")
        ui->TipoSorteoA->setText("Nocturno");


    switch (dbQuiniela->tipoSorteo) {
    case "pri":
        ui->TipoSorteoA->setText("Primera");
        break;
    case "mat":
        ui->TipoSorteoA->setText("Matutina");
        break;
    case "ves":
        ui->TipoSorteoA->setText("Vespertina");
        break;
    case "noc":
        ui->TipoSorteoA->setText("Nocturna");
        break;
    default:
        break;
    }
*/
    ui->NumeroSorteoA->setText("Nro: " + dbQuiniela->numeroSorteo);
    ui->FechaSorteoA->setText(dbQuiniela->fechaSorteo);

    ui->NumeroSorteoA01->display(dbQuiniela->premiosSorteo[0]);
    ui->NumeroSorteoA02->display(dbQuiniela->premiosSorteo[1]);
    ui->NumeroSorteoA03->display(dbQuiniela->premiosSorteo[2]);
    ui->NumeroSorteoA04->display(dbQuiniela->premiosSorteo[3]);
    ui->NumeroSorteoA05->display(dbQuiniela->premiosSorteo[4]);
    ui->NumeroSorteoA06->display(dbQuiniela->premiosSorteo[5]);
    ui->NumeroSorteoA07->display(dbQuiniela->premiosSorteo[6]);
    ui->NumeroSorteoA08->display(dbQuiniela->premiosSorteo[7]);
    ui->NumeroSorteoA09->display(dbQuiniela->premiosSorteo[8]);
    ui->NumeroSorteoA10->display(dbQuiniela->premiosSorteo[9]);

}

/***************************************************************************************************/
/* Funcion para mostrar las quinielas en la Columna B                                              */
/***************************************************************************************************/
/**
 \fn void MainWindow::mostrarQuinielaB( databaseQuiniela * dbQuiniela )
 \brief Muestra la Quiniela en la Columna B
 \note Muestra la Quiniela en la Columna B
 \param [in] databaseQuiniela * dbQuiniela -> Puntero con los valores de la Database
*/
void MainWindow::mostrarQuinielaB( databaseQuiniela * dbQuiniela )
{
    //ui->NombreSorteoB->setText(dbQuiniela->nombreSorteo);

    //ui->TipoSorteoB->setText(dbQuiniela->tipoSorteo);
/*
    switch (dbQuiniela->tipoSorteo) {
    case "pri":
        ui->TipoSorteoB->setText("Primera");
        break;
    case "mat":
        ui->TipoSorteoB->setText("Matutina");
        break;
    case "ves":
        ui->TipoSorteoB->setText("Vespertina");
        break;
    case "noc":
        ui->TipoSorteoB->setText("Nocturna");
        break;
    default:
        break;
    }
*/
    //ui->FechaSorteoB->setText(dbQuiniela->fechaSorteo);

    ui->NumeroSorteoB01->display(dbQuiniela->premiosSorteo[10]);
    ui->NumeroSorteoB02->display(dbQuiniela->premiosSorteo[11]);
    ui->NumeroSorteoB03->display(dbQuiniela->premiosSorteo[12]);
    ui->NumeroSorteoB04->display(dbQuiniela->premiosSorteo[13]);
    ui->NumeroSorteoB05->display(dbQuiniela->premiosSorteo[14]);
    ui->NumeroSorteoB06->display(dbQuiniela->premiosSorteo[15]);
    ui->NumeroSorteoB07->display(dbQuiniela->premiosSorteo[16]);
    ui->NumeroSorteoB08->display(dbQuiniela->premiosSorteo[17]);
    ui->NumeroSorteoB09->display(dbQuiniela->premiosSorteo[18]);
    ui->NumeroSorteoB10->display(dbQuiniela->premiosSorteo[19]);

}

/***************************************************************************************************/
/* Funcion para mostrar las quinielas en la Columna C                                              */
/***************************************************************************************************/
/**
 \fn void MainWindow::mostrarQuinielaC( databaseQuiniela * dbQuiniela )
 \brief Muestra la Quiniela en la Columna C
 \note Muestra la Quiniela en la Columna C
 \param [in] databaseQuiniela * dbQuiniela -> Puntero con los valores de la Database
*/
void MainWindow::mostrarQuinielaC( databaseQuiniela * dbQuiniela )
{
    ui->NombreSorteoC->setText(dbQuiniela->nombreSorteo);
    ui->TipoSorteoC->setText(dbQuiniela->tipoSorteo);
/*

    if(dbQuiniela->tipoSorteo == "pri")
        ui->TipoSorteoC->setText("Primera");
    if(dbQuiniela->tipoSorteo == "mat")
        ui->TipoSorteoC->setText("Matutino");
    if(dbQuiniela->tipoSorteo == "ves")
        ui->TipoSorteoC->setText("Vespertino");
    if(dbQuiniela->tipoSorteo == "noc")
        ui->TipoSorteoC->setText("Nocturno");

    switch (dbQuiniela->tipoSorteo) {
    case "pri":
        ui->TipoSorteoC->setText("Primera");
        break;
    case "mat":
        ui->TipoSorteoC->setText("Matutina");
        break;
    case "ves":
        ui->TipoSorteoC->setText("Vespertina");
        break;
    case "noc":
        ui->TipoSorteoC->setText("Nocturna");
        break;
    default:
        break;
    }
*/
    ui->NumeroSorteoC->setText("Nro: " + dbQuiniela->numeroSorteo);

    ui->FechaSorteoC->setText(dbQuiniela->fechaSorteo);

    ui->NumeroSorteoC01->display(dbQuiniela->premiosSorteo[0]);
    ui->NumeroSorteoC02->display(dbQuiniela->premiosSorteo[1]);
    ui->NumeroSorteoC03->display(dbQuiniela->premiosSorteo[2]);
    ui->NumeroSorteoC04->display(dbQuiniela->premiosSorteo[3]);
    ui->NumeroSorteoC05->display(dbQuiniela->premiosSorteo[4]);
    ui->NumeroSorteoC06->display(dbQuiniela->premiosSorteo[5]);
    ui->NumeroSorteoC07->display(dbQuiniela->premiosSorteo[6]);
    ui->NumeroSorteoC08->display(dbQuiniela->premiosSorteo[7]);
    ui->NumeroSorteoC09->display(dbQuiniela->premiosSorteo[8]);
    ui->NumeroSorteoC10->display(dbQuiniela->premiosSorteo[9]);
}

/***************************************************************************************************/
/* Funcion para mostrar las quinielas en la Columna D                                              */
/***************************************************************************************************/
/**
 \fn void MainWindow::mostrarQuinielaD( databaseQuiniela * dbQuiniela )
 \brief Muestra la Quiniela en la Columna D
 \note Muestra la Quiniela en la Columna D
 \param [in] databaseQuiniela * dbQuiniela -> Puntero con los valores de la Database
*/
void MainWindow::mostrarQuinielaD( databaseQuiniela * dbQuiniela )
{
    //ui->NombreSorteoD->setText(dbQuiniela->nombreSorteo);

    //ui->TipoSorteoD->setText(dbQuiniela->tipoSorteo);
/*
    switch (dbQuiniela->tipoSorteo) {
    case "pri":
        ui->TipoSorteoC->setText("Primera");
        break;
    case "mat":
        ui->TipoSorteoC->setText("Matutina");
        break;
    case "ves":
        ui->TipoSorteoC->setText("Vespertina");
        break;
    case "noc":
        ui->TipoSorteoC->setText("Nocturna");
        break;
    default:
        break;
    }
*/
    //ui->FechaSorteoD->setText(dbQuiniela->fechaSorteo);

    ui->NumeroSorteoD01->display(dbQuiniela->premiosSorteo[10]);
    ui->NumeroSorteoD02->display(dbQuiniela->premiosSorteo[11]);
    ui->NumeroSorteoD03->display(dbQuiniela->premiosSorteo[12]);
    ui->NumeroSorteoD04->display(dbQuiniela->premiosSorteo[13]);
    ui->NumeroSorteoD05->display(dbQuiniela->premiosSorteo[14]);
    ui->NumeroSorteoD06->display(dbQuiniela->premiosSorteo[15]);
    ui->NumeroSorteoD07->display(dbQuiniela->premiosSorteo[16]);
    ui->NumeroSorteoD08->display(dbQuiniela->premiosSorteo[17]);
    ui->NumeroSorteoD09->display(dbQuiniela->premiosSorteo[18]);
    ui->NumeroSorteoD10->display(dbQuiniela->premiosSorteo[19]);
}

/***************************************************************************************************/
/* Funcion para mostrar las Cabezas de las Quinielas                                               */
/***************************************************************************************************/
/**
 \fn void MainWindow::mostrarCabezas( databaseQuiniela * dbQuiniela , int posicion)
 \brief Muestra las Cabezas de las Quinielas
 \note Muestra las Cabezas de las Quinielas
 \param [in] databaseQuiniela * dbQuiniela -> Puntero con los valores de la Database
 \param [in] int posicion -> Posicion en la Filas
*/
void MainWindow::mostrarCabezas( databaseQuiniela * dbQuiniela , int posicion)
{
    /*
    QString tipoSorteo;
    if(dbQuiniela->tipoSorteo == "pri")
        tipoSorteo = "Primera";
    if(dbQuiniela->tipoSorteo == "mat")
        tipoSorteo = "Matutina";
    if(dbQuiniela->tipoSorteo == "ves")
        tipoSorteo = "Vespertina";
    if(dbQuiniela->tipoSorteo == "noc")
        tipoSorteo = "Nocturna";
*/


    switch (posicion) {
    case 1:
                ui->cabezaNombre1->setText(dbQuiniela->nombreSorteo);
                ui->cabezaTipo1->setText(dbQuiniela->tipoSorteo);
                ui->cabezaNumero1->setText("Nro: " + dbQuiniela->numeroSorteo);
                ui->cabezaFecha1->setText("Fecha: " + dbQuiniela->fechaSorteo);
                ui->cabezaPremio1->setText(dbQuiniela->premiosSorteo[0]);
        break;
    case 2:
                ui->cabezaNombre2->setText(dbQuiniela->nombreSorteo);
                ui->cabezaTipo2->setText(dbQuiniela->tipoSorteo);
                ui->cabezaNumero2->setText("Nro: " + dbQuiniela->numeroSorteo);
                ui->cabezaFecha2->setText("Fecha: " + dbQuiniela->fechaSorteo);
                ui->cabezaPremio2->setText(dbQuiniela->premiosSorteo[0]);
        break;
    case 3:
                ui->cabezaNombre3->setText(dbQuiniela->nombreSorteo);
                ui->cabezaTipo3->setText(dbQuiniela->tipoSorteo);
                ui->cabezaNumero3->setText("Nro: " + dbQuiniela->numeroSorteo);
                ui->cabezaFecha3->setText("Fecha: " + dbQuiniela->fechaSorteo);
                ui->cabezaPremio3->setText(dbQuiniela->premiosSorteo[0]);
        break;
    case 4:
                ui->cabezaNombre4->setText(dbQuiniela->nombreSorteo);
                ui->cabezaTipo4->setText(dbQuiniela->tipoSorteo);
                ui->cabezaNumero4->setText("Nro: " + dbQuiniela->numeroSorteo);
                ui->cabezaFecha4->setText("Fecha: " + dbQuiniela->fechaSorteo);
                ui->cabezaPremio4->setText(dbQuiniela->premiosSorteo[0]);
        break;
    default:
        break;
    }

}


void MainWindow::HoraSorteo(int Horas,int Minutos,int Segundos)
{
    QString ProximoSorteo;
    HORA_SORTEO Restan;

    Restan.Horas    = 0;
    Restan.Minutos  = 0;
    Restan.Segundos = 0;


    if ( (Horas < 11) || ((Horas == 11) && (Minutos <= 30)) )
    {
        Restan.Horas    = 11 - Horas - 1;
        Restan.Minutos  = 60 - Minutos - 1;
        Restan.Segundos = 60 - Segundos;

        if ( (Horas == 11) && (Minutos == 15) && (Segundos == 0) )
        {
            system("mplayer extras/cierreDeApuestas.mp3  -endpos 7");
        }
    }

    else if ( ((Horas == 11) && (Minutos >= 30)) ||  (Horas < 14) )
    {
        Restan.Horas    = 14 - Horas - 1;
        Restan.Minutos  = 60 - Minutos - 1;
        Restan.Segundos = 60 - Segundos;

        if ( (Horas == 13) && (Minutos == 45) && (Segundos == 0) )
        {
            system("mplayer extras/cierreDeApuestas.mp3  -endpos 7");
        }
    }

    else if ( ((Horas >= 14) && (Horas < 17)) || ((Horas == 17) && (Minutos <= 30)) )
    {
        Restan.Horas    = 17 - Horas - 1;
        Restan.Minutos  = 60 - Minutos - 1;
        Restan.Segundos = 60 - Segundos;

        if ( (Horas == 17) && (Minutos == 15) && (Segundos == 0) )
        {
            system("mplayer extras/cierreDeApuestas.mp3  -endpos 7");
        }
    }

    else if ( ((Horas == 17) && (Minutos >= 30)) || (Horas < 21) )
    {
        Restan.Horas    = 21 - Horas - 1;
        Restan.Minutos  = 60 - Minutos - 1;
        Restan.Segundos = 60 - Segundos;

        if ( (Horas == 20) && (Minutos == 45) && (Segundos == 0) )
        {
            system("mplayer extras/cierreDeApuestas.mp3  -endpos 7");
        }
    }

    else if ( ((Horas >= 21) && ((Horas <= 23) && (Minutos <= 59) && (Segundos <= 59))) )
    {
        Restan.Horas    = 35 - Horas - 1;
        Restan.Minutos  = 60 - Minutos - 1;
        Restan.Segundos = 60 - Segundos;
    }

    Restan.Reloj.sprintf("Faltan %02d:%02d:%02d para\n   el proximo sorteo",Restan.Horas,Restan.Minutos,Restan.Segundos);
    ui->label_RestoSorteo->setText(Restan.Reloj);
}
