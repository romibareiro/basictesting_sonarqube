#ifndef MYTCPCLIENT_H
#define MYTCPCLIENT_H

#include <QtNetwork/QtNetwork>
#include <QtCore>
#include <QtGui>
#include <json.h>
#include <parser.h>

class MyTCPClient:public QTcpSocket
{
  Q_OBJECT
public:
    MyTCPClient();
    ~MyTCPClient();

    int sincronizarImagen;
    QString nombreImagen;
    int64_t pesoImagen;
    int id [4];

    void writeMyData( QString solicitud );

public slots:
    void readMyData();


private:
    QTcpSocket* socket;
    QByteArray* data;

};

#endif // MYTCPCLIENT_H
