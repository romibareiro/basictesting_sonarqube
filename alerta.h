#ifndef ALERTA_H
#define ALERTA_H

#include <QDialog>
#include <QtGui>
#include <define.h>
#include "stdio.h"
#include <unistd.h>
#include <database.h>

namespace Ui {
class alerta;
}

class alerta : public QDialog
{
    Q_OBJECT    

    int porcentaje;
    int versionDisponible;

    QBasicTimer timer;
    void timerEvent(QTimerEvent*);
    
public:
    explicit alerta(QWidget *parent = 0);
    ~alerta();
    
private:
    Ui::alerta *ui;
};

#endif // ALERTA_H
