#ifndef CONFIGURACION_H
#define CONFIGURACION_H

#include <QtGui>

#include "stdio.h"
#include "sqlite3.h"
#include <database.h>
#include <parser.h>
#include <usbimages.h>
#include <define.h>

#define SALIR 123456
#define TIEMPO_INACTIVIDAD 300

#define ESTADO_INICIAL 0

#define ESTADO_PROPIEDADES_NOMBRE_QUINIELA 1
#define ESTADO_PROPIEDADES_FECHA_QUINIELA 10
#define ESTADO_PROPIEDADES_TIPO_QUINIELA 100
#define ESTADO_PROPIEDADES_NUMERO_QUINIELA 1000


#define ESTADO_CARGAR_QUINIELA_CIFRA_A 11
#define ESTADO_CARGAR_QUINIELA_CIFRA_B 12
#define ESTADO_CARGAR_QUINIELA_CIFRA_C 13
#define ESTADO_CARGAR_QUINIELA_CIFRA_D 14

#define ESTADO_GRUPO_LETRA 21
#define ESTADO_LETRA 22

#define ESTADO_NUMERO_DEL_DIA_A 5
#define ESTADO_NUMERO_DEL_DIA_B 50

#define ESTADO_ACTUALIZACION 7

#define ESTADO_CONFIGURACION 9
#define ESTADO_ESTILO 91
#define ESTADO_PRESENTACION 92
#define ESTADO_PROVINCIA 93
#define ESTADO_OPERACION 94
#define ESTADO_PUBLICIDAD 95
#define ESTADO_JUEGOS 96
#define ESTADO_SONIDO 97
#define ESTADO_FECHA_ACTUAL 98
#define ESTADO_HORA_ACTUAL 980


namespace Ui {
class Configuracion;
}

class Configuracion : public QDialog
{
    Q_OBJECT

public:    
    QBasicTimer timer;

    int estado;
    int posicion;
    int tiempo;
    int multiplicadorLetra;
    int opcionManual;
    int flagInicio;
    int flagGeneral;
    int versionActual;
    int versionDisponible;
    int subVersion;

    QString numeroDia;
    QString fechaActual;
    QString horaActual;
    QString aux;


//    QUINIELACARGAMANUAL quinielaCargaManual;

    databaseQuiniela quinielaCargaManual;

    QPalette * paletteBlue;
    QPalette * paletteBlack;

    // Funciones de la Clase

    explicit Configuracion(QWidget *parent = 0);
    ~Configuracion();

    // Funciones del Sistema

    void timerEvent(QTimerEvent*);
    void keyPressEvent(QKeyEvent*);

    // Funciones Propias

    void seleccionarOpcion0(int opcion);
    void seleccionarOpcion1(int opcion);
    void visualizarNumeros();
    void limpiarColumna0();
    void limpiarColumna1();
    
private slots:

private:
    Ui::Configuracion *ui;
    USBImages *pendrive;
};

#endif // CONFIGURACION_H

