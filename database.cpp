#include <database.h>

/**
 *\fn       int setConfiguracion ( QString nombre , int valor )
 *\brief    Funcion para Grabar la Configuracion en la Base de Datos
 *\note     Graba en la base de datos el valor en la fila del nombre correspondiente.
 *\param    [in] QString nombre -> Nombre de la fila a modificar.
 *\param    [in] int valor -> Valor a grabar.
 *\return   Devuelve "0" si no hubo problema.
 *          Devuelve "1" si hubo algun problema.
*/
int setConfiguracion ( QString nombre , int valor )
{
    char *zErrMsg = 0;
    int rc;
    int error;

    sqlite3 *db;    
    QString querySql;

    error = abrirBaseCliente(&db);

    if(error)
    {
         return EXIT_FAILURE; //si entro aca es que no pudo abrir la bd
    }

// Crear sentencia SQL
    querySql = "UPDATE configuracion SET estado = "
             + QString::number(valor)
             + ", cambio = 1 WHERE nombre ='"
             + nombre
             + "'";
// Ejecutar sentencia SQL
    rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL , &zErrMsg);

    if( rc != SQLITE_OK )
    {
        // Fallo el update
        fprintf(stderr, "setConfiguracion - SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        sqlite3_close(db);
        return EXIT_FAILURE;
    }
    else
    {
        fprintf(stdout, "setConfiguracion - Operation done successfully\n");
        sqlite3_close(db);
        return EXIT_SUCCESS;
    }
}

/**
 *\fn       int getConfiguracion(QString nombre)
 *\brief    Funcion para Grabar la Configuracion en la Base de Datos
 *\note     Obtiene el valor del estado de la fila del nombre correspondiente.
 *\param    [in] QString nombre -> Nombre de la fila a modificar.
 *\return   Devuelve el valor del estado de la fila del nombre correspondiente.
*/
int getConfiguracion(QString nombre)
{
    int dato;

    sqlite3_stmt *query;

    int error;

    sqlite3 *db;
    QString querySql;

    error = abrirBaseCliente(&db);

    if(error)
    {
         return -1 ; //si entro aca es que no pudo abrir la bd
    }

// Crear sentencia SQL
    querySql = "SELECT estado FROM configuracion WHERE nombre ='"
             + nombre
             + "'";

// Ejecutar sentencia SQL
    sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);

    if ( sqlite3_step(query) != SQLITE_ROW)
    {
        sqlite3_finalize(query);
        fprintf(stderr, "getConfiguracion - SQL error: %s\n", sqlite3_errmsg(db));
        return -1;
    }
    dato = sqlite3_column_int(query, 0);

/* Cerrar Base de Datos */

    sqlite3_finalize(query);
    sqlite3_free(NULL);             // ¿Para que Sirve?
    sqlite3_close(db);

    return dato;
}

/**
 *\fn       int cambioConfiguracion (QString nombre)
 *\brief    Funcion para saber si cambio un parametro de la configuracion
 *\note     Revisa en la base de datos si cambio el parametro enviado en la ultima modificacion de la configuracion.
 *\param    [in] QString nombre -> Nombre de la fila a modificar.
 *\return   Devuelve si se modifico el estado de la fila del nombre correspondiente.
*/
int cambioConfiguracion (QString nombre)
{
    int dato, error;
    sqlite3 *db;
    sqlite3_stmt *query;
    QString querySql;

    error = abrirBaseCliente(&db);

    if(error)
    {
         return -1; //si entro aca es que no pudo abrir la bd
    }

// Crear sentencia SQL
    querySql = "SELECT cambio FROM configuracion WHERE nombre ='"
             + nombre
             + "'";

// Ejecutar sentencia SQL
    sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);
    if (sqlite3_step(query) != SQLITE_ROW) {
        sqlite3_finalize(query);
        fprintf(stderr, "cambioConfiguracion - SQL error: %s\n", sqlite3_errmsg(db));
        return -1;
    }

    dato = sqlite3_column_int(query, 0);

// Cerrar Base de Datos

    sqlite3_finalize(query);
    sqlite3_free(NULL);
    sqlite3_close(db);

    return dato;

}

/**
 *\fn       int clearConfiguracion (void)
 *\brief    Funcion para borrar el flag de cambio en la columna de la base de datos
 *\note     Borrar la columna cambio en la base de datos.
 *\return   Devuelve "0" si no hubo problema.
 *          Devuelve "1" si hubo algun problema.
*/
int clearConfiguracion (void)
{
    int rc = 0;
    char *zErrMsg = 0;
    int error = 0;
    QString querySql;
    sqlite3 *db;

    error = abrirBaseCliente(&db);

    if(error)
    {
         return EXIT_FAILURE; // Si entro aca es que no pudo abrir la bd
    }

// Crear sentencia SQL
    querySql = "UPDATE configuracion SET cambio = "
             + QString::number(0);

// Ejecutar sentencia SQL
    rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg);

    if( rc != SQLITE_OK )
    {
        // Fallo la actualizacion
        fprintf(stderr, "clearConfiguracion - SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        sqlite3_close(db);
        return EXIT_FAILURE;
    }
    else
    {
        // salio todo ok
        sqlite3_close(db);
        return EXIT_SUCCESS;
    }
}

//FUNCIONES VARIAS

/**
 *\fn       int abrirBaseCliente(sqlite3**db )
 *\brief    Funcion para abrir la Base de Datos
 *\note     Abre la base y te devuelve un puntero a la misma o te informa un error.
 *\param    [in] sqlite3**db -> es un puntero al puntero que utilizaremos para usar la base.
 *\return   Devuelve "0" si no hubo problema.
 *          Devuelve "1" si hubo algun problema.
*/
int abrirBaseCliente(sqlite3 **db)
{
    sqlite3 *aux;

    // Abrir Base de Datos

    if( sqlite3_open( DBCLIENTE , &aux ) )
    {
        // Informar error
        fprintf(stderr, "abrirBaseCliente - SQL error: %s\n", sqlite3_errmsg(aux));
        return EXIT_FAILURE;
    }
    else
    {
        fprintf(stderr, "abrirBaseCliente - Se abrio correctamente la Base de Datos\n");
        (*db) = aux;
        return EXIT_SUCCESS;
    }
}

//FUNCIONES DE LAS TABLAS DE PUBLICIDAD VARIABLE


// ejemplo:
 //insertarPublicidadVariable("/home/texto/negro.jpg");

/**
 *\fn       int insertarPublicidadVariable(QString direccion)
 *\brief    Funcion para Grabar en la base de datos la direccion de donde se encuentra la publicidad insertada.
 *\note     Si ya existe una con ese nombre la obvia y si el dispositivo ya tiene 4, de ser asi borra la mas vieja y carga la publicidad solicitada.
 *\param    [in] QString direccion -> Valor a grabar en la base.
 *\return   Devuelve "0" si no hubo problema.
 *          Devuelve "1" si hubo algun problema.
*/
int insertarPublicidadVariable(QString direccion)
{//esta funcion inserta en la tabla de publicidad variable si ya existe no la introduce si
 //retorna un 1 es que existe o algo fallo.
     QString querySql;
     int rc=0;
     char *zErrMsg = 0;
     sqlite3_stmt *query;
     sqlite3 *db;
     int error=0;
     int cantidad=0;
     int id=0;

     error=abrirBaseCliente(&db);

     if(error)
     {
         return EXIT_FAILURE; // Si entro aca es que no pudo abrir la bd
     }

    cantidad = contarPublicidadVariable();

    querySql="select *from publicidadvariable WHERE direccion='"
             + direccion.toLatin1()
             + "'";
    sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);

    if (sqlite3_step(query) == SQLITE_ROW)
    {
         // si entro aca es porque esa publicidad ya existe por ende no la cargo
         sqlite3_finalize(query);
         sqlite3_close(db);
         return 1;
    }
    else
    {
        if (cantidad>3)
        {
            querySql="select *from publicidadvariable";
            sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);
            if (sqlite3_step(query) == SQLITE_ROW)
            {
            // si entro aca es porque hay mas de 4 borra la publicidad mas vieja para incorporar la nueva imagen
                id=sqlite3_column_int(query, 0);
                sqlite3_finalize(query);
                querySql = "DELETE FROM publicidadvariable WHERE id_publicidad="
                         +  QString::number(id);

                rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg); //borra el dato de la tabla publicidadvariable

                if( rc != SQLITE_OK )
                {
                    // Fallo el Borrado
                    fprintf(stderr, "insertarPublicidadVariable - SQL error: %s\n", zErrMsg);
                    sqlite3_free(zErrMsg);
                    return EXIT_FAILURE;

                }
                querySql = "INSERT INTO publicidadvariable (direccion) VALUES ('"
                         + direccion.toLatin1()
                         + "')";

                rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg); //insert
                if( rc != SQLITE_OK )
                {
                    // Fallo la Insercion
                    fprintf(stderr, "insertarPublicidadVariable - SQL error: %s\n", zErrMsg);
                    sqlite3_free(zErrMsg);
                    rc = EXIT_FAILURE;
                }
            }
        }
        else
        {
            // Sino existe esa publicidad entonces la agrego
            querySql = "INSERT INTO publicidadvariable (direccion) VALUES ('"
                     + direccion.toLatin1()
                     + "')";
            rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg); //insert
            if( rc != SQLITE_OK )
            {
                // Fallo la Insercion
                fprintf(stderr, "insertarPublicidadVariable - SQL error: %s\n", zErrMsg);
                sqlite3_free(zErrMsg);
                rc = EXIT_FAILURE;
            }
            sqlite3_close(db);
        }
    }
    return rc;
}


 //ejemplo:
//  borrarPublicidadVariable("/home/texto/negro.jpg");

/**
 *\fn       int borrarPublicidadVariable(QString direccion)
 *\brief    Funcion para borrar de la base de datos la direccion de donde se encuentra la publicidad en cuestion.
 *\note     Esta funcion solo se relaciona con la base de datos, no realiza el borrado fisico de la imagen.
 *\param    [in] QString direccion -> Valor a borrar de la base.
 *\return   Devuelve "0" si no hubo problema.
 *          Devuelve "1" si hubo algun problema.
*/

int borrarPublicidadVariable (QString direccion) //borra de la tabla de publicidad variable
{
    QString querySql;
    int rc=0;
    char *zErrMsg = 0;
    sqlite3 *db;
    int error=0;

    error=abrirBaseCliente(&db);

    if(error)
    {
        return -1; //si entro aca es que no pudo abrir la bd
    }

    querySql = "delete from publicidadvariable where direccion ='"
             +  direccion
             + "'";

    rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg); //borra el dato de la tabla publicidadvariable
    if( rc != SQLITE_OK )
    {
        fprintf(stderr, "borrarPublicidadVariable - SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        return 1;
        //fallo el borrado
     }
    sqlite3_close(db);
    return 0;
}

// ejemplo
//actualizarPublicidadVariable ("/home/texto/viejo.jpg","/home/texto/nuevo.jpg");

/**
 *\fn       int actualizarPublicidadVariable(QString direccion1,QString direccion2)
 *\brief    Funcion para actualizar de la base de datos la direccion de donde se encuentra la publicidad en cuestion.
 *\note     Se usa para un reemplazo de publicidad
 *\param    [in] QString direccion1 -> Valor de la base que se desea reemplazar (valor antiguo).
 *\param    [in] QString direccion2 -> Valor de la base que se desea actualizar (nuevo valor).
 *\return   Devuelve "0" si no hubo problema.
 *          Devuelve "1" si hubo algun problema.
*/

int actualizarPublicidadVariable (QString direccion1,QString direccion2)
{
    QString querySql;
    int rc=0;
    char *zErrMsg = 0;
    sqlite3_stmt *query;
    sqlite3 *db;
    int error=0;

    error=abrirBaseCliente(&db);

    if(error)
    {
        return -1; //si entro aca es que no pudo abrir la bd
    }

    querySql = "SELECT id_publicidad from publicidadvariable WHERE direccion ='"
             +  direccion1
             + "'";

    sqlite3_prepare_v2(db, querySql.toLatin1(),querySql.length(), &query, NULL);

    if( sqlite3_step(query) != SQLITE_ROW)
    {
        fprintf(stderr, "actualizarPublicidadVariable - SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        sqlite3_close(db);
        return 1;
        //fallo la actualizacion o no existe
    }
     else
    {
        querySql = "UPDATE publicidadvariable SET direccion ='"
                 +  direccion2.toLatin1()
                 + "' WHERE direccion='"
                 +  direccion1.toLatin1()
                 + "'";

        rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg); //actualizar

        if( rc != SQLITE_OK )
        {
        //fallo la actualizacion
            sqlite3_free(zErrMsg);
            sqlite3_close(db);
            return 1;
        }
    }

    sqlite3_close(db);
    return 0;
}

/**
 *\fn       int borrarPublicidadVariableTodas()
 *\brief    Funcion para borrar de la base de datos.
 *\note     Se usa para borrar de la base de datos todas las direcciones de las publicidades variables
 *\param    [in] esta funcion no recibe ningun parametro
 *\return   Devuelve "0" si no hubo problema.
 *          Devuelve "1" si hubo algun problema.
*/


int borrarPublicidadVariableTodas(void)
{
    QString querySql;
    int rc=0;
    char *zErrMsg = 0;
    sqlite3 *db;
    int error=0;

    error=abrirBaseCliente(&db);

    if(error)
    {
        return -1; //si entro aca es que no pudo abrir la bd
    }

    querySql = "delete from publicidadvariable";

    rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg); //borra el contenido de la  tabla publicidadvariable
    if( rc != SQLITE_OK )
    {
        fprintf(stderr, "borrarPublicidadVariableTodas - SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        return 1;
        //fallo el borrado
     }
    sqlite3_close(db);
    return 0;
}

// ejemplo
//  b=contarPublicidadVariable();

/**
 *\fn       int contarPublicidadVariable()
 *\brief    Funcion para contar cuantas publicidades variables tiene la base de datos.
 *\note     Se usa desde la funcion insertarPublicidadVariable() para saber si ya se alcanzo el maximo de publicidades admitido
 *\param    [in] no tiene parametro de entrada
 *\return   Devuelve (un numero entero entre 0 y 4) si no hubo problema.
 *          Devuelve "-1" si hubo algun problema.
*/

int contarPublicidadVariable(void)
{
    QString querySql;
    sqlite3_stmt *query;
    sqlite3 *db;
    int error=0;
    int i;

    error=abrirBaseCliente(&db);

    if(error)
    {
        return -1; //si entro aca es que no pudo abrir la bd
    }

    querySql = "SELECT *from publicidadvariable";

    sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);

    if (sqlite3_step(query) == SQLITE_ROW)
    {
         i=1;

         while(sqlite3_step(query) == SQLITE_ROW)
         {
            i++;
         }

     }

    sqlite3_close(db);
    return i;

}


// ejemplo
  //  QString publivariable[4];
   // int b=0;
    //b=seleccionarPublicidadVariable(publivariable);


int publicidadVariable::seleccionarPublicidadVariable()
{
   QString querySql;
   sqlite3 *db;
   sqlite3_stmt *query;
   int i=0;
   int error=0;

    error=abrirBaseCliente(&db);

    if(error)
    {
        return -1; //si entro aca es que no pudo abrir la bd
    }


    querySql = "SELECT *from publicidadvariable";

    sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);

           if (sqlite3_step(query) == SQLITE_ROW)
           {
               char texto[100];
               memset (texto,'0',sizeof(texto));

               sprintf (texto,"%s"
                             ,sqlite3_column_text(query,1));

               nombrePublicides[i] =texto;


                       while(sqlite3_step(query) == SQLITE_ROW)
                       {
                        i++;
                        memset (texto,'0',sizeof(texto));

                        sprintf (texto,"%s"
                                      ,sqlite3_column_text(query,1));

                        nombrePublicides[i] = texto;


                       }
           }
           else
              {
               //debo informar que no hay publicidades cargadas
                error=1;
              }

    sqlite3_finalize(query);
    sqlite3_close(db);
    return error;
}

// FUNCIONES DE LA TABLA DE PUBLICIDAD FIJA

// ejemplo:
 //insertarPublicidadFija("/home/texto/negro.jpg");

/**
 *\fn       int insertarPublicidadFija(QString direccion)
 *\brief    Funcion para Grabar en la base de datos la direccion de donde se encuentra la publicidad insertada.
 *\note     Si ya existe una con ese nombre la obvia.
 *\param    [in] QString direccion -> Valor a grabar en la base.
 *\param    [in] int idPubliidad   -> Valor del Id de la Publicidad.
 *\return   Devuelve "0" si no hubo problema.
 *          Devuelve "1" si hubo algun problema.
*/

int insertarPublicidadFija( QString direccion , int idPublicidad )
{//esta funcion inserta en la tabla de publicidad fija si ya existe no la introduce si
 //retorna un 1 es que existe o algo fallo.

     QString querySql;
     int rc = 0;
     char *zErrMsg = 0;
     sqlite3_stmt *query;
     sqlite3 *db;
     int error = 0;

     error = abrirBaseCliente( &db );

     if(error)
     {
         qDebug() << "insertarPublicidadFija - Error al abrir DataBase de Cliente ";
         return EXIT_FAILURE; // Si entro aca es que no pudo abrir la bd
     }

     querySql = "select * from publicidadfija WHERE id_publicidad_fija = "
              + idPublicidad;

     sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);

     if (sqlite3_step(query) == SQLITE_ROW)
     {
         // si entro aca es porue esa publicidad ya existe por ende no la cargo
         sqlite3_finalize( query );
         sqlite3_close( db );
         return EXIT_FAILURE;
     }
     // sino existe esa publicidad entonces la agrego

     querySql = "INSERT INTO publicidadfija ( direccion, id_publicidad_fija ) VALUES ('"
              + direccion.toLatin1()
              + "',"
              + QString::number(idPublicidad)
              + ")";

     rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg); //insert
     if( rc != SQLITE_OK )
     {
         fprintf(stderr, "insertarPublicidadFija - SQL error: %s\n", zErrMsg);
         sqlite3_free(zErrMsg);
         sqlite3_close( db );
         return EXIT_FAILURE;
         //fallo la insercion
      }

     sqlite3_close( db );
     return EXIT_SUCCESS;
}

/**
 *\fn       int existePublicidadFija(int idPublicidad)
 *\brief    Funcion para saber si existe o no la publicidad con su respectivo id.
 *\note     Si ya existe una con ese nombre la obvia.
 *\param    [in] int id            -> Valor del Id de la base de datos.
 *\return   Devuelve "0" si no existe la publicidad fija.
 *          Devuelve "1" si hubo algun problema.
 *          Devuelve "2" si exite la publicidad fijahubo algun problema.
*/

int existePublicidadFija( int * id )
{//esta funcion inserta en la tabla de publicidad fija si ya existe no la introduce si
 //retorna un 1 es que existe o algo fallo.

     QString querySql;
     sqlite3_stmt *query;
     sqlite3 *db;

     if( abrirBaseCliente( &db ) )
     {
         qDebug() << "existePublicidadFija - Error al abrir DataBase de Cliente ";
         return EXIT_FAILURE; // Si entro aca es que no pudo abrir la bd
     }

     querySql = "select id_publicidad_fija from publicidadfija";

     sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);

     sqlite3_step(query);
     id[0] = sqlite3_column_int(query, 0);
     sqlite3_step(query);
     id[1] = sqlite3_column_int(query, 0);
     sqlite3_step(query);
     id[2] = sqlite3_column_int(query, 0);
     sqlite3_step(query);
     id[3] = sqlite3_column_int(query, 0);

     sqlite3_finalize(query);
     sqlite3_close( db );
     return EXIT_SUCCESS;
}

/**
 *\fn       int actualizarIDPublicidadFija(int idPublicidad)
 *\brief    Funcion para actualizar las publicidades Fijas.
 *\param    [in] int id            -> Valor del Id de la base de datos.
 *\return   Devuelve "0" si no existe la publicidad fija.
 *          Devuelve "1" si hubo algun problema.
 *          Devuelve "2" si exite la publicidad fijahubo algun problema.
*/

int actualizarIDPublicidadFija( int * id )
{//esta funcion inserta en la tabla de publicidad fija si ya existe no la introduce si
 //retorna un 1 es que existe o algo fallo.

    QString querySql;
    int rc = 0;
    char *zErrMsg = 0;
    sqlite3 *db;
    int error = 0;

    error = abrirBaseCliente( &db );

    if(error)
    {
        qDebug() << "actualizarPublicidadFija - Error al abrir DataBase de Cliente ";
        return EXIT_FAILURE;
    }

    querySql = "UPDATE publicidadfija SET id_publicidad_fija = "
             + QString::number( id[0] )
             + " WHERE  id = 1";

    rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg); //insert
    if( rc != SQLITE_OK )
    {
        fprintf(stderr, "actualizarPublicidadFija - SQL error 1: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        sqlite3_close( db );
        return EXIT_FAILURE;
     }

    querySql = "UPDATE publicidadfija SET id_publicidad_fija = "
             + QString::number( id[1] )
             + " WHERE  id = 2";

    rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg); //insert
    if( rc != SQLITE_OK )
    {
        fprintf(stderr, "actualizarPublicidadFija - SQL error 2: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        sqlite3_close( db );
        return EXIT_FAILURE;
     }

    querySql = "UPDATE publicidadfija SET id_publicidad_fija = "
             + QString::number( id[2] )
             + " WHERE  id = 3";

    rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg); //insert
    if( rc != SQLITE_OK )
    {
        fprintf(stderr, "actualizarPublicidadFija - SQL error 3: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        sqlite3_close( db );
        return EXIT_FAILURE;
     }

    querySql = "UPDATE publicidadfija SET id_publicidad_fija = "
             + QString::number( id[3] )
             + " WHERE  id = 4";

    rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg); //insert
    if( rc != SQLITE_OK )
    {
        fprintf(stderr, "actualizarPublicidadFija - SQL error 4: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        sqlite3_close( db );
        return EXIT_FAILURE;
     }

    sqlite3_close( db );
    return EXIT_SUCCESS;
}


// ejemplo:
// borrarPublicidadFija("/home/texto/negro.jpg");

/**
 *\fn       int borrarPublicidadfija(QString direccion)
 *\brief    Funcion para borrar de la base de datos la direccion de donde se encuentra la publicidad en cuestion.
 *\note     Esta funcion solo se relaciona con la base de datos, no realiza el borrado fisico de la imagen.
 *\param    [in] QString direccion -> Valor a borrar de la base.
 *\return   Devuelve "0" si no hubo problema.
 *          Devuelve "1" si hubo algun problema.
*/

int borrarPublicidadFija (QString direccion) //borra de la tabla de publicidad variable
{
    QString querySql;
    int rc=0;
    char *zErrMsg = 0;
    sqlite3 *db;
    int error=0;

    error=abrirBaseCliente(&db);

    if(error)
    {
        return -1; //si entro aca es que no pudo abrir la bd
    }

    querySql = "delete from publicidadfija where direccion ='"
             +  direccion
             + "'";

    rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg); //borra el dato de la tabla publicidadvariable

    if( rc != SQLITE_OK )
    {
        fprintf(stderr, "borrarPublicidadFija - SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        rc=1;
        //fallo el borrado
     }
    sqlite3_close(db);
    return rc;
}

// ejemplo
//actualizarPublicidadFija ("/home/texto/viejo.jpg","/home/texto/nuevo.jpg");

/**
 *\fn       int actualizarPublicidadFija(QString direccion)
 *\brief    Funcion para actualizar de la base de datos la direccion de donde se encuentra la publicidad en cuestion.
 *\note     Se usa para un reemplazo de publicidad
 *\param    [in] int idPublicidadExistente -> Valor de la id Publicidad Existente.
 *\param    [in] int idPublicidadNueva     -> Valor de la id Publicidad Nueva.
 *\param    [in] QString direccion         -> Valor de la direccion a que desea actualizar.
 *\return   Devuelve "0" si no hubo problema.
 *          Devuelve "1" si hubo algun problema.
*/

int actualizarPublicidadFija ( int idPublicidadExistente, int idPublicidadNueva , QString direccion)
{
    QString querySql;
    int rc = 0 , id;
    char *zErrMsg = 0;
    sqlite3_stmt *query;
    sqlite3 *db;

    if( abrirBaseCliente ( &db ) )
    {
        return EXIT_FAILURE; //si entro aca es que no pudo abrir la bd
    }

    querySql = "SELECT id from publicidadfija WHERE id_Publicidad_Fija = "
             + QString::number(idPublicidadExistente);



    sqlite3_prepare_v2(db, querySql.toLatin1(),querySql.length(), &query, NULL);

    if( sqlite3_step(query) != SQLITE_ROW)
    {
        fprintf(stderr, "actualizarPublicidadFija - SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        sqlite3_close(db);
        return EXIT_FAILURE;
        // Fallo la actualizacion o no existe
    }

    id = sqlite3_column_int(query, 0);

    querySql = "UPDATE publicidadfija SET id_publicidad_fija = "
             + QString::number(idPublicidadNueva)
             + ", direccion = '"
             + direccion.toLatin1()
             + "'"
             + "WHERE id = "
             + QString::number(id);

    qDebug() << querySql;

    rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg); //actualizar

    if( rc != SQLITE_OK )
    {
    // Fallo la actualizacion
        sqlite3_free(zErrMsg);
        sqlite3_close(db);
        return EXIT_FAILURE;
    }

    sqlite3_close(db);
    return EXIT_SUCCESS;
}

// Ejemplo:
//b= borrarPublicidadFijaTodas();
/**
 *\fn       int borrarPublicidadFijaTodas()
 *\brief    Funcion para borrar de la base de datos.
 *\note     Se usa para borrar de la base de datos todas las direcciones de las publicidades fijas
 *\return   Devuelve "0" si no hubo problema.
 *          Devuelve "1" si hubo algun problema.
*/

int borrarPublicidadFijaTodas(void)
{
    QString querySql;
    int rc=0;
    char *zErrMsg = 0;
    sqlite3 *db;
    int error = EXIT_SUCCESS;

    error = abrirBaseCliente( &db );

    if(error)
    {
        return EXIT_FAILURE; //si entro aca es que no pudo abrir la bd
    }

    querySql = "delete from publicidadfija";

    rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg); //borra el contenido de la  tabla publicidadfija
    if( rc != SQLITE_OK )
    {
        // Fallo el borrado
        fprintf(stderr, "borrarPublicidadFijaTodas - SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        error = EXIT_FAILURE;
    }
    sqlite3_close(db);
    return error;
}

// ejemplo
  //  QString publifija[4];
   // int b=0;
    //b=seleccionarPublicidadFija(publifija);

/**
 *\fn       int seleccionarPublicidadFija(QString *vector)
 *\brief    Funcion para leer de la base de datos.
 *\note     Se usa para leer de la base de datos todas las direcciones de la publicidad fija
 *\param    [in] QString vector-> Recepciona un puntero a un vector de QString donde se almacenaran las direcciones de las publicidades.
 *\return   Devuelve "0" si no hubo problema.
 *          Devuelve "1" si hubo algun problema.
*/

int seleccionarPublicidadFija(QString *vector)
{
    QString querySql;
    sqlite3 *db;
    sqlite3_stmt *query;
    int i = 0;
    int error = EXIT_SUCCESS;

    error = abrirBaseCliente(&db);

    if(error)
    {
        return EXIT_FAILURE; //si entro aca es que no pudo abrir la bd
    }

    querySql = "SELECT * from publicidadfija";

    sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);

    if (sqlite3_step(query) == SQLITE_ROW)
    {
        i = 0;
        do{
            vector [i] = QString("%1").arg( (const char *) sqlite3_column_text(query,1));
            i++;

        }while( sqlite3_step(query) == SQLITE_ROW );
    }
    else
    {
    // Debo informar que no hay publicidades cargadas
        error = EXIT_FAILURE;
    }

    sqlite3_finalize(query);
    sqlite3_close(db);
    return error;
}

// ejemplo
// b=contarPublicidadFija();

/**
 *\fn       int contarPublicidadFija()
 *\brief    Funcion para contar.
 *\note     Se usa para contar cuantas publicidades fijas hay registradas en la base de datos.
 *\param    [in] no tiene parametro de entrada
 *\return   Devuelve (un numero entero entre 0 y 4) si no hubo problema.
 *          Devuelve "-1" si hubo algun problema.
*/

int contarPublicidadFija(void)
{
    QString querySql;
    sqlite3_stmt *query;
    sqlite3 *db;
    int error=0;
    int i;

    error=abrirBaseCliente(&db);

    if(error)
    {
        return -1; // Si entro aca es que no pudo abrir la bd
    }

    querySql = "SELECT * from publicidadfija";

    sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);

    if (sqlite3_step(query) == SQLITE_ROW)
    {
         i=1;

         while(sqlite3_step(query) == SQLITE_ROW)
         {
            i++;
         }

    }
    sqlite3_close(db);
    return i;

}

// FUNCIONES PARA LOS JUEGOS

//EJEMPLO
  // direccionImagenQuini(3,direccion);

/**
 *\fn       int direccionImagenQuini(int numero, char *direccion)
 *\brief    Funcion para asociar las imagenes de los numeros del quini6.
 *\note     Esta funcion actualmente no se usa debido a que se decidio retirar este juego del proyecto
 *\param    [in] int numero->referencia a los numeros del 0 al 36.
 *\param    [in] char *direccion->puntero a vector de char donde se almacena la direccion de las imagenes en cuestion.
 *\return   Devuelve "0"  si no hubo problema.
 *          Devuelve "1" si hubo algun problema.
*/

int direccionImagenQuini(int numero, char *direccion)
{
    int rc=0;
    QString querySql;
    sqlite3_stmt *query;
    sqlite3 *db;
    int error=0;

    error = abrirBaseCliente(&db);

    if(error)
    {
         return EXIT_FAILURE; //si entro aca es que no pudo abrir la bd
    }

//cominenzo revisando que el numero solicitado existe

    querySql = "select imagenquini from suenos where numero="
             + QString::number(numero);

    sqlite3_prepare_v2(db, querySql.toLatin1(),querySql.length(), &query, NULL);

    if (sqlite3_step(query) == SQLITE_ROW)
    {
//        memset (direccion,0,sizeof(direccion));

        sprintf (direccion,"%s"
                     ,sqlite3_column_text(query,0));

        sqlite3_finalize(query);
        rc=0;
       }
       else
       {
           // ese numero no existe, informar error
           sqlite3_finalize(query);
           rc=1;
       }

    /* Cerrar Base de Datos */
    sqlite3_close(db);
    return rc;
}

/* DATABASE QUINIELA */

//Constructor de databaseQuiniela

databaseQuiniela::databaseQuiniela()
{
    manual = 0;
}

//Destructor de databaseQuiniela

databaseQuiniela::~databaseQuiniela()
{

}

/**
 *\fn       int databaseQuiniela::insertarSorteo()
 *\brief    Funcion de la clase databaseQuiniela
 *\note     Si la quiniela no existe la ingresa en la base de datos, si existe la actualiza.
 *\return   Devuelve "0" si no hubo problema.
 *          Devuelve "1" si hubo algun problema.
*/

int databaseQuiniela::insertarSorteo()
{
    int rc = 0;
    char *zErrMsg = 0;

    QString querySql;

/*  querySql = "INSERT INTO quiniela (id_tipo,fecha,quiniela,letras,nrosorteo) VALUES ("
                + QString::number(tipo)
                + ",'2','3','4','5')";
    sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg);
*/

    abrirBaseCliente(&db);

    sqlite3_stmt *query;
    int ntipo = 0;
    int id = 0;
    int i;

    querySql = "SELECT id_tipo from tipo WHERE tipo='"
             + tipoSorteo.toLatin1()
             + "'";

    sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);

    if (sqlite3_step(query) == SQLITE_ROW)
    {
        ntipo = sqlite3_column_int(query, 0); // Aca guardo el numero de id_tipo
        sqlite3_finalize(query);

// Ahora debo verificar que esa quiniela en cuestion no este cargada, si esta cargada la actualizo

        querySql = "SELECT id_quiniela FROM quiniela WHERE fecha='"
                 + fechaSorteo.toLatin1()
                 + "' and quiniela='"
                 + nombreSorteo.toLatin1()
                 + "' and id_tipo="
                 + QString::number(ntipo);

        sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);


        if (sqlite3_step(query) != SQLITE_ROW)
        {
        // como no existe entonces la cargo
            sqlite3_finalize(query);

            querySql = "INSERT INTO quiniela (id_tipo,fecha,quiniela,letras,nrosorteo) VALUES ("
                     + QString::number(ntipo)
                     + ",'"
                     + fechaSorteo.toLatin1()
                     + "','"
                     + nombreSorteo.toLatin1()
                     + "','"
                     + letrasSorteo.toLatin1()
                     + "','"
                     + numeroSorteo.toLatin1()
                     + "')";

            rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg); //insert

            if( rc != SQLITE_OK )
            {
                // Fallo la insercion
                fprintf(stderr, "insertarSorteo - SQL error: %s\n", zErrMsg);
                sqlite3_free(zErrMsg);
                rc = EXIT_FAILURE;
            }
            else
            {
            // Ahora debo cargar los 20 numeros
            // La sentencia que sigue me devuelve el ultimo id de la tabla para saber donde cargar los numeros
                querySql = "select max(id_quiniela) from quiniela";

                sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);

                if (sqlite3_step(query) == SQLITE_ROW)
                {
                    // Ahora cargo los 20 numeros
                    id = sqlite3_column_int(query, 0); //aca guardo el numero del ultimo id de la tabla quiniela
                    sqlite3_finalize(query);
                    for ( i=0 ; i<20 ; i++ )
                    {
                        querySql = "INSERT INTO PREMIO (id_quiniela, posicion,premio) VALUES ("
                                 + QString::number(id)
                                 + ","
                                 + QString::number(i+1)
                                 + ",'"
                                 + premiosSorteo[i].toLatin1()
                                 + "')";

                        rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL , &zErrMsg); //insert

                        if( rc != SQLITE_OK )
                        {
                            // Fallo la insercion
                            fprintf(stderr, "insertarSorteo - SQL error: %s\n", zErrMsg);
                            sqlite3_free(zErrMsg);
                            rc = EXIT_FAILURE ;
                        }
                        else
                        {
                             qDebug("insertarSorteo - Operation done successfully");
                             rc = EXIT_SUCCESS;
                        }
                    }
                }
            }
        }
        else
        {
            // Si existe entonces actualizo
            id = sqlite3_column_int(query, 0);//aca guardo el id de la quiniela a actualizar
            sqlite3_finalize(query);
            querySql = "UPDATE quiniela SET id_tipo="
                     + QString::number(ntipo)
                     + ", fecha='"
                     + fechaSorteo.toLatin1()
                     + "',"
                     + "quiniela='"
                     + nombreSorteo.toLatin1()
                     + "',"
                     + "letras='"
                     + letrasSorteo.toLatin1()
                     + "',"
                     + "nrosorteo='"
                     + numeroSorteo.toLatin1()
                     + "'"
                     + "WHERE id_quiniela="
                     + QString::number(id);

            rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg); //actualizar

            if( rc != SQLITE_OK )
            {
                // Fallo la actualizacion
                fprintf(stderr, "insertarSorteo - SQL error: %s\n", zErrMsg);
                sqlite3_free(zErrMsg);
                rc=1;
            }
            else
            {
                // Actualizar los 20 numeros
                for ( i=0 ; i<20 ; i++ )
                {
                    querySql = "UPDATE premio SET premio='"
                             + premiosSorteo[i].toLatin1()
                             + "' WHERE (id_quiniela="
                             + QString::number(id)
                             + " AND posicion ="
                             + QString::number(i+1)
                             + ")";

                    rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL , &zErrMsg); //update

                    if( rc != SQLITE_OK )
                    {
                        // Fallo el update
                        fprintf(stderr, "insertarSorteoManual - SQL error: %s\n", zErrMsg);
                        sqlite3_free(zErrMsg);
                        rc = EXIT_FAILURE;
                    }
                    else
                    {
                        qDebug("insertarSorteo - Operation done successfully");
                        rc = EXIT_SUCCESS;
                    }
                }
            }
        }
    }
    else
    {
        // Ese tipo no existe
        sqlite3_finalize(query);
        rc = EXIT_FAILURE;
    }

    sqlite3_close(db);
    return rc;
}

/**
 *\fn       int databaseQuiniela::seleccionarSorteo()
 *\brief    Funcion de la clase databaseQuiniela
 *\note     Si la quiniela existe me devuelve todos los valores referidos a la misma
 *\return   Devuelve "0" si no hubo problema.
 *          Devuelve "1" si hubo algun problema.
*/

int databaseQuiniela::seleccionarSorteo()
{
    QString querySql;
    sqlite3_stmt *query;
    int id = 0;
    int ntipo = 0;
    int i = 0;
    int error = EXIT_SUCCESS;

    abrirBaseCliente(&db);

    querySql = "SELECT id_tipo from tipo WHERE tipo='"
               + tipoSorteo
               + "'";

    sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);

    if (sqlite3_step(query) == SQLITE_ROW)
    {
        ntipo = sqlite3_column_int(query, 0); // Aca guardo el numero de id_tipo
        sqlite3_finalize(query);

    // Ahora debo verificar que esa quiniela en cuestion existe, sino rechazo la operación

//        querySql = "SELECT quiniela.id_quiniela,quiniela.nroSorteo,quiniela.letras FROM quiniela WHERE fecha='"
//                + fechaSorteo.toLatin1()
//                + "' and id_tipo ="
//                + QString::number( ntipo)
//                + " and quiniela='"
//                + nombreSorteo.toLatin1()
//                + "'";

        querySql = "SELECT quiniela.id_quiniela,quiniela.nroSorteo,quiniela.letras,quiniela.fecha FROM quiniela WHERE id_tipo ="
                 + QString::number( ntipo)
                 + " and quiniela='"
                 + nombreSorteo.toLatin1()
                 + "' ORDER BY fecha DESC";

        sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);

        if (sqlite3_step(query) == SQLITE_ROW)
        {
        // Si existe la quiniela entonces busco los demas datos
            id = sqlite3_column_int(query, 0); //aca guardo el id de la quiniela

            numeroSorteo = QString("%1").arg( (const char *) sqlite3_column_text( query , 1 ) );

            letrasSorteo = QString("%1").arg( (const char *) sqlite3_column_text( query , 2 ) );

            fechaSorteo  = QString("%1").arg( (const char *) sqlite3_column_text( query , 3 ) );


            // Le da Formato a la Fecha

            int auxf;
            auxf = fechaSorteo.toInt();
            fechaSorteo.clear();
            fechaSorteo.sprintf("%d-%d-%d", auxf % 100 , ( auxf % 10000 ) / 100 , auxf / 10000  );

            //

            sqlite3_finalize(query);

            querySql = " SELECT * from premio where id_quiniela = "
                     + QString::number(id)
                     + " order by posicion";   // Busco en tabla premio

            sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);


            if (sqlite3_step(query) == SQLITE_ROW)
            {
                i = 0;
                do{
                    premiosSorteo[i] = QString("%1").arg( (const char *) sqlite3_column_text( query , 3 ) );
                    i++;
                }while( sqlite3_step(query) == SQLITE_ROW );
            }
            else
            {
            // Informar problemas con la tabla premio
                sqlite3_finalize(query);
                error = EXIT_FAILURE;
            }
        }
        else
        {
        // Debo informar que esa quiniela no existe
            sqlite3_finalize(query);
            error = EXIT_FAILURE;
        }
    }
    else
    {
    // Debo informar que el tipo no corresponde
        sqlite3_finalize(query);
        error = EXIT_FAILURE;
    }

    // Cerrar Base de Datos

    sqlite3_close(db);

    return error;
}

/**
 *\fn       int databaseQuiniela::seleccionarSorteoUnico()
 *\brief    Funcion de la clase databaseQuiniela
 *\note     Si la quiniela existe me devuelve todos los valores referidos a la misma
 *\return   Devuelve "0" si no hubo problema.
 *          Devuelve "1" si hubo algun problema.
*/

int databaseQuiniela::seleccionarSorteoUnico()
{
    QString querySql;
    sqlite3_stmt *query;
    int id = 0;
    int ntipo = 0;
    int i = 0;
    int error = EXIT_SUCCESS;

    abrirBaseCliente(&db);

    querySql = "SELECT id_tipo from tipo WHERE tipo='"
               + tipoSorteo
               + "'";

    sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);

    if (sqlite3_step(query) == SQLITE_ROW)
    {
        ntipo = sqlite3_column_int(query, 0); // Aca guardo el numero de id_tipo
        sqlite3_finalize(query);

    // Ahora debo verificar que esa quiniela en cuestion existe, sino rechazo la operación

        querySql = "SELECT quiniela.id_quiniela,quiniela.nroSorteo,quiniela.letras FROM quiniela WHERE fecha='"
                + fechaSorteo.toLatin1()
                + "' and id_tipo ="
                + QString::number( ntipo)
                + " and quiniela='"
                + nombreSorteo.toLatin1()
                + "'";

        sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);

        if (sqlite3_step(query) == SQLITE_ROW)
        {
        // Si existe la quiniela entonces busco los demas datos
            id = sqlite3_column_int(query, 0); //aca guardo el id de la quiniela

            numeroSorteo = QString("%1").arg( (const char *) sqlite3_column_text( query , 1 ) );

            letrasSorteo = QString("%1").arg( (const char *) sqlite3_column_text( query , 2 ) );

            sqlite3_finalize(query);

            querySql = " SELECT * from premio where id_quiniela = "
                     + QString::number(id)
                     + " order by posicion";   // Busco en tabla premio

            sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);


            if (sqlite3_step(query) == SQLITE_ROW)
            {
                i = 0;
                do{
                    premiosSorteo[i] = QString("%1").arg( (const char *) sqlite3_column_text( query , 3 ) );
                    i++;
                }while( sqlite3_step(query) == SQLITE_ROW );
            }
            else
            {
            // Informar problemas con la tabla premio
                sqlite3_finalize(query);
                error = EXIT_FAILURE;
            }
        }
        else
        {
        // Debo informar que esa quiniela no existe
            sqlite3_finalize(query);
            error = EXIT_FAILURE;
        }
    }
    else
    {
    // Debo informar que el tipo no corresponde
        sqlite3_finalize(query);
        error = EXIT_FAILURE;
    }

    // Cerrar Base de Datos

    sqlite3_close(db);

    return error;
}

/*
* esta funcion esta pensada para el programa del  maxi que muestra todos los sueños
* le pasas el puntero a la base y un puntero a un vector de estructuras del tipo
* suenos y en ese vector te guarda en orden todos los datos de los suenos
* necesario para el programa del negrito
* ejemplo
* suenos detalles [100];
* memset(detalles,0,sizeof(detalles));// blanqueo el vector de estructuras
* a = selectSuenos(db,detalles);
*/

/**
* esta funcion esta pensada para el programa del  maxi que muestra todos los sueños
* le pasas el puntero a la base y un puntero a un vector de estructuras del tipo
* suenos y en ese vector te guarda en orden todos los datos de los suenos
* necesario para el programa del negrito
* ejemplo
* suenos detalles [100];
* memset(detalles,0,sizeof(detalles));// blanqueo el vector de estructuras
* a = selectSuenos(db,detalles);
*/

dataBaseSuenios::dataBaseSuenios()
{

    printf("Constructor de databaseSuenios\n");

}

dataBaseSuenios::~dataBaseSuenios()
{
    printf("Destructor de databaseSuenios\n");
}

/**
 *\fn       int dataBaseSuenios::seleccionarSuenios()
 *\brief    Funcion de la clase suenios
 *\note     Busca en la base la direccion de todas las imagenes para el juego de los suenios.
 *\return   Devuelve "0" si no hubo problema.
 *          Devuelve "1" si hubo algun problema.
*/

int dataBaseSuenios::seleccionarSuenios()
{

    QString querySql;
    sqlite3_stmt *query;
    int i = 0;
    int error;

    abrirBaseCliente(&db);

    querySql = "SELECT * FROM suenios";

    sqlite3_prepare_v2( db , querySql.toLatin1(), querySql.length(), &query, NULL);

    while ( sqlite3_step(query) == SQLITE_ROW )
    {

        textoSuenios[i] = QString("%1-%2").arg( sqlite3_column_int ( query , 1 ))
                                          .arg((const char *) sqlite3_column_text( query , 2 ) );

        direccionSuenios[i] = QString("%1").arg((const char *) sqlite3_column_text ( query , 4 ) );

        i++;
    }

    if(i==100)
    {
        error = EXIT_FAILURE ; // Salio todo ok
    }
    else
    {
        error = EXIT_SUCCESS ; // Algo no funciono
    }

    sqlite3_close(db);

    return error;
}


// Funciones para la Carga Manual de Quinielas

/**
 *\fn       int databaseQuiniela::insertarSorteoManual()
 *\brief    Funcion de la clase databaseQuiniela
 *\note     Se utiliza para la insercion manual de los sorteos de quinielas.
 *\return   Devuelve "0" si no hubo problema.
 *          Devuelve "1" si hubo algun problema.
*/

int databaseQuiniela::insertarSorteoManual()
{
    int rc = 0;
    char *zErrMsg = 0;
    QString querySql;

//  querySql = "INSERT INTO quiniela (id_tipo,fecha,quiniela,letras,nrosorteo) VALUES ("
//                + QString::number(tipo)
//                + ",'2','3','4','5')";
//  sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg);


    abrirBaseCliente(&db);

    sqlite3_stmt *query;
    int ntipo = 0;
    int id = 0;
    int i;

    querySql = "SELECT id_tipo from tipo WHERE tipo='"
             + tipoSorteo.toLatin1()
             + "'";

    sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);

    if (sqlite3_step(query) == SQLITE_ROW)
    {
        ntipo = sqlite3_column_int(query, 0); // Aca guardo el numero de id_tipo
        sqlite3_finalize(query);

// Verificar que esa quiniela en cuestion no este cargada, si esta cargada la actualizo

        querySql = "SELECT id_quiniela FROM quiniela WHERE manual="
                 + QString::number(manual);

        sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);


        if (sqlite3_step(query) != SQLITE_ROW)
        {
        // como no existe entonces la cargo
            sqlite3_finalize(query);

            querySql = "INSERT INTO quiniela (id_tipo,fecha,quiniela,letras,nrosorteo,manual) VALUES ("
                     + QString::number(ntipo)
                     + ",'"
                     + fechaSorteo.toLatin1()
                     + "','"
                     + nombreSorteo.toLatin1()
                     + "','"
                     + letrasSorteo.toLatin1()
                     + "','"
                     + numeroSorteo.toLatin1()
                     + "',"
                     + QString::number(manual)
                     + ")";

            rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg); //insert

            if( rc != SQLITE_OK )
            {
                fprintf(stderr, "insertarSorteoManual - SQL error: %s\n", zErrMsg);
                sqlite3_free(zErrMsg);
                rc = 1;
                //fallo la insercion
            }
            else
            {
            // Ahora debo cargar los 20 numeros
            // la sentencia que sigue me devuelve el ultimo id de la tabla para saber donde cargar los numeros
                querySql = "select max(id_quiniela) from quiniela";

                sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);

                if (sqlite3_step(query) == SQLITE_ROW)
                {
                    id = sqlite3_column_int(query, 0); //aca guardo el numero del ultimo id de la tabla quiniela
                    sqlite3_finalize(query);
                    //ahora cargo los 20 numeros

                    for (i=0;i<20;i++)
                    {
                        querySql = "INSERT INTO PREMIO (id_quiniela, posicion,premio) VALUES ("
                                 + QString::number(id)
                                 + ","
                                 + QString::number(i+1)
                                 + ",'"
                                 + premiosSorteo[i].toLatin1()
                                 + "')";

                        rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL , &zErrMsg); //insert

                        if( rc != SQLITE_OK )
                        {
                            fprintf(stderr, "insertarSorteoManual - SQL error: %s\n", zErrMsg);
                            sqlite3_free(zErrMsg);
                            rc = 1;
                            //fallo la insercion
                        }
                        else
                        {
                             fprintf(stdout, "insertarSorteoManual - Operation done successfully\n");
                             rc = 0;
                        }
                    }
                }
            }
        }
        else
        {
            // si existe entonces actualizo
            id=sqlite3_column_int(query, 0);//aca guardo el id de la quiniela a actualizar
            sqlite3_finalize(query);
            querySql = "UPDATE quiniela SET id_tipo="
                     + QString::number(ntipo)
                     + ", fecha='"
                     + fechaSorteo.toLatin1()
                     + "',"
                     + "quiniela='"
                     + nombreSorteo.toLatin1()
                     + "',"
                     + "letras='"
                     + letrasSorteo.toLatin1()
                     + "',"
                     + "nrosorteo='"
                     + numeroSorteo.toLatin1()
                     + "',"
                     + "manual="
                     + QString::number(manual)
                     + " WHERE id_quiniela="
                     + QString::number(id);

            rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL ,&zErrMsg); //actualizar

            if( rc != SQLITE_OK )
            {
                fprintf(stderr, "insertarSorteoManual - SQL error: %s\n", zErrMsg);
                sqlite3_free(zErrMsg);
                rc=1;
                //fallo la actualizacion
            }
            else
            {
                //ahora actualizo los 20 numeros
                for (i=0;i<20;i++)
                {
                    querySql = "UPDATE premio SET premio='"
                             + premiosSorteo[i].toLatin1()
                             + "' WHERE (id_quiniela="
                             + QString::number(id)
                             + " AND posicion ="
                             + QString::number(i+1)
                             + ")";

                    rc = sqlite3_exec(db, querySql.toLatin1(), NULL , NULL , &zErrMsg); //update

                    if( rc != SQLITE_OK )
                    {
                        fprintf(stderr, "insertarSorteoManual - SQL error: %s\n", zErrMsg);
                        sqlite3_free(zErrMsg);
                        rc=1;
                        //fallo el update
                    }
                    else
                    {
                        fprintf(stdout, "insertarSorteoManual - Operation done successfully\n");
                        rc=0;
                    }
                }
            }
        }
    }
    else
    {
    //ese tipo no existe
        sqlite3_finalize(query);
        rc=1;
    }

    sqlite3_close(db);
    return rc;
}


/**
 *\fn       int databaseQuiniela::seleccionarSorteoManual()
 *\brief    Funcion de la clase databaseQuiniela
 *\note     Se utiliza para extraer los valores de un sorteo ingresado manualmente.
 *\return   Devuelve "0" si no hubo problema.
 *          Devuelve "1" si hubo algun problema.
*/

int databaseQuiniela::seleccionarSorteoManual()
{
    QString querySql;
    sqlite3_stmt *query;
    int id = 0;
    int ntipo = 0;
    int i = 0;
    int error = 0;

    abrirBaseCliente(&db);

// Verificar que esa quiniela en cuestion existe, sino rechazo la operación

    querySql = "SELECT quiniela.id_quiniela,quiniela.id_tipo,quiniela.quiniela,quiniela.fecha,quiniela.nrosorteo,quiniela.letras FROM quiniela WHERE manual="
             + QString::number(manual);

    sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);

// Si existe la quiniela entonces busco los demas datos
    if (sqlite3_step(query) == SQLITE_ROW)
    {

        id = sqlite3_column_int(query, 0); // Aca guardo el id de la quiniela

        ntipo = sqlite3_column_int(query, 1); // Aca guardo el id del tipo

        nombreSorteo = QString("%1").arg( (const char *) sqlite3_column_text( query , 2 ) );

        fechaSorteo = QString("%1").arg( (const char *) sqlite3_column_text( query , 3 ) );

        numeroSorteo = QString("%1").arg( (const char *) sqlite3_column_text( query , 4 ) );

        letrasSorteo = QString("%1").arg( (const char *) sqlite3_column_text( query , 5 ) );

        sqlite3_finalize(query);

        querySql = "SELECT tipo FROM tipo WHERE id_tipo="
                 + QString::number(ntipo);

        sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);

        if (sqlite3_step(query) == SQLITE_ROW)
        {
            tipoSorteo.sprintf( "%s" , sqlite3_column_text( query , 0 ) ); // Aca guardo el numero de id_tipo
            sqlite3_finalize(query);
        }

        querySql = "SELECT *from premio WHERE id_quiniela="
                    + QString::number(id)
                    + " ORDER BY posicion";   //busco en tabla premio

        sqlite3_prepare_v2(db, querySql.toLatin1(), querySql.length(), &query, NULL);
        if (sqlite3_step(query) == SQLITE_ROW)
        {
            i = 0;
            do{
                premiosSorteo[i] = QString("%1").arg( (const char *) sqlite3_column_text( query , 3 ) );
                i++;
            }while( sqlite3_step(query) == SQLITE_ROW );
        }
        else
        {
            sqlite3_finalize(query);    // Informar problemas con la tabla premio
            error = EXIT_FAILURE;
        }
    }
    else
    {
        sqlite3_finalize(query);        // Debo informar que esa quiniela no existe
        error = EXIT_FAILURE;
    }

// Cerrar Base de Datos

    sqlite3_close(db);
    return error;
}
