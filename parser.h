#ifndef PARSER_H
#define PARSER_H

#include <QFile>
#include <QTextBrowser>
#include <QLineEdit>
#include <QFileInfo>
#include <QDebug>

#include "sqlite3.h"
#include <database.h>
#include <conexion.h>
#include <json.h>

#define INICIO_NUMERO_ROJO "<font face='Verdana, Arial, Helvetica, sans-serif' color='#FF0000' size='2'>"
#define INICIO_NUMERO_NEGRO "<font face='Verdana, Arial, Helvetica, sans-serif' size='2'>"


void passToDBO ( char* nombre_archivo, char * fecha, char * tipo);
void findMe ( QString *sOriginal,QString sToFind, int *isDataOk, databaseQuiniela *quiniela);

int jsonParseQuiniela( const QByteArray &str , databaseQuiniela * quiniela );

void passTuJugada(QString nombre , QString fecha );

int descapadoNumeros(QString *sOriginal, QString tipoQuiniela , databaseQuiniela *dbQuiniela);

#endif // PARSER_H
